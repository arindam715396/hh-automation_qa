package com.biofourmis.biovitals.pagefactory.patient;

import com.biofourmis.biovitals.utilities.CommonUtils;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;
import java.util.Map;

@Log4j
public class DiaryNotesPage extends CommonUtils {
    private WebDriver driver;

    public DiaryNotesPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/textview_header_diary")
    private WebElement dairyHeader;
    @AndroidFindBy(xpath = "//*[@content-desc='Diary']")
    private WebElement diary;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/textview_add_new_note")
    private WebElement addNewNote;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/textview_search_diary")
    private WebElement search;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/textview_save_new_note")
    private WebElement saveNote;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/textview_cancel_add_new_note")
    private WebElement cancel;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/edittext_note_title")
    private WebElement title;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/edittext_note_description")
    private WebElement description;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/ll_add_picture")
    private WebElement addPhoto;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/image_click_picture")
    private WebElement clickPhoto;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/imageViewCancel")
    private WebElement cancelPhoto;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/image_picture")
    private WebElement addedImages;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/image_remove_picture")
    private List<WebElement> removePhotos;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/textview_edit_note")
    private WebElement editNote;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/edittext_search_notes_diary")
    private WebElement searchInput;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/textview_save_edit_note")
    private WebElement saveEditNote;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/image_picture")
    private WebElement uploadedImages;
    @AndroidFindBy(xpath = "//*[@content-desc=\"Today's plan\"]")
    private WebElement todayPlan;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/textview_diary_item_title")
    private WebElement noteTitle;

    private By talkSoon(String title) {
        return By.xpath("//*[contains(@text,\"" + title + "\")]/../..//android.view.ViewGroup//*[@text='Talk soon']");
    }

    public By selectType(String name) {
        return By.xpath("//*[@resource-id='com.biofourmis.careathomerpm:id/ll_note_icon_main']//*[@text='" + name + "']");
    }

    public boolean navigateToDiary() throws Exception {
        _click(diary);
        return _isElementVisible(dairyHeader);
    }

    public boolean verifyAddNewNoteButtonDisplayed() throws Exception {
        return _isElementEnable(addNewNote);
    }

    public boolean verifySearchDisplayed() throws Exception {
        return _isElementEnable(search);
    }

    public boolean addNewDiaryNoteDetails(Map<String, String> diaryNote, boolean isRequiredOnly) throws Exception {
        _sendKeys(title, diaryNote.get("Title"));
        _sendKeys(description, diaryNote.get("Description"));
        if (!isRequiredOnly) {
            _click(selectType(diaryNote.get("Type")));
            addImage(Integer.parseInt(diaryNote.get("Images")));
        }
        _click(saveNote);
        _waitForLoading();
        return true;
    }

    private void addImage(int imagesCount) throws Exception {
        for (int i = 0; i < imagesCount; i++) {
            int size = _getElementSize(addedImages);
            _click(addPhoto);
            if (i == 0)
                if (_isTextContainsPresent("While using the app"))
                    _clickOnTextContains("While using the app");
            _click(clickPhoto);
            _waitForLoading();
            _sleep(2);
            System.out.println(_getElementSize(addedImages));
            System.out.println("Actual :" + size);
            Assert.assertTrue(_getElementSize(addedImages) > size,
                    "Clicked image should be added and displayed in diary note.");
        }
    }

    public void clickOnAddNote() throws Exception {
        _click(addNewNote);
    }

    public boolean verifyAddNotePage(String text) {
        return _isTextContainsPresent(text);
    }

    public boolean verifyNoteAddedSuccessfully(String title) throws Exception {
        searchNote(title);
        if (_isElementPresent(noteTitle))
            return _getText(noteTitle).strip().equalsIgnoreCase(title);
        else
            return !_isTextContainsPresent("No notes found!");
    }

    public void searchNote(String title) throws Exception {
        _waitForLoading();
        _click(search);
        //_clear(searchInput);
        _sendKeys(searchInput, title);
        _waitForLoading();
    }

    public boolean verifyNoteDetails(Map<String, String> defaultDiaryNoteDetails, boolean isRequired, boolean isUpdated) throws Exception {
        String title = defaultDiaryNoteDetails.get("Title");
        String description = defaultDiaryNoteDetails.get("Description");
        String images = defaultDiaryNoteDetails.get("Images");
        //TODO Verify Dat time of creation
        _clickOnTextContains(title);
        //TODO Verify Dat time of creation
        Assert.assertTrue(_isTextContainsPresent(title),
                "Diary note title [" + title + "] should be displayed.");
        Assert.assertTrue(_isTextContainsPresent(description),
                "Diary note description [" + description + "] should be displayed.");
        if (!isRequired) {
            Assert.assertEquals(Integer.parseInt(images), _getElementSize(uploadedImages),
                    "[" + images + "] images should be added and displayed.");
        }
        Assert.assertTrue(_isTextContainsPresent("Back to Diary"),
                "[Back to Diary] option should be displayed.");
        _clickOnTextContains("Back to Diary");
        return _isTextContainsPresent(description);
    }

    public void openNote(String title) throws Exception {
        searchNote(title);
        _waitForLoading();
        _clickOnTextContains(title);
    }

    public int updateNoteDetails(Map<String, String> updateNote, boolean isRequired) throws Exception {
        _click(editNote);
        _sendKeys(title, updateNote.get("Title"));
        _clear(description);
        _sendKeys(description, updateNote.get("Description"));
        int return_value = 0;
        if (!isRequired) {
            _click(selectType(updateNote.get("Type")));

            int size = _getElementSize(addedImages);
            if (size > 3) {
                removePhotos.get(0).click();
                return_value = -1;
            } else {
                size = _getElementSize(addedImages);
                _click(addPhoto);
                if (_isTextContainsPresent("While using the app"))
                    _clickOnTextContains("While using the app");
                _click(clickPhoto);
                _waitForLoading();
                _sleep(2);
                Assert.assertTrue(_getElementSize(addedImages) > size,
                        "Clicked image should be added and displayed in diary note.");
                return_value = 1;
            }
        }
        _click(saveEditNote);
        _waitForLoading();
        return return_value;
    }

    public boolean verifyResponse(String title) throws Exception {
        while (!_isElementVisible(todayPlan))
            _pressBack();

        _click(todayPlan);
        _waitForElementVisible(diary);
        _click(diary);
        searchNote(title);
        _waitForLoading();
        Assert.assertTrue(_isElementVisible(talkSoon(title)),
                "[Talk soon] message should be displayed for diary note [" + title + "].");
        _clickOnTextContains(title);
        _waitForLoading();
        return _isTextContainsPresent("Talk soon");
    }

    public boolean isOptionDisplayed(String getString) {
        return _isTextContainsPresent(getString);
    }

    public void addNoteTitle(String s) throws Exception {
        _sendKeys(title, s);
    }

    public void addDescription(String s) throws Exception {
        _sendKeys(description, s);
    }

    public void iconType(String s) throws Exception {
        _click(selectType(s));
    }

    public void addImg(int i) throws Exception {
        addImage(i);
    }

    public boolean isSaveNoteBtnEnable() throws Exception {
        _click(saveNote);
        _waitForLoading();
        return !_isElementVisible(saveNote);
    }

    public void clickOnCancelBtn() throws Exception {
        _click(cancel);
    }

    public boolean isEditButtonVisible() {
        return _isElementVisible(editNote);
    }

    public void clickOnBackToDiary() {
        _pressBack();
    }
}
