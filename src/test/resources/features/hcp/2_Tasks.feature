Feature: Tasks HCP App

  Background:
    Given Launch the "HCP" App with noReset
    Given Launch the browser
    And Login into dashboard

  Scenario: Prerequisite for automation
    Given Add a new patient
    Then Login into HCP app

  @TestCaseKey=HOHP-T588
  Scenario:Verify Task management section in HCP mobile
    And Search My patient in HCP APP and open View tasks
    Then Verify Tasks screen content
    And Go back to home screen and navigate to Tasks section
    Then Verify Tasks screen content

  @TestCaseKey=HOHP-T668 @TestCaseKey=HOHP-T1384
  Scenario:Create new pending task in HCP mobile and check in all other platforms
    And Search My patient in HCP APP and open View tasks
    When Add new task with required fields
    Then Navigate to Tasks tab on HCP web dashboard
    Then verify created task in HCP web

  Scenario: To verify Add, Edit, Comment and Complete a task with required fields on patient details page
    Then Search patient name under tasks
    And Verify create new task in HCP app with Required fields
    Then Verify comment on created task in HCP app
    And Verify edit a task in HCP app with Required fields
    Then Verify complete a task in HCP app
    And Verify comment on completed task in HCP app
    Then Verify re-open completed task in HCP app
    And Verify comment on re-opened task in HCP app
    Then Verify edit re-open task in HCP app with All fields
    Then Verify complete a re-open task in HCP app

  Scenario: To verify Add, Edit, Comment and Complete a task with all fields on patient details page
    Then Search patient name under tasks
    And Verify create new task in HCP app with All fields
    Then Verify comment on created task in HCP app
    And Verify edit a task in HCP app with All fields
    Then Verify complete a task in HCP app
    And Verify comment on completed task in HCP app
    Then Verify re-open completed task in HCP app
    And Verify comment on re-opened task in HCP app
    Then Verify edit re-open task in HCP app with Required fields
    Then Verify complete a re-open task in HCP app


