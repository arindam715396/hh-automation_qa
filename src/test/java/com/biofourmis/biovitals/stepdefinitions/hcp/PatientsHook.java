package com.biofourmis.biovitals.stepdefinitions.hcp;

import com.biofourmis.biovitals.constants.configuration.TestConfig;
import com.biofourmis.biovitals.pagefactory.hcp.PatientsPage;
import com.biofourmis.biovitals.utilities.CustomUtils;
import com.github.javafaker.Faker;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;

import java.util.Objects;

@Log4j
public class PatientsHook {

    @Autowired(required = false)
    @Qualifier(value = "HCP")
    private WebDriver clinician;

    @Autowired
    private TestConfig testConfig;
    @Autowired
    private Faker faker;

    private CustomUtils customUtils = new CustomUtils(null);
    private PatientsPage patientsPage;

    @And("^Search (My|All|Unassigned) patient in HCP APP and open (Clinical notes|Handoff patient|View tasks|Remove from My patients|Cancel)$")
    public void searchPatientInHCPAPPAndOpenClinicalNotes(String arg1, String arg0) throws Exception {
        patientsPage = new PatientsPage(clinician);

        if (Objects.isNull(CustomUtils.mnr)) {
            CustomUtils.mnr = testConfig.getPrimaryPatientMRN();
            customUtils.setPatientDetails(testConfig.getPrimaryPatientMRN(), testConfig.getPrimaryPatientName());
        }

        if (arg1.equalsIgnoreCase("My")) {
            log.info("Search patient with MNR number.");
            patientsPage.searchPatient(CustomUtils.mnr);
        } else if (arg1.equalsIgnoreCase("All")) {
            log.info("Navigate to all patients.");
            patientsPage.navigateToAllPatients();

            log.info("Search patient with MNR number.");
            patientsPage.searchPatient(CustomUtils.mnr);
        } else {
            log.info("Navigate to all patients.");
            patientsPage.navigateToAllPatients();

            log.info("Search patient with MNR number.");
            patientsPage.searchPatient(testConfig.getUnassignedPatientMRN());
        }

        log.info("Open menu option for searched patient.");
        patientsPage.openPatientMenu();
        log.info("Click on option [" + arg0 + "].");
        patientsPage.clickOnMenu(arg0);

        //XCUIElementTypeStaticText[@name="AOx3"]
        //XCUIElementTypeStaticText[@name="AOx3"]/../..//XCUIElementTypeOther
        //ancestor::*[*[@label="AOx3"]]
        //*[@label='AOx3']//ancestor::XCUIElementTypeCell
    }

    @And("search patient in HCP APP")
    public void searchPatientInHCPAPP() throws Exception {
        patientsPage = new PatientsPage(clinician);

        if (Objects.isNull(CustomUtils.mnr)) {
            CustomUtils.mnr = testConfig.getPrimaryPatientMRN();
            customUtils.setPatientDetails(testConfig.getPrimaryPatientMRN(), testConfig.getPrimaryPatientName());
        }

        log.info("search patient with Id " + CustomUtils.mnr);
        patientsPage.searchPatient(CustomUtils.mnr);
    }


    @And("Search patient in all patients in HCP APP and open Handoff patient")
    public void searchPatientInAllPatientsInHCPAPPAndOpenHandoffPatient() throws Exception {
        patientsPage = new PatientsPage(clinician);

        log.info("Navigate to all patients.");
        patientsPage.navigateToAllPatients();

        log.info("search patient with Id " + testConfig.getUnassignedPatientMRN());
        patientsPage.searchPatient(testConfig.getUnassignedPatientMRN());
    }

    @Then("click on cancel option and verify it navigates to patients list")
    public void clickOnCancelOptionAndVerifyItNavigatesToPatientsList() throws Exception {
        patientsPage = new PatientsPage(clinician);

        log.info("Open menu option for searched patient.");
        patientsPage.openPatientMenu();

        log.info("Click on option [Cancel].");
        patientsPage.clickOnCancel();

        log.info("Close search option.");
        patientsPage.cancelSearchOption();

        log.info("Patient list should be displayed.");
        Assert.assertTrue(patientsPage.isPatientPageDisplayed(), "Patient list not displayed.");
    }
}
