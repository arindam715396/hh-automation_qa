package com.biofourmis.biovitals.utilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.*;

import java.time.Duration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class is read only do not modify anything here
 */
@Log4j
public class SwipeUtils {

    private WebDriver driver;
    /* Swipe down START and END percentage */
    int[] SWIPE_DOWN_START = {50, 32};
    int[] SWIPE_DOWN_END = {50, 80};

    /* Swipe up START and END percentage */
    int[] SWIPE_UP_START = {50, 80};
    int[] SWIPE_UP_END = {50, 35};

    /* Swipe right START and END percentage */
    int[] SWIPE_RIGHT_START = {10, 50};
    int[] SWIPE_RIGHT_END = {85, 50};

    /* Swipe left START and END percentage */
    int[] SWIPE_LEFT_START = {85, 30};
    int[] SWIPE_LEFT_END = {10, 50};

    int maxScroll = 10;

    public SwipeUtils(WebDriver driver) {
        this.driver = driver;
    }

    public void _termsAndConditionScrollDown(MobileElement element) throws Exception {
        maxScroll = 80;
        int scroll = 0;
        double normalizer = 1;
        while (!element.isEnabled() && scroll <= maxScroll) {
            this._swipeUp(normalizer);
            scroll++;
        }

        if (scroll > maxScroll) {
            throw new Exception("Element not found after " + maxScroll + "scrolls");
        }
    }

    public void _checkIfDisplayedWithScrollDown(MobileElement element) throws Exception {
        By elem = this._convertMobElemToBy(element);
        int scroll = 0;
        double normalizer = 1;
        do {
            this._swipeUp(normalizer);
            scroll++;
        } while (!(this.driver.findElements(elem).size() > 0) && scroll <= maxScroll);

        if (scroll > maxScroll) {
            throw new Exception("Element not found after " + maxScroll + "scrolls");
        }
    }

    public void _checkIfDisplayedWithScrollUp(MobileElement element) throws Exception {
        By elem = this._convertMobElemToBy(element);
        int scroll = 1;
        double normalizer = 1;
        do {
            this._swipeDown(normalizer);
            scroll++;
        } while (!(this.driver.findElements(elem).size() > 0) && scroll <= maxScroll);

        if (scroll > maxScroll) {
            throw new Exception("Element not found after " + maxScroll + "scrolls");
        }
    }

    public void _checkIfDisplayedWithScrollRight(MobileElement element) throws Exception {
        By elem = this._convertMobElemToBy(element);
        int scroll = 0;
        double normalizer = 1;
        do {
            this._swipeLeft(normalizer);
            scroll++;
        } while (!(this.driver.findElements(elem).size() > 0) && scroll <= maxScroll);

        if (scroll > maxScroll) {
            throw new Exception("Element not found after " + maxScroll + "scrolls");
        }
    }

    public void _checkIfDisplayedWithScrollLeft(MobileElement element) throws Exception {
        By elem = this._convertMobElemToBy(element);
        int scroll = 0;
        double normalizer = 1;
        do {
            this._swipeRight(normalizer);
            scroll++;
        } while (!(this.driver.findElements(elem).size() > 0) && scroll <= maxScroll);

        if (scroll > maxScroll) {
            throw new Exception("Element not found after " + maxScroll + "scrolls");
        }
    }

    private int[] calculateXY(int[] coordinates, double normalizer) {
        return new int[]{(int) (coordinates[0] * normalizer), (int) (coordinates[1] * normalizer)};
    }

    private int[] getDeviceScreenCoordinates(Dimension screenSize, int[] cords) {
        int x = (int) (screenSize.getWidth() * ((float) cords[0] / 100));
        int y = (int) (screenSize.getHeight() * ((float) cords[1] / 100));
        return new int[]{x, y};
    }

    @SuppressWarnings("unchecked")
    private void swipe(int[] from, int[] to) {
        AppiumDriver<MobileElement> mob = (AppiumDriver<MobileElement>) this.driver;
        TouchAction touchAction = new TouchAction((PerformsTouchActions) this.driver);
        String platform = mob.getPlatformName();
        if (platform != null && platform.equalsIgnoreCase("ios"))
            touchAction.longPress(PointOption.point(from[0], from[1]));
        else
            touchAction.press(PointOption.point(from[0], from[1]));

        touchAction.waitAction(WaitOptions.waitOptions(Duration.ofMillis(0)))
                .moveTo(PointOption.point(to[0], to[1]))
                .release()
                .perform();
    }

    /**
     * This is a private method is used to give the actual coordinates present on the screen for a horizontal sleekbar
     * @param element This parameter is to give the presence of sleekbar on screen to help to get the coordinates
     * @return y axis coordinate because horizontally x-axis will remain same
     */
    private int _getSliderCoordinates(MobileElement element) {
        Point point = element.getLocation();
        return point.getY() + 40;
    }

    /**
     * This method is used to slide the horizontal sleekbar present on the screen after getting the screen size
     *
     * @param num     This parameter tells the level of severity of disease and every severity is approx 180 far from each other with 25 percent of x-axis
     * @param element This is the second parameter is to give the presence of sleekbar on screen to help to get the coordinates
     * @return nothing
     */
    public void _swipeHorizontalSleekSlider(int num, MobileElement element) {
        Dimension screenSize = driver.manage().window().getSize();
        TouchAction touchAction = new TouchAction((PerformsTouchActions) driver);
        touchAction.tap(PointOption.point(((int) (screenSize.getWidth() * 25 / 100) + num * 180), _getSliderCoordinates(element))).perform();
    }

    /**
     * This method is used to slide the vertical sleekbar present on the screen
     *
     * @param actualNum This parameter tells the value to move the sleek bar vertically across the y-axis
     * @param element   This is the second parameter is to give the presence of sleekbar on screen to help to get the coordinates
     * @return nothing
     */
    public void _swipeVerticalSleekSlider(MobileElement element, int actualNum) {
        int num = 100 - actualNum;
        Point verticalSleek = element.getLocation();
        TouchAction touchAction = new TouchAction((PerformsTouchActions) driver);
        touchAction.tap(PointOption.point(verticalSleek.getX(), (int) (verticalSleek.getY() + (num * 9.38)))).perform();
    }

    private void swipePercentage(int[] from, int[] to) {
        Dimension screenSize = driver.manage().window().getSize();
        int[] start = this.getDeviceScreenCoordinates(screenSize, from);
        int[] end = this.getDeviceScreenCoordinates(screenSize, to);
        this.swipe(start, end);
    }

    public void _swipeUp(double normalizer) {
        this.swipePercentage(
                this.calculateXY(SWIPE_UP_START, normalizer),
                this.calculateXY(SWIPE_UP_END, normalizer)
        );
    }

    public void _swipeDown(double normalizer) {
        this.swipePercentage(
                this.calculateXY(SWIPE_DOWN_START, normalizer),
                this.calculateXY(SWIPE_DOWN_END, normalizer)
        );
    }

    public void _swipeLeft(double normalizer) {
        this.swipePercentage(
                this.calculateXY(SWIPE_LEFT_START, normalizer),
                this.calculateXY(SWIPE_LEFT_END, normalizer)
        );
    }

    public void _swipeRight(double normalizer) {
        this.swipePercentage(
                this.calculateXY(SWIPE_RIGHT_START, normalizer),
                this.calculateXY(SWIPE_RIGHT_END, normalizer)
        );
    }

    protected By _convertElemToBy(WebElement element) throws Exception {
        String locator;
        StringBuilder elem = new StringBuilder();
        String eleString = element.toString();
        if (eleString.contains("Proxy element for: DefaultElementLocator")) {
            String elemString = eleString.replace("Proxy element for: DefaultElementLocator ", "");
            String[] trimmedText = elemString.substring(1, elemString.length() - 1).split(":");
            locator = trimmedText[0].replace("By.", "").trim();
            elem = new StringBuilder(trimmedText[1].trim());
        } else if (eleString.contains("Located by By.chained")) {
            Matcher m = Pattern.compile("\\{(.*?)\\}").matcher(eleString);
            String matchedString = "";
            while (m.find()) {
                matchedString = m.group(1);
            }
            String[] trimmedText = matchedString.split(": ");
            locator = trimmedText[0].replace("By.", "").trim();
            elem = new StringBuilder(trimmedText[1].trim());
        } else if (eleString.contains("Located by By")) {
            String elemString = eleString.replace("Located by By.", "");
            String[] trimmedText = elemString.split(":");
            locator = trimmedText[0].trim();
            for (int i = 1; i < trimmedText.length; i++)
                if (i != trimmedText.length-1)
                    elem.append(trimmedText[i].trim()).append(":");
                else
                    elem.append(trimmedText[i].trim());
           // elem = new StringBuilder(trimmedText[1].trim());
        } else {
            String[] trimmedText = (eleString.split("->")[1]).split(":");
            locator = trimmedText[0].trim();
            elem = new StringBuilder(trimmedText[1].trim().replaceAll(".$", ""));
        }
        return this.getByElement(locator, elem.toString());
    }

    protected By _convertMobElemToBy(MobileElement element) throws Exception {
        Matcher m = Pattern.compile("\\{(.*?)\\}").matcher(element.toString());
        String matchedString = "";
        while (m.find()) {
            matchedString = m.group(1);
        }
        String[] splitText = matchedString.split(":");
        return this.getByElement(splitText[0].trim().substring(3), splitText[1].trim());
    }

    protected By getByElement(String locator, String element) throws Exception {
        switch (locator.toLowerCase()) {
            case "id": {
                return By.id(element);
            }
            case "name": {
                return By.name(element);
            }
            case "classname": {
                return By.className(element);
            }
            case "linktext": {
                return By.linkText(element);
            }
            case "partiallinktext": {
                return By.partialLinkText(element);
            }
            case "cssselector": {
                return By.cssSelector(element);
            }
            case "xpath": {
                return By.xpath(element);
            }
            default: {
                throw new Exception("Unknown locator type '" + locator + "'");
            }
        }
    }
}