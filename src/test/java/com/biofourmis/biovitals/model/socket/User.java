package com.biofourmis.biovitals.model.socket;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class User {

    String _id;
    String _schema;
}
