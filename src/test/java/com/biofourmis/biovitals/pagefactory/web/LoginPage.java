package com.biofourmis.biovitals.pagefactory.web;

import com.biofourmis.biovitals.utilities.CommonUtils;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

@Log4j
public class LoginPage extends CommonUtils {

    private WebDriver driver;

    public LoginPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "email")
    WebElement username;

    @FindBy(id = "password")
    WebElement password;

    @FindBy(xpath = "//*[@id='loginBtn']")
    WebElement loginBtn;

    @FindBy(id = "confirm")
    private WebElement confirm;

    @FindBy(id = "emailErrorInfo")
    WebElement emailErrorInfo;

    @FindBy(id = "emailRequiredInfo")
    WebElement emailRequiredInfo;

    @FindBy(id = "minPasswordInfo")
    WebElement minPasswordInfo;

    @FindBy(id = "passwordRequiredInfo")
    WebElement passwordRequiredInfo;

    @FindBy(id = "loginError")
    WebElement invalidLoginErr;

    @FindBy(id = "add-patient")
    WebElement addPatient;

    @FindBy(xpath = "//*[contains(@class,'profile-info')]")
    WebElement userProfile;

    @FindBy(xpath = "//*[contains(text(),'Log out')]")
    WebElement logout;

    @FindBy(xpath = "//img[@src='assets/icons/eye-close.svg']")
    WebElement pwdEye;
    @FindBy(xpath = "//*[contains(@class,'loader')]")
    private WebElement loader;


    public void login(String email, String pwd) throws Exception {
        if (!_isElementVisible(loginBtn))
            _refreshPage();

        _sendKeys(username, email);
        _sendKeys(password, pwd);
        _click(loginBtn);
        _waitForLoading();
        _waitForElementInvisible(loader);
        Assert.assertTrue(_waitForElementInvisible(loginBtn), "Not able to login into dashboard.");
        _waitForPageLoad();
    }

    public void enterMail(String email) throws Exception {
        _sendKeys(username, email);
    }

    public void enterPass(String pwd) throws Exception {
        _sendKeys(password, pwd);
    }

    public boolean verifyHomePage() {
        return _isElementPresent(addPatient);
    }

    public boolean logOut() throws Exception {
        if (!_isElementVisible(userProfile))
            _refreshPage();
        _sleep(10);
        _waitForPageLoad();
        _waitForLoading();
        _waitForElementVisible(userProfile);
        _click(userProfile);
        _click(logout);
        _waitForLoading();
        if (_waitForElementVisible(confirm))
            _click(confirm);
        else
            return false;
        return true;
    }

    public boolean loginPageText(String textValues) {
        _waitForPageLoad();
        if (textValues.equalsIgnoreCase("your@email.com"))
            return _isElementVisible(By.xpath("//*[@placeholder='" + textValues + "']"));
        else
            return _isTextPresent(textValues);
    }

    public boolean isLoginButtonEnable() throws Exception {
        return _isElementEnable(loginBtn);
    }

    public void enterUserName(String value) throws Exception {
        _sendKeys(username, value);
    }

    public void enterPassword(String value) throws Exception {
        _sendKeys(password, value);
    }

    public void clearPassword() throws Exception {
        _clear(password);
    }

    public void clearUserName() throws Exception {
        _clear(username);
    }

    public boolean verifyPasswordFormat(String value) throws Exception {
        _clear(password);
        _sendKeys(password, value);
        _click(username);
        try {
            _click(pwdEye);
        } catch (Exception e) {
        }
        try {
            return _getText(minPasswordInfo).trim().equals("Password is invalid");
        } catch (Exception e) {
            return false;
        }
    }

    public boolean verifyUserNameFormat(String value) throws Exception {
        _clear(username);
        _sendKeys(username, value);
        _click(password);
        return _getText(emailErrorInfo).trim().equals("Email address is invalid");
    }

    public boolean verifyUserNameRequired(String value, String message) throws Exception {
        _sendKeys(username, value);
        while (username.getAttribute("value").length() != 0)
            driver.switchTo().activeElement().sendKeys(Keys.BACK_SPACE);

        _click(password);
        return _getText(emailRequiredInfo).trim().equals(message);
    }

    public boolean verifyPasswordRequired(String value, String message) throws Exception {
        _sendKeys(password, value);
        while (password.getAttribute("value").length() != 0)
            driver.switchTo().activeElement().sendKeys(Keys.BACK_SPACE);

        _click(username);
        return _getText(passwordRequiredInfo).trim().equals(message);
    }

    public void clickOnLogin() throws Exception {
        _click(loginBtn);
    }

    public String unMaskedPwd() {
        return password.getAttribute("type");
    }

    public void clickPwdEye() throws Exception {
        _click(pwdEye);
    }

    public String getPassword() throws Exception {
        return _getAttributeValue(password, "value");
    }


    public String loginErrMsg() throws Exception {
       // return driver.findElement(By.xpath("//*[@id='loginError']")).getText();
        return _getText(invalidLoginErr);
    }

    public boolean isLoginAttemptMessage(int count) {
        try {
            return _isTextContainsPresent("You have " + count + " more password attempts. Once the limit is reached, your account will be locked for 5 minutes");
        }
        catch (Exception e)
        {
            _sleep(1);
            return _isTextContainsPresent("You have " + count + " more password attempts. Once the limit is reached, your account will be locked for 5 minutes");
        }
    }

    public boolean isAutoLoggedOutMessage(){
        return _isTextContainsPresent("You've been logged out as you're currently logged in from another browser or device.");
    }
}
