package com.biofourmis.biovitals.stepdefinitions.patient;

import com.biofourmis.biovitals.pagefactory.patient.TodaysPlanPage;
import com.biofourmis.biovitals.utilities.CustomUtils;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;

import java.util.LinkedHashMap;
import java.util.Map;

@Log4j
public class TodaysPlanHook {

    @Autowired(required = false)
    @Qualifier(value = "HH")
    private WebDriver patient;

    private TodaysPlanPage todaysPlanPage;
    private CustomUtils customUtils = new CustomUtils(null);

    @Then("^Verify (Added|Deleted) Care plan displayed in Patient App$")
    public void verifyAddedCarePlanDisplayedInPatientApp(String arg0) throws Exception {
        todaysPlanPage = new TodaysPlanPage(patient);
        String carePlan = customUtils.getCarePlan();

        log.info("Verify " + arg0 + " care plan.");
        if (arg0.equals("Added"))
            Assert.assertTrue(todaysPlanPage.verifyCarePlanDisplayed(carePlan),
                    carePlan + " should be displayed on Today's plan screen.");
        else
            Assert.assertFalse(todaysPlanPage.verifyCarePlanDisplayed(carePlan),
                    carePlan + " should not be displayed on Today's plan screen.");
    }

    @And("Verify added activity {string} in patient app")
    public void verifyAddedActivityInPatientApp(String arg0) throws Exception {
        todaysPlanPage = new TodaysPlanPage(patient);

        Assert.assertTrue(todaysPlanPage.isActivityDisplayed(arg0),
                arg0 + " activity should be displayed.");
        Map<String, String> activity = new LinkedHashMap<>();
        for (Map<String, String> data : customUtils.getActivityDetails())
            if (data.get("Name").equals(arg0)) {
                activity.putAll(data);
                break;
            }

        String text = todaysPlanPage.getActivityDetails(arg0);
        text = text.replace("am", "AM")
                .replace("pm", "PM")
                .replaceAll("\\s", "");
        text = text.startsWith("0") ? text.substring(1, text.length()) : text;
        text = text.split(":")[1].contains("00") ? text.replaceAll("00", "").replace(":", "") : text;

        log.info("Verify activity time.");
        Assert.assertEquals(activity.get("Time"), text,
                "Activity " + arg0 + " should be added at time " + text);
    }

    @And("Verify updated activity {string} in patient app")
    public void verifyUpdatedActivityInPatientApp(String arg0) throws Exception {
        todaysPlanPage = new TodaysPlanPage(patient);

        Assert.assertTrue(todaysPlanPage.isActivityDisplayed(arg0),
                arg0 + " activity should be displayed.");
        Map<String, String> activity = new LinkedHashMap<>();
        for (Map<String, String> data : customUtils.getActivityDetails())
            if (data.get("Name").equals(arg0)) {
                activity.putAll(data);
                break;
            }

        String text = todaysPlanPage.getActivityDetails(arg0);
        text = text.replace("am", "AM").replace("pm", "PM").replaceAll("\\s", "");
        text = text.startsWith("0") ? text.substring(1, text.length()) : text;
        text = text.split(":")[1].contains("00") ? text.replaceAll("00", "").replace(":", "") : text;

        log.info("Verify activity updated time.");
        Assert.assertEquals(activity.get("Time"), text,
                "Activity " + arg0 + " should be added at updated time " + text);

    }

    @And("Verify deleted activity {string} in patient app")
    public void verifyDeletedActivityInPatientApp(String arg0) throws Exception {
        todaysPlanPage = new TodaysPlanPage(patient);
        log.info("Verify activity ["+arg0+"] not displayed in patient app.");
        Assert.assertFalse(todaysPlanPage.isActivityDisplayed(arg0),
                arg0 + " activity should not be displayed.");
    }

    @And("Go to Todays plan tab in patient app")
    public void goToTodaysPlanTabInPatientApp() throws Exception {
        todaysPlanPage = new TodaysPlanPage(patient);
        todaysPlanPage.navigateToTodays();
    }
}
