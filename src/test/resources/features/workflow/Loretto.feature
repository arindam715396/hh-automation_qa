@Loretto
Feature:Validate Loretto workflow

  Background:
    Given Launch the browser

  Scenario: Verify texts displayed on Login page
    Then Verify login screen texts

  Scenario: Verify the functionality of Login button when valid email and password is entered.
    Then Verify caregiver login successfully
    And Logout user

  Scenario: Verify the login functionality by entering invalid email and password
    When Enter the username as "Invalid@mail.com" and password as "Invalid@Care"
    And Click on the login button
    Then Verify the error message after invalid email and password

  Scenario: Verify available LHN menu and theirs respected pages
    And Login into dashboard
    Then Verify LHN menu options displayed
      | My patients  |
      | All patients |
      | Discharged   |
      | Tasks        |
      | Inventory    |
    And Verify each menu is clickable and should navigate to respected page

  Scenario: Verify Add Patient with Mandatory fields only
    And Login into dashboard
    And Add patient mandatory details
    Then Verify mandatory field details on review page
    And Save patient
    Then Verify new patient tutorial before login

  Scenario: Verify Add Patient with all details
    And Login into dashboard
    And Open Add Patient wizard
    Then Fill patient details of section Personal information
    Then Fill patient details of section Medical information
    Then Fill patient details of section Care team
    Then Fill patient details of section Equipment information
    Then Fill patient details of section Review
    Then Verify all field details on review page
    And Save patient
    Given Launch the "HH" App with reset
    Then Scan Created patient QR and login into app
    Then Verify new patient tutorial after login

  Scenario: Verify Update and Delete Clinical note
    Then Login into dashboard
    And Navigate to patient "MNR1001" under my patients
    Then Verify Add a note
    Then Verify update a note
    And Verify Search a note
    Then Verify Delete a note

  Scenario: Verify Add and Delete Care Plan
    Then Login into dashboard
    And Navigate to patient "MNR1001" under my patients
    Then Verify Add a Care plan
    And Verify Delete a Care plan

  Scenario: Verify Activity Page details on Activities under my patients
    Then Login into dashboard
    And Navigate to patient "MNR1001" under my patients
    Then Verify Activity Page details of clinic "loretto"
      | Use inhaler                |
      | Use nebulizer              |
      | Inject insulin             |
      | Check blood pressure       |
      | Check weight               |
      | Check temporal temperature |
      | Drink water                |
      | Elevate legs               |
      | Elevate arm                |
      | Home hospital team visit   |
      | Physical therapy visit     |
      | Place hot pack             |
      | Place cold pack            |
      | Walk                       |
      | Take medications           |
      | Check oxygen level         |
      | Take medication            |
      | Check blood glucose        |
      | Answer questionnaire       |

  Scenario: Verify Add, Update and Delete and Activity
    And Login into dashboard
    Then Open QR of Patient "MRN1000"
    And Scan QR and login into app
    And Navigate to patient "MRN1000" under my patients
    Then Verify Add an Activity "Drink water"
      | No of Glass |
      | 2           |
    And Verify added activity "Drink water" in patient app
    Then Verify Update and Activity "Drink water"
    And Verify updated activity "Drink water" in patient app
    Then Verify Delete and Activity "Drink water"
    And Verify deleted activity "Drink water" in patient app


  Scenario: Verify Add and Update a Diary Note with required fields
    And Login into dashboard
    Then Open QR of Patient "MRN1000"
    And Scan QR and login into app
    Then Navigate to Diary and verify components in patient app
    Then Verify add a new diary note with required fields in patient app
    And Navigate to patient "MRN1000" under my patients
    Then Verify added diary note in with required fields hcp web
    Then Verify update diary note with required fields in patient app
    And Verify updated diary note in with required fields hcp web

  Scenario: Verify Add and Update a Diary Note with all fields
    And Login into dashboard
    Then Open QR of Patient "MRN1000"
    And Scan QR and login into app
    Then Navigate to Diary and verify components in patient app
    Then Verify add a new diary note with all fields in patient app
    And Navigate to patient "MRN1000" under my patients
    And Verify added diary note in with not required fields hcp web
    Then Verify update diary note with not required fields in patient app
    And Verify updated diary note in with not required fields hcp web

  Scenario: Verify respond a Diary Note from HCP web
    And Login into dashboard
    Then Open QR of Patient "MRN1000"
    And Scan QR and login into app
    Then Navigate to Diary and verify components in patient app
    Then Verify add a new diary note with required fields in patient app
    And Navigate to patient "MRN1000" under my patients
    Then Verify respond diary note from hcp web
    And Verify response on diary note in patient app

  Scenario: Validate settings screen
    And Login into dashboard
    Then Open QR of Patient "MRN1000"
    And Scan QR and login into app
    Then Navigate to Hidden settings screen
    And Verify Hidden setting screen elements