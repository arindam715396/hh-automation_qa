package com.biofourmis.biovitals.stepdefinitions.patient;

import com.biofourmis.biovitals.pagefactory.patient.LoginPage;
import com.biofourmis.biovitals.pagefactory.patient.SettingsPage;
import com.biofourmis.biovitals.pagefactory.web.HomePage;
import com.biofourmis.biovitals.utilities.CustomUtils;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;

@Log4j
public class LoginHook {

    @Autowired(required = false)
    @Qualifier(value = "HH")
    private WebDriver patient;

    @Autowired(required = false)
    @Qualifier(value = "web")
    private WebDriver web;

    private CustomUtils customUtils = new CustomUtils(null);
    private LoginPage login;
    private HomePage homePage;

    @And("^Scan QR and login into app$")
    public void login() throws Exception {
        login = new LoginPage(patient);
        log.info("Get QR Hash URL.");
        String qr = customUtils.getQrHashUrl();

        log.info("Login into patient app.");
        login.login(customUtils.getQRHashDetails(qr));

        log.info("Verify patient logged in successfully.");
        Assert.assertFalse(login.verifyLoginSuccessfully(),
                "Not able to login.");

        log.info("Accept Permissions and skip.");
        login.acceptLoginPermissions();
        log.info("Click on Done");
        login.clickOnDone();
        Assert.assertTrue(login.isTodayPlanDisplayed(),
                "Not able to login into patient app.");
    }

    @And("^Scan Created patient QR and login into app$")
    public void LoginCurrentUser() throws Exception {
        homePage = new HomePage(web);
        log.info("Login into patient app.");
        login();
    }

    @Then("Logout patient")
    public void logoutPatient() throws Exception {
        SettingsPage settingsPage = new SettingsPage(patient);
        login = new LoginPage(patient);
        log.info("Navigate to Hidden Setting page.");
        settingsPage.navigateToHiddenSettings();
        log.info("Log out user.");
        Assert.assertTrue(login.logOut(),
                "Not able to logout from patient app.");
    }

    @Given("Prepare app for reset")
    public void prepareAppForReset() {
        SettingsPage settingsPage = new SettingsPage(patient);
        settingsPage._sleep(20);
    }
}
