Feature: Patient Authentication

  Background:
    Given Launch the browser
    And Login into dashboard
    Then Add a new patient
    When Launch the "HH" App with reset


  Scenario: To verify login/logout into HH Patient app
    And Scan QR and login into app
    Then Logout patient