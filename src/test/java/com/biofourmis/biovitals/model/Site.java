package com.biofourmis.biovitals.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class Site {

    String id;
    String name;
    String[] modules;
    @JsonIgnore
    String appId;
}
