Feature: Patient app [Setting] screen verification

  Background:
    Given Launch the browser
    And Login into dashboard
    Given Launch the "HH" App with noReset

  Scenario: To verify hidden settings screen
    Then Navigate to Hidden settings screen
    And Verify Hidden setting screen elements