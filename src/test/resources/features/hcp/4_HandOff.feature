Feature: Hand off patient feature HCP App

  Background:
    Given Launch the browser
    Given Login into dashboard
    Given Launch the "HCP" App with reset

  Scenario: Prerequisite for automation
    Given Add a new patient
    Then Login into HCP app

  @TestCaseKey=HOHP-T572
  Scenario: Verify that user should be able to get back to patient list screen on selecting cancel option
    And search patient in HCP APP
    Then click on cancel option and verify it navigates to patients list

  @TestCaseKey=HOHP-T573 @TestCaseKey=HOHP-T574
  Scenario: Verify that user should be able to view Patient hand off screen in HCP mobile
    And Search My patient in HCP APP and open Handoff patient
    Then verify Handoff patient screen displayed

  @TestCaseKey=HOHP-T576
  Scenario: Verify cancelling patient hand off operation in patient hand off screen
    And Search My patient in HCP APP and open Handoff patient
    Then verify cancelling hand off after filling all details

  @TestCaseKey=HOHP-T575 @TestCasekey=HOHP-T587 @TestCaseKey=HOHP-T580 @TestCaseKey=HOHP-T582
  Scenario: Verify user should be able to successfully create a patient hand off in the HCP mobile
    And Search My patient in HCP APP and open Handoff patient
    Then verify hand off after filling all details
    And verify hand off details on web dashboard

  @TestCaseKey=HOHP-T577 @TestCaseKey=HOHP-T578
  Scenario: Verify "No changes from prior visit" in patient handoff screen
    And Search My patient in HCP APP and open Handoff patient
    Then verify Cancel hand off when No changes from prior visit option is selected
    And verify Proceed hand off when No changes from prior visit option is selected

  @TestCaseKey=HOHP-T579
  Scenario: Verify patient handoff screen without saving any changes
    And Search My patient in HCP APP and open Handoff patient
    Then Proceed with No changes from prior visit option and press back on home screen

  @TestCaseKey=HOHP-T1238
  Scenario: Verify Patient hand off screen for All Patients in the HCP mobile.
    And Search Unassigned patient in HCP APP and open Handoff patient
    Then verify user cannot perform hand off for patient not in carer giver list

  @TestCaseKey=HOHP-T584 @TestCaseKey=HOHP-T581
  Scenario: Verify updating existing patient handoff at HCP mobile is reflecting in both HCP mobile and HCP web
    And Search My patient in HCP APP and open Handoff patient
    Then verify hand off after updating all details
    And verify hand off details on web dashboard

  @TestCaseKey=HOHP-T585
  Scenario: Verify updating existing patient handoff at HCP web is reflecting in both HCP mobile and HCP web
    And Navigate to patient under my patients
    Then Update HandOff details with "Unstable" Illness severity and hand off
    And Verify HandOff details after successfully HandOff patient
    And Search My patient in HCP APP and open Handoff patient
    Then verify hand off details on HCP app

  @TestCaseKey=HOHP-T586
  Scenario: Verify auto refresh in the patient handoff screen in HCP mobile
    And Search My patient in HCP APP and open Handoff patient
    Then Navigate to patient under my patients
    When Update HandOff details with "Watcher" Illness severity and hand off
    Then verify auto refresh pop-up displayed in HCP app
    And after reload all changes reflect in app