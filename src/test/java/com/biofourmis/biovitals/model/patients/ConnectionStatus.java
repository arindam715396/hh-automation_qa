package com.biofourmis.biovitals.model.patients;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ConnectionStatus {

    Integer everion;
    Integer vitalPatch;
}
