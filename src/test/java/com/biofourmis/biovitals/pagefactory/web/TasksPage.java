package com.biofourmis.biovitals.pagefactory.web;

import com.biofourmis.biovitals.utilities.CommonUtils;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.time.LocalDateTime;
import java.util.Map;

@Log4j
public class TasksPage extends CommonUtils {

    private WebDriver driver;

    public TasksPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//*[@type=\"task-ic\"]")
    private WebElement taskIcon;
    @FindBy(xpath = "//*[@type=\"ic-close\"]")
    private WebElement closeIcon;
    @FindBy(xpath = "//*[contains(@class,\"patient-task\")]//*[contains(text(),'New task')]")
    private WebElement newTask;
    @FindBy(id = "description")
    private WebElement description;
    @FindBy(xpath = "//*[@placeholder=\"Leave a comment\"]")
    private WebElement commentInput;
    @FindBy(xpath = "//*[contains(@class,\"comment-input\")]//*[@type='send']")
    private WebElement sendComment;
    @FindBy(xpath = "//*[@type=\"ic-date\"]")
    private WebElement calendarIcon;

    @FindBy(xpath = "//*[@aria-label=\"Add a hour\"]")
    private WebElement addOneHour;
    @FindBy(xpath = "//*[@aria-label=\"Minus a hour\"]")
    private WebElement minusOneHour;

    @FindBy(xpath = "//*[@aria-label=\"Add a minute\"]")
    private WebElement addOneMinute;
    @FindBy(xpath = "//*[@aria-label=\"Minus a minute\"]")
    private WebElement minusOneMinute;
    @FindBy(xpath = "//*[@formcontrolname=\"assignedTo\"]")
    private WebElement assigneeTo;
    @FindBy(xpath = "//*[@formcontrolname=\"assignedTo\"]//input")
    private WebElement assigneeToInput;
    @FindBy(xpath = "//*[@title=\"Clear all\"]")
    private WebElement clearAssignee;
    @FindBy(xpath = "//*[@type=\"ic-close-dark\"]")
    private WebElement clearDate;
    @FindBy(xpath = "//*[@formcontrolname='dueDate']/..//div")
    private WebElement dueDate;
    @FindBy(xpath = "//*[contains(@class,\"completed-tasks\")]//*[contains(text(),'Completed')]")
    private WebElement completed;
    @FindBy(xpath = "//*[@placeholder='Leave a comment']")
    private WebElement commentPlaceholder;
    @FindBy(xpath = "//*[contains(@class,'form-opened')]//*[@type='ic-edit-b']")
    private WebElement editIcon;
    @FindBy(xpath = "//*[contains(@class,'form-opened')]//*[text()='Edit']")
    private WebElement editText;

    public void clickOnTaskIcon() throws Exception {
        _waitForPageLoad();
        _pageWait(taskIcon);
        _click(taskIcon);
        Assert.assertTrue(_waitForElementVisible(newTask), "Task container not open.");
    }

    public void clickOnNewTask() throws Exception {
        _click(newTask);
    }

    public void inputTaskDescription(String string) throws Exception {
        _waitForElementVisible(description);
        _sendKeys(description, string);
    }

    public void createTask() throws Exception {
        _clickOnTextContains("Create");
    }

    public boolean verifyCreatedTaskSuccessfully(String description) {
        return _isTextContainsPresent(description);
    }

    public boolean verifyCloseIcon() throws Exception {
        _click(closeIcon);
        return _waitForElementInvisible(closeIcon);
    }

    public boolean verifyCloseIcon(String description) throws Exception {
        _click(closeIcon);
        if (_waitForElementInvisible(closeIcon)) {
            return !_isTextContainsPresent(description);
        } else
            return false;
    }

    public boolean isCloseIconDisplayed() {
        return _isElementVisible(closeIcon);
    }

    public boolean verifyCancelButton(String description) throws Exception {
        _clickOnTextContains("Cancel");
        _waitForPageLoad();
        _pageWait(newTask);
        return _isTextContainsPresent(description);
    }

    public void addComment(String comment) throws Exception {
        _sendKeys(commentInput, comment);
        _click(sendComment);
    }

    public boolean verifyAddedComment(String comment) {
        return _isTextContainsPresent(comment);
    }

    public void viewTask(String description) throws Exception {
        _waitForPageLoad();
        _sleep(10);
        _scrollToElement(driver.findElement(By.xpath("//*[contains(text(),'" + description + "')]")));
        _sleep(20);
        Assert.assertTrue(_isTextContainsPresent(description), "Created task with description [" + description + "] not found.");
        _clickOnTextContains(description);
    }

    public boolean isTaskPresent(String description) {
        _waitForPageLoad();
        _sleep(10);
        try {
            _scrollToElement(driver.findElement(By.xpath("//*[contains(text(),'" + description + "')]")));
        } catch (NoSuchElementException e) {
            return false;
        }
        return _isTextContainsPresent(description);
    }

    public void clickOnEditTask() throws Exception {
        _clickOnTextContains("Edit");
    }

    public void updateDescription(String description) throws Exception {
        _waitForElementVisible(this.description);
        _sendKeys(this.description, description);
    }

    public void clickOnUpdate() throws Exception {
        _clickOnText("Update");
    }

    public boolean verifyUpdatedTaskDescription(String description) {
        return _isTextContainsPresent(description);
    }

    public boolean VerifyClickOnBack() throws Exception {
        _clickOnTextContains("Back");
        return _waitForElementVisible(newTask);
    }

    public void completeTask() throws Exception {
        _sleep(5);
        _clickOnText("Complete");
    }

    public void clickOnCompletedTasks() throws Exception {
        _waitForPageLoad();
        _waitForElementVisible(completed);
        _click(completed);
    }

    public void clickOnReOpen() throws Exception {
        _sleep(5);
        _clickOnText("Reopen");
    }

    public boolean verifyTaskReopened() {
        _sleep(5);
        return _isTextPresent("Complete");
    }

    public Map<String, String> inputOptionalFields(Map<String, String> taskValue) throws Exception {
        _click(calendarIcon);
        _waitForElementVisible(addOneMinute);
        _click(addOneMinute);
        _click(minusOneMinute);
        _click(addOneMinute);
        _click(addOneMinute);
        _click(addOneMinute);
        _clickOnText("Set");
        _sleep(5);
        if (_isTextPresent("Set")) {
            _clickOnText("Set");
        }
        _sleep(5);
        String due = _getText(dueDate);
        taskValue.put("DueDate", due);

        _waitForElementVisible(assigneeTo);
        _click(assigneeTo);
        _sendKeys(assigneeToInput, taskValue.get("Assignee"));
        _sendKeys(assigneeToInput, Keys.ENTER);
        return taskValue;
    }

    public String setDueDate() throws Exception {
        _click(calendarIcon);
        _waitForElementVisible(addOneMinute);
        _click(addOneMinute);
        _click(minusOneMinute);
        _click(addOneMinute);
        _click(addOneMinute);
        _click(addOneMinute);
        _clickOnText("Set");
        _sleep(5);
        if (_isTextPresent("Set")) {
            _clickOnText("Set");
        }
        _sleep(5);
        String due = _getText(dueDate);
        return due;
    }

    public void selectAssigneeTo(String name) throws Exception {
        _waitForElementVisible(assigneeTo);
        _click(assigneeTo);
        _sendKeys(assigneeToInput, name);
        _sendKeys(assigneeToInput, Keys.ENTER);
    }

    public void removeOptionalFields() throws Exception {
        _waitForElementVisible(clearDate);
        _click(clearDate);
        _click(clearAssignee);
    }

    private By time(String desc, String value) {
        return By.xpath("//*[text()='" + desc + "']/..//*[contains(text(),'" + value + "')]");
    }

    private By assignedBy(String desc, String value) {
        return By.xpath("//*[contains(@class,'desc')]//*[text()='" + desc + "']/../..//*[contains(text(),'Assigned by')]/..//*[contains(text(),'" + value + "')]");
    }

    private By assignedTo(String desc, String value) {
        return By.xpath("//*[contains(@class,'desc')]//*[text()='" + desc + "']/../..//*[contains(text(),'Assigned to')]/..//*[contains(text(),'" + value + "')]");
    }

    private By assignedOn(String desc, String value) {
        return By.xpath("//*[contains(@class,'desc')]//*[text()='" + desc + "']/../..//*[contains(text(),'Assigned on')]/..//*[contains(text(),'" + value + "')]");
    }

    private By dueDate(String desc, String value) {
        return By.xpath("//*[contains(@class,'desc')]//*[text()='" + desc + "']/../..//*[contains(text(),'Due date')]/..//*[contains(text(),'" + value + "')]");
    }

    private By updatedOn(String desc, String value) {
        return By.xpath("//*[contains(@class,'desc')]//*[text()='" + desc + "']/../..//*[contains(text(),'Updated on')]/..//*[contains(text(),'" + value + "')]");
    }

    private By updatedBy(String desc, String value) {
        return By.xpath("//*[contains(@class,'desc')]//*[text()='" + desc + "']/../..//*[contains(text(),'Last updated by')]/..//*[contains(text(),'" + value + "')]");
    }

    public By completedBy(String description, String completedBy) {
        return By.xpath("//*[contains(@class,'desc')]//*[text()='" + description + "']/../..//*[contains(text(),'Completed by')]/..//*[contains(text(),'" + completedBy + "')]");
    }

    public By completedOn(String description, String completedOn) {
        return By.xpath("//*[contains(@class,'desc')]//*[text()='" + description + "']/../..//*[contains(text(),'Completed on')]/..//*[contains(text(),'" + completedOn + "')]");
    }

    public boolean verifyOptionalFieldsValue(Map<String, String> taskValue) {
        String desc = taskValue.get("Description");
        String dueDate = taskValue.get("DueDate");
        String assignee = taskValue.get("Assignee");
        String time = dueDate.split(",")[1];

        Assert.assertTrue(_isElementVisible(time(desc, assignee)),
                "Assignee name not matched.");
        Assert.assertTrue(_isElementVisible(time(desc, "Today," + time)),
                "Created time not matched.");
        return true;
    }

    public boolean verifyAssignedBy(String taskDesc, String name) {
        return _isElementVisible(assignedBy(taskDesc, name));
    }

    public boolean verifyAssignedTo(String taskDesc, String name) {
        return _isElementVisible(assignedTo(taskDesc, name));
    }

    public boolean verifyDueDate(String taskDesc, String dueDate) {
        String time;
        if (dueDate.equalsIgnoreCase("--"))
            time = dueDate;
        else {
            String date = dueDate.split(",")[0];
            date = date.split(" ")[2];
            if (String.valueOf(LocalDateTime.now().getDayOfMonth()).equalsIgnoreCase(date))
                time = "Today, " + dueDate.split(",")[1].strip();
            else
                time = dueDate.replace("On", "").strip();
        }

        return _isElementVisible(dueDate(taskDesc, time));
    }

    public boolean verifyAssignedOn(String taskDesc, String assignedTime) {
        return _isElementVisible(assignedOn(taskDesc, "Today, " + assignedTime));
    }

    public boolean verifyUpdatedBy(String description, String updatedBy) {
        return _isElementVisible(updatedBy(description, updatedBy));
    }

    public boolean verifyUpdatedOn(String description, String updatedOn) {
        return _isElementVisible(updatedOn(description, updatedOn));
    }

    public void searchForPatientInTasks(String mrn) {
        _waitForPageLoad();
        _scrollToElement(driver.findElement(By.xpath("//*[contains(text(),'" + mrn + "')]")));
        _waitForPageLoad();
    }

    private By newTask(String mrn) {
        return By.xpath("//*[contains(text(),'" + mrn + "')]//ancestor::div[contains(@class,'patient-task') and contains(@class,'pb-o')]//*[contains(text(),'New task')]");
    }

    private By completeTask(String mrn) {
        return By.xpath("//*[contains(text(),'" + mrn + "')]//ancestor::div[contains(@class,'patient-task') and contains(@class,'pb-o')]//*[contains(text(),'Completed')]");
    }

    public void clickOnNewTaskOnTasks(String mrn) throws Exception {
        _smartClick(newTask(mrn));
    }

    public void clickOnCompletedTasksTab(String mrn) throws Exception {
        _waitForPageLoad();
        _smartClick(completeTask(mrn));
    }

    public boolean isTabDisplayed(String option) {
        return _isTextContainsPresent(option);
    }

    public boolean isTabSelected(String option) {
        return _isElementVisible(By.xpath("//*[contains(text(),'" + option + "') and contains(@class,'active')]"));
    }

    public void clickOnTab(String option) throws Exception {
        _clickOnTextContains(option);
    }

    public boolean isTaskPageDisplayed() {
        return _waitForElementVisible(By.xpath("//*[contains(text(),'Tasks') and contains(@class,'h-title')]"));
    }

    public boolean verifyCommentLabel() {
        return _isElementVisible(commentPlaceholder);
    }

    public boolean verifyCommentDetails(String comment, String commentedDetails) {
        return _isElementVisible(By.xpath("//*[contains(text(),'" + comment + "')]/parent::*[contains(@class,'comment')]/*[contains(text(),'" + commentedDetails + "')]"));
    }

    public boolean isEditButtonDisplayed() {
        return _isElementVisible(editText);
    }

    public boolean isEditIconDisplayed() {
        return _isElementVisible(editIcon);
    }

    public String updateDueDate() throws Exception {
        _click(calendarIcon);
        _waitForElementVisible(addOneMinute);
        _click(addOneHour);
        _click(addOneHour);
        _click(addOneMinute);
        _click(minusOneMinute);
        _click(addOneMinute);
        _click(addOneMinute);
        _click(addOneMinute);
        _clickOnText("Set");
        _sleep(5);
        if (_isTextPresent("Set")) {
            _clickOnText("Set");
        }
        _sleep(5);
        String due = _getText(dueDate);
        return due;
    }

    public void clickOnCancel() throws Exception {
        _clickOnTextContains("Cancel");
    }

    public void removeDueDate() throws Exception {
        _waitForElementVisible(clearDate);
        _click(clearDate);
    }

    public void removeAssignedTo() throws Exception {
        _waitForElementVisible(clearDate);
        _click(clearAssignee);
    }

    public boolean isReopenButtonDisplayed() {
        return _isTextContainsPresent("Reopen");
    }

    public boolean verifyCompletedBy(String description, String completedBy) {
        return _isElementVisible(completedBy(description, completedBy));
    }

    public boolean verifyCompletedOn(String description, String completedOn) {
        String time;
        if (completedOn.equalsIgnoreCase("--"))
            time = completedOn;
        else
            time = "Today, " + completedOn;

        return _isElementVisible(completedOn(description, time));
    }
}
