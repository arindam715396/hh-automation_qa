package com.biofourmis.biovitals.pagefactory.hcp;

import com.biofourmis.biovitals.utilities.CommonUtils;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

@Log4j
public class LoginPage extends CommonUtils {

    private WebDriver driver;

    public LoginPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(20)), this);
    }

    @iOSXCUITFindBy(id = "usernameTextField")
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/username")
    private WebElement email;

    @iOSXCUITFindBy(id = "passwordTextField")
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/password")
    private WebElement pass;

    @iOSXCUITFindBy(id = "loginButton")
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/btn_login")
    private WebElement loginBtn;

    @iOSXCUITFindBy(id = "eyeButton")
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/show_hide_pwd")
    private WebElement showPwd;

    @iOSXCUITFindBy(xpath = "//*[@name=\"Patients\"]")
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/bottom_nav_patients")
    private WebElement patientsTabOnHomeScreen;

    @iOSXCUITFindBy(id = "SVProgressHUD")
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/login_progress")
    private WebElement loginProgress;

    @iOSXCUITFindBy(id = "Settings")
    private WebElement setting;


    public boolean verifyLoginBtnIsDisable() throws Exception {
        _waitForElementVisible(loginBtn);
        return _isElementEnable(loginBtn);
    }

    public void clearPassword() throws Exception {
        _clear(pass);
    }

    public void enterEmail(String email) throws Exception {
        _sendKeys(this.email, email);
    }

    public void clearMail() throws Exception {
        _waitForElementVisible(email);
        _clear(email);
    }

    public void enterPassword(String password) throws Exception {
        _sendKeys(pass, password);
    }

    public boolean verifyLogin() {
        System.out.println("Progress bar " + _isElementVisible(loginProgress));
        _waitForElementInvisible(loginBtn);
        System.out.println("Progress bar " + _isElementVisible(loginProgress));
        _waitForElementInvisible(loginProgress);
        System.out.println("Progress bar " + _isElementVisible(loginProgress));
        return _waitForElementVisible(patientsTabOnHomeScreen);
    }

    public void clickOnLoginBtn() throws Exception {
        _click(loginBtn);
    }

    public boolean verifyErrorMessage(String s) {
        _waitForElementInvisible(loginProgress);
        return _isTextContainsPresent(s);
    }


    public void navigateToSettingsTab() throws Exception {
        _waitForElementInvisible(loginProgress);
        _waitForElementVisible(setting);
        _click(setting);
    }

    public boolean isSettingsTabDisplayed() {
        _waitForElementInvisible(loginProgress);
        return _isTextContainsPresent("Settings");
    }

    public boolean isOptionDisplayed(String text) {
        return _isTextContainsPresent(text);
    }

    public void clickOn(String no) throws Exception {
        _clickOnTextContains(no);
        _waitForElementInvisible(loginProgress);
    }

    public boolean isLoginScreenDisplayed() {
        _waitForElementInvisible(loginProgress);
        return _waitForElementVisible(loginBtn);
    }

    public boolean isLogOutOptionDisplayed() throws Exception {
        if (driver instanceof IOSDriver)
            return _getElementSize(By.xpath("//*[contains(@label,'Log out')]")) == 2;
        else
            return _getElementSize(By.xpath("//*[contains(@text,'Log out')]")) == 2;

    }
}
