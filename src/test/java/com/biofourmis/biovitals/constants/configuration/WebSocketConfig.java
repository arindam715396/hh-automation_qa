package com.biofourmis.biovitals.constants.configuration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WebSocketConfig {

    private String socketUrl;
    private Integer duration;
    private Integer vUsers;
    private Integer rampUp;
    private Integer interval;

}
