package com.biofourmis.biovitals.model.socket;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MobileDeviceStatusUpdate {
    String batteryLevel;
    String bluetoothStatus;
    String bundleId;
    String code;
    String device;
    String deviceId;
    String deviceModel;
    String deviceToken;
    Boolean isAppDebugMode;
    String isOnCharger;
    String name;
    String networkStrength;
    String osVersion;
    User user;
    String version;
    String voipToken;
}
