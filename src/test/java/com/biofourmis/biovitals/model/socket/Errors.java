package com.biofourmis.biovitals.model.socket;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class Errors {
    @JsonAlias({"websocket_error","Error: websocket error"})
    private String websocket_error;

    @JsonIgnore
    @JsonAlias({"eCon","ECONNRESET"})
    private String eCon;
}
