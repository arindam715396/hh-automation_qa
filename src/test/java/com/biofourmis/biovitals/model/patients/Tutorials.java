package com.biofourmis.biovitals.model.patients;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Tutorials {

    @JsonAlias({"patientOnboarding", "patient-onboarding"})
    Boolean patientOnboarding;
}
