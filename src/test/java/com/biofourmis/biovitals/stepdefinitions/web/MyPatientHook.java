package com.biofourmis.biovitals.stepdefinitions.web;

import com.biofourmis.biovitals.constants.configuration.TestConfig;
import com.biofourmis.biovitals.constants.scripts.ClinicInfo;
import com.biofourmis.biovitals.pagefactory.web.AddPatientPage;
import com.biofourmis.biovitals.pagefactory.web.HandOffPage;
import com.biofourmis.biovitals.pagefactory.web.HomePage;
import com.biofourmis.biovitals.pagefactory.web.MyPatientAllPatientPage;
import com.biofourmis.biovitals.utilities.CustomUtils;
import com.github.javafaker.Faker;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.log4j.Log4j;
import org.apache.commons.math3.analysis.function.Add;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;

@Log4j
public class MyPatientHook {

    @Autowired
    @Qualifier(value = "web")
    private WebDriver driver;
    @Autowired
    private Faker faker;
    @Autowired
    private TestConfig testConfig;
    @Autowired
    private ClinicInfo clinicInfo;


    private CustomUtils customUtils = new CustomUtils(null);
    private HomePage homePage;
    private MyPatientAllPatientPage myPatientAllPatientPage;
    private HandOffPage handOffPage;

    @And("Verify {string} add and remove patient form my patients")
    public void verifyAddAndRemovePatientFormMyPatients(String arg0) throws Exception {
        homePage = new HomePage(driver);
        myPatientAllPatientPage = new MyPatientAllPatientPage(driver);
        handOffPage = new HandOffPage(driver);

        log.info("Search for patient.[" + arg0 + "].");
        homePage.searchPatient(arg0);

        boolean isPresentInMy = !homePage.isPatientPresent(arg0);
        boolean whatNext = false;

        if (isPresentInMy) {
            Assert.assertTrue(verifyRemovePatientFromMy(arg0),
                    "Patient should be removed from My patients.");
            Assert.assertTrue(verifyPatientPresentInAll(arg0),
                    "Patient should be displayed in All patients.");
            whatNext = false;
        } else {
            goToAllPatients();

            log.info("Search for patient.[" + arg0 + "].");
            homePage.searchPatient(arg0);
            Assert.assertTrue(verifyAddPatientIntoMy(arg0),
                    "Patient should be displayed under My patients.");
            Assert.assertTrue(verifyPatientPresentInAll(arg0),
                    "Patient should be displayed in All patients.");
            whatNext = true;
        }

        if (whatNext) {
            goToMyPatients();
            log.info("Search for patient.[" + arg0 + "].");
            homePage.searchPatient(arg0);
            Assert.assertTrue(verifyRemovePatientFromMy(arg0),
                    "Patient should be removed from My patients.");
        } else {
            goToAllPatients();

            log.info("Search for patient.[" + arg0 + "].");
            homePage.searchPatient(arg0);
            Assert.assertTrue(verifyAddPatientIntoMy(arg0),
                    "Patient should be displayed under My patients.");
        }
        Assert.assertTrue(verifyPatientPresentInAll(arg0),
                "Patient should be displayed in All patients.");

    }

    private boolean verifyPatientPresentInAll(String arg0) throws Exception {
        goToAllPatients();
        log.info("Search for patient.[" + arg0 + "].");
        homePage.searchPatient(arg0);
        return !homePage.isPatientPresent(arg0);
    }

    private void goToAllPatients() throws Exception {
        log.info("Navigate to home.");
        homePage.goToHomePage();
        homePage.clickOnHamburger();
        log.info("Navigate to All patients.");
        homePage.navigateTo("All patients");
    }

    private void goToMyPatients() throws Exception {
        log.info("Navigate to home.");
        homePage.goToHomePage();
        homePage.clickOnHamburger();
        log.info("Navigate to My patient.");
        homePage.navigateTo("My patients");
    }


    private boolean verifyRemovePatientFromMy(String arg0) throws Exception {
        handOffPage.clickOnPatientOptions();
        handOffPage.clickOnOption("Remove from My patients");
        myPatientAllPatientPage.clickOnRemove();
        log.info("Search for patient.[" + arg0 + "].");
        homePage.searchPatient(arg0);
        return homePage.isPatientPresent(arg0);
    }

    private boolean verifyAddPatientIntoMy(String arg0) throws Exception {
        handOffPage.clickOnPatientOptions();
        handOffPage.clickOnOption("Add to My patients");
        goToMyPatients();
        log.info("Search for patient.[" + arg0 + "].");
        homePage.searchPatient(arg0);
        return !homePage.isPatientPresent(arg0);
    }

    @Then("Verify add and remove patient form my patients")
    public void verifyAddAndRemovePatientFormMyPatients() throws Exception {
        verifyAddAndRemovePatientFormMyPatients(CustomUtils.mnr);
    }

    @Then("Verify My patient list displayed")
    public void verifyMyPatientListDisplayed() {
        myPatientAllPatientPage = new MyPatientAllPatientPage(driver);
        Assert.assertTrue(myPatientAllPatientPage.verifyPatientListDisplayed(),
                "My patient list not displayed.");
    }

    @Then("Navigate to patient {string} under all patients")
    public void navigateToPatientUnderMyAllPatients(String arg0) throws Exception {
        userShouldNavigateSuccessfullyToAllPatientScreen();
        myPatientAllPatientPage = new MyPatientAllPatientPage(driver);
        myPatientAllPatientPage.searchInput(arg0);
        myPatientAllPatientPage.clickPatientMRN(arg0);
    }

    @Then("Navigate to patient under all patients")
    public void navigateToPatientAllPatients() throws Exception {
        userShouldNavigateSuccessfullyToAllPatientScreen();

        String arg0 = testConfig.getUnassignedPatientMRN();
        myPatientAllPatientPage = new MyPatientAllPatientPage(driver);
        myPatientAllPatientPage.searchInput(arg0);
        myPatientAllPatientPage.clickPatientMRN(arg0);
    }

    @Then("Verify search input field is clickable, accept input and clear by clicking on cross\\(X) icon")
    public void verifySearchInputFieldIsClickableAcceptInputAndClearByClickingOnCrossXIcon() throws Exception {
        myPatientAllPatientPage = new MyPatientAllPatientPage(driver);
        Assert.assertTrue(myPatientAllPatientPage.verifySearchFieldDisplayed(),
                "");
        Assert.assertTrue(myPatientAllPatientPage.verifySearchFieldClickable(),
                "");
        Assert.assertTrue(myPatientAllPatientPage.verifySearchFieldAcceptInput(),
                "");
        Assert.assertTrue(myPatientAllPatientPage.verifyClickOnClearIcon(),
                "");
        Assert.assertTrue(myPatientAllPatientPage.verifySearchFieldClear(),
                "");
    }

    @And("Check the input data for search component")
    public void checkTheInputDataForSearchComponent() throws Exception {
        myPatientAllPatientPage = new MyPatientAllPatientPage(driver);
        myPatientAllPatientPage.searchInput("Search patient");
        Assert.assertTrue(myPatientAllPatientPage.verifyNoExactResultFind(),
                "");
    }

    @Then("Verify QR code displayed by navigating through overflow menu")
    public void verifyQRCodeDisplayedByNavigatingThroughOverflowMenu() throws Exception {
        homePage = new HomePage(driver);
        myPatientAllPatientPage = new MyPatientAllPatientPage(driver);
        log.info("Search for patient.[" + CustomUtils.mnr + "].");
        homePage.searchPatient(CustomUtils.mnr);

        myPatientAllPatientPage.clickOnThreeDots();
        myPatientAllPatientPage.clickOnShowQR();
        Assert.assertTrue(myPatientAllPatientPage.QRCodeDisplayed(),
                "");
    }

    @And("Try to close the QR code popup page")
    public void tryToCloseTheQRCodePopupPage() throws Exception {
        Assert.assertTrue(myPatientAllPatientPage.closeQRContainer(),
                "");
    }

    @Then("User should navigate successfully to All patient screen")
    public void userShouldNavigateSuccessfullyToAllPatientScreen() throws Exception {
        homePage = new HomePage(driver);
        log.info("Click on Hamburger.");
        homePage.clickOnHamburger();

        log.info("Navigate to All patients");
        homePage.navigateTo("All patients");
    }

    @Then("Navigate to All patient screen")
    public void userToAllPatientScreen() throws Exception {
        homePage = new HomePage(driver);
        log.info("Click on Hamburger.");
        homePage.clickOnHamburger();

        log.info("Navigate to All patients");
        homePage.navigateTo("All patients");
    }

    @And("All patient should show list of My patient and Other patient")
    public void allPatientShouldShowListOfMyPatientAndOtherPatient() {
        myPatientAllPatientPage = new MyPatientAllPatientPage(driver);
        Assert.assertTrue(myPatientAllPatientPage.verifyPatientListDisplayed(),
                "");
    }


    @Then("Verify No caregiver message")
    public void VerifyNoCareGiverMessage()
    {
        myPatientAllPatientPage = new MyPatientAllPatientPage(driver);
        Assert.assertTrue(myPatientAllPatientPage.isMessageDisplayed("No clinician has been assigned"));
    }

    @Then("Add a patient with clinician only")
    public void AddAPatientWithClinicianOnly() throws Exception {

        homePage = new HomePage(driver);
        AddPatientPage addPatientPage = new AddPatientPage(driver);

        List<String> physician = List.of(testConfig.getClinicianName());
        List<String> nurse = List.of();

        Map<String, String> values = new HashMap<>();
        values.put("MRN", "MR" + String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
        values.put("Patient_Mob", "3" + String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
        values.put("Patient_Landline", "4" + String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
        values.put("Family_Pho_Num", "1" + String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
        values.put("Point_number", String.valueOf(faker.number().digits(10)));

        log.info("Try to create patient with details :" + customUtils.getDefaultPatientDetails(values));

        //Read Clinic Mandatory Fields
        log.info("Navigate to home page.");
        homePage.goToHomePage();

        log.info("Click on Add Patient.");
        Assert.assertTrue(homePage.clickOnAddPatient(), "Not able to click on Add Patient.");

        String[] patientOptions = clinicInfo.getAddPatientSteps().split(":");
        List<String> steps = new ArrayList<>();

        for (String option : patientOptions)
            if (option.contains("["))
                steps.add(option.substring(0, option.indexOf("[")).replaceAll("\\s", "").toLowerCase(Locale.ROOT));
            else
                steps.add(option.replaceAll("\\s", "").toLowerCase(Locale.ROOT));

        //For Personal Information page
        if (steps.contains("personalinformation")) {
            addPatientPage.fillPersonalInformation(customUtils.getDefaultPatientDetails(values), false);
            addPatientPage.clickOnNext();
        }

        //For Medical Information page
        if (steps.contains("medicalinformation")) {
            log.info("Click on next.");
            addPatientPage.clickOnNext();
        }

        //For Care team information page
        if (steps.contains("careteam")) {
            log.info("Verify Care team information page.");
            addPatientPage.verifyCareTeamInfoPageRequiredFields(physician, nurse);

            log.info("Click on next.");
            addPatientPage.clickOnNext();
        }

        //Equipment information page
        if (steps.contains("equipmentinformation")) {
            log.info("Fill Equipment information.");
            addPatientPage.fillEquipmentInformation(customUtils.getDefaultPatientDetails(), true);

            log.info("Click on next.");
            addPatientPage.clickOnNext();
        }

        log.info("Set QR Expire details.");
        customUtils.setQRExpireDetails(8);

        log.info("Save patient.");
        addPatientPage.savePatient();

        log.info("Update QR Expire details if required.");
        customUtils.updateQRExpireDetails(8);

        log.info("Set global MRN*");
        customUtils.setGlobalMNR(customUtils.getDefaultPatientDetails().get("MRN"));
        customUtils.setPatientDetails(customUtils.getDefaultPatientDetails().get("MRN"),
                customUtils.getDefaultPatientDetails().get("F_Name")+" "+customUtils.getDefaultPatientDetails().get("L_Name"));
        log.info("Set QR Hash details.");
        customUtils.setQrHashUrl(homePage.readQRUrl());

        log.info("Click on View patient option.");
        addPatientPage.clickOnViewPatientDetails();

        log.info("Verify new patient tutorial.");
        Assert.assertTrue(addPatientPage.verifyNewPatientTutorial(customUtils.getDefaultPatientDetails().get("MRN")),
                "Failed while verify new patient tutorial.");
    }

}
