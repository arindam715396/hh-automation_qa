package com.biofourmis.biovitals.model.patients;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MedicalHistory {
    @JsonIgnore
    PastHospitalizations pastHospitalizations;

    @JsonIgnore
    @JsonAlias({"cad", "CAD"})
    CAD cad;
    @JsonIgnore
    HistoryOfCKD historyOfCKD;
    String[] contactPrecautions;
    String[] admittingDiagonosis;
    @JsonIgnore
    String _id;
    String allergies;
    String clinicalSummary;
    String codeStatus;
    @JsonIgnore
    String patient;
    @JsonIgnore
    Long createdAt;
    @JsonIgnore
    Long updatedAt;
    @JsonIgnore
    Integer __v;
    @JsonIgnore
    String id;
}
