package com.biofourmis.biovitals.stepdefinitions.patient;

import com.biofourmis.biovitals.configs.LocalisationConfig;
import com.biofourmis.biovitals.pagefactory.patient.MyHealthPage;
import com.biofourmis.biovitals.pagefactory.web.MyPatientAllPatientPage;
import com.biofourmis.biovitals.utilities.CustomUtils;
import com.github.javafaker.Faker;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;

import java.time.LocalDateTime;
import java.util.List;

@Log4j
public class MyHealthHook {

    @Autowired(required = false)
    @Qualifier(value = "HH")
    private WebDriver patient;

    @Autowired(required = false)
    @Qualifier(value = "web")
    private WebDriver web;

    @Autowired(required = false)
    private LocalisationConfig localisation;

    @Autowired(required = false)
    private Faker faker;

    private CustomUtils customUtils = new CustomUtils(null);
    private MyHealthPage myHealthPage;
    private String expected;

    @Then("verify my health screen vitals")
    public void verifyMyHealthScreenVitals() throws Exception {
        myHealthPage = new MyHealthPage(patient);

        log.info("Navigate to my health screen by clicking on My health tab.");
        myHealthPage.navigateToMyHealthTab();

        log.info("Verify My health screen title displayed.");
        Assert.assertEquals(myHealthPage.titleText(), localisation.getString("text.my.health"),
                localisation.getString("text.my.health") + " title not displayed.");

        List<String> keys = List.of("text.heart.rate", "text.respiratory.rate", "text.body.temperature"
                , "text.steps", "text.blood.pressure", "text.weight", "text.spo2", "text.temporal.temperature",
                "text.blood.glucose");

        for (String vital : keys)
            Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp(localisation.getString(vital)),
                    "Vital [" + localisation.getString(vital) + "] button not displayed.");
    }

    @And("is Enter vitals button displayed")
    public void isEnterVitalsButtonDisplayed() throws Exception {
        myHealthPage = new MyHealthPage(patient);

        log.info("Navigate to my health screen by clicking on My health tab.");
        myHealthPage.navigateToMyHealthTab();

        log.info("Verify is Enter vitals button should be displayed.");
        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp(localisation.getString("enter.vitals")),
                "Enter vitals button not displayed. [" + localisation.getString("enter.vitals") + "].");
    }

    String lastSyncValue;
    String lastSyncTime;
    private LocalDateTime lastSyncDateTime;

    @And("verify entering invalid BP vitals and close pop-up icon")
    public void verifyEnteringInvalidBPVitalsAndClosePopUpIcon() throws Exception {
        myHealthPage = new MyHealthPage(patient);

        log.info("Navigate to my health screen by clicking on My health tab.");
        myHealthPage.navigateToMyHealthTab();

        log.info("Verify is Enter vitals button should be displayed.");
        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp(localisation.getString("enter.vitals")),
                "Enter vitals button not displayed. [" + localisation.getString("enter.vitals") + "].");

        lastSyncTime = myHealthPage.getLastSyncVitalTime(localisation.getString("text.blood.pressure"));
        lastSyncValue = myHealthPage.getLastSyncVitalValue(localisation.getString("text.blood.pressure"));

        log.info("Click on Enter vitals button.");
        myHealthPage.clickOnEnterVitalsBtn();

        log.info("Click on " + localisation.getString("text.blood.pressure"));
        myHealthPage.clickOnVital(localisation.getString("text.blood.pressure"));

        log.info("Verify is Close pop up icon displayed.");
        Assert.assertTrue(myHealthPage.isClosePopUpIconDisplayed(),
                "Vitals pop and it's close icon not displayed.");

        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp("Check blood pressure"),
                "Check blood pressure title not displayed.");
        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp("Please input your blood pressure in the box below"),
                "Please input your blood pressure in the box below header not displayed.");
        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp("Systolic"),
                "Systolic as a text not displayed.");
        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp("Diastolic"),
                "Diastolic as a text not displayed.");
        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp("mmHg"),
                "mmHg as a unit not displayed.");
        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp("Cancel"),
                "Cancel button not displayed.");
        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp("Submit"),
                "Submit button not displayed.");

        log.info("Click on submit button.");
        myHealthPage.clickOnSubmit();

        Assert.assertEquals(myHealthPage.verifyToastMessage().strip(), "Input value cannot be empty",
                "[Input value cannot be empty] toast message not displayed.");

        log.info("Click on cancel button.");
        myHealthPage.clickOnCancel();

        Assert.assertFalse(myHealthPage.isOptionDisplayedOnPopUp("Check blood pressure"),
                "Check blood pressure pop up should be closed after clicking on cancel button.");

        log.info("Click on Enter vitals button.");
        myHealthPage.clickOnEnterVitalsBtn();

        log.info("Click on " + localisation.getString("text.blood.pressure"));
        myHealthPage.clickOnVital(localisation.getString("text.blood.pressure"));

        log.info("Send 12 as a Systolic value.");
        myHealthPage.sendValueIn("Systolic", "12");

        log.info("Click on submit button.");
        myHealthPage.clickOnSubmit();

        Assert.assertEquals(myHealthPage.verifyToastMessage().strip(), "Systolic value should be in the range of 21 to 250",
                "[Systolic value should be in the range of 21 to 250] toast message not displayed.");

        log.info("Click on cancel button.");
        myHealthPage.clickOnCancel();

        Assert.assertFalse(myHealthPage.isOptionDisplayedOnPopUp("Check blood pressure"),
                "Check blood pressure pop up should be closed after clicking on cancel button.");

        String temp = myHealthPage.getLastSyncVitalTime(localisation.getString("text.blood.pressure"));

        Assert.assertEquals(lastSyncTime, temp,
                "Last sync time should not be updated after click on Cancel after filling invalid value.");

        temp = myHealthPage.getLastSyncVitalValue(localisation.getString("text.blood.pressure"));

        Assert.assertEquals(lastSyncValue, temp,
                "Last sync value should not be updated after click on Cancel after filling invalid value.");

        log.info("Click on Enter vitals button.");
        myHealthPage.clickOnEnterVitalsBtn();

        log.info("Click on " + localisation.getString("text.blood.pressure"));
        myHealthPage.clickOnVital(localisation.getString("text.blood.pressure"));

        log.info("Send 21 as a Systolic value.");
        myHealthPage.sendValueIn("Systolic", "21");

        log.info("Send 12 as a Diastolic value.");
        myHealthPage.sendValueIn("Diastolic", "12");

        log.info("Click on submit button.");
        myHealthPage.clickOnSubmit();

        Assert.assertEquals(myHealthPage.verifyToastMessage().strip(), "Diastolic value should be in the range of 21 to 200",
                "[Diastolic value should be in the range of 21 to 250] toast message not displayed.");

        log.info("Send 80 as a Systolic value.");
        myHealthPage.sendValueIn("Systolic", "80");

        log.info("Send 251 as a Diastolic value.");
        myHealthPage.sendValueIn("Diastolic", "251");

        log.info("Click on submit button.");
        myHealthPage.clickOnSubmit();

        Assert.assertEquals(myHealthPage.verifyToastMessage().strip(), "Diastolic value should be in the range of 21 to 200",
                "[Diastolic value should be in the range of 21 to 250] toast message not displayed.");

        log.info("Send 254 as a Systolic value.");
        myHealthPage.sendValueIn("Systolic", "254");

        log.info("Send 35 as a Diastolic value.");
        myHealthPage.sendValueIn("Diastolic", "35");

        log.info("Click on submit button.");
        myHealthPage.clickOnSubmit();

        Assert.assertEquals(myHealthPage.verifyToastMessage().strip(), "Systolic value should be in the range of 21 to 250",
                "[Systolic value should be in the range of 21 to 250] toast message not displayed.");

        log.info("Click on close pop icon.");
        myHealthPage.clickOnClosePopUp();

        Assert.assertFalse(myHealthPage.isOptionDisplayedOnPopUp("Check blood pressure"),
                "Check blood pressure pop up should be closed after clicking on cross (X) icon.");

        temp = myHealthPage.getLastSyncVitalTime(localisation.getString("text.blood.pressure"));

        Assert.assertEquals(lastSyncTime, temp,
                "Last sync time should not be updated after click on Cancel after filling invalid value.");

        temp = myHealthPage.getLastSyncVitalValue(localisation.getString("text.blood.pressure"));

        Assert.assertEquals(lastSyncValue, temp,
                "Last sync value should not be updated after click on Cancel after filling invalid value.");

    }

    String sys;
    String dia;

    @Then("verify entering valid BP vitals")
    public void verifyEnteringValidBPVitalsAndVerifyInDashboard() throws Exception {

        myHealthPage = new MyHealthPage(patient);

        sys = String.valueOf(faker.number().numberBetween(21, 99));
        dia = String.valueOf(faker.number().numberBetween(21, 120));

        log.info("Click on Enter vitals button.");
        myHealthPage.clickOnEnterVitalsBtn();

        log.info("Click on " + localisation.getString("text.blood.pressure"));
        myHealthPage.clickOnVital(localisation.getString("text.blood.pressure"));

        log.info("Send " + sys + " as a Systolic value.");
        myHealthPage.sendValueIn("Systolic", sys);

        log.info("Send " + dia + " as a Diastolic value.");
        myHealthPage.sendValueIn("Diastolic", dia);

        log.info("Click on submit button.");
        myHealthPage.clickOnSubmit();

        lastSyncDateTime = LocalDateTime.now();

        Assert.assertEquals(myHealthPage.verifyToastMessage().strip(), "Vitals data submitted successfully",
                "[Vitals data submitted successfully] toast message not displayed.");

        String actual = myHealthPage.getLastSyncVitalValue("Blood pressure");
        this.expected = getSubmittedValue();
        Assert.assertEquals(actual, expected,
                "Last sync BP values not matched. Expected [" + expected + "] - Actual [" + actual + "]");

        String lastSyncTime = myHealthPage.getLastSyncVitalTime(localisation.getString("text.blood.pressure"));

        Assert.assertEquals(lastSyncTime, "Today, Now",
                "Last sync BP values not matched. Expected [Today, Now] - Actual [" + lastSyncTime + "]");

        myHealthPage._sleep(60);

        lastSyncTime = myHealthPage.getLastSyncVitalTime(localisation.getString("text.blood.pressure"));

        String time = verifyLastSyncTime(lastSyncDateTime, false);

        Assert.assertEquals(lastSyncTime, "Today, " + time,
                "Last sync BP time not matched. Expected [Today, " + time + "] - Actual [" + lastSyncTime + "]");

    }

    private String verifyLastSyncTime(LocalDateTime lastSyncTime, boolean isWeb) {
        String time;
        if (lastSyncTime.getHour() >= 12) {
            String hour;
            if (String.valueOf(lastSyncTime.getHour() - 12).length() == 1)
                hour = "0" + String.valueOf((lastSyncTime.getHour() - 12));
            else
                hour = String.valueOf((lastSyncTime.getHour() - 12));

            if (lastSyncTime.getMinute() == 0)
                time = hour + ":00 PM";
            else if (String.valueOf(lastSyncTime.getMinute()).length() == 1)
                time = hour + ":0" + lastSyncTime.getMinute() + " PM";
            else
                time = hour + ":" + lastSyncTime.getMinute() + " PM";
        } else if (lastSyncTime.getHour() == 0) {
            if (lastSyncTime.getMinute() == 0)
                time = String.valueOf("12:00 PM");
            else if (String.valueOf(lastSyncTime.getMinute()).length() == 1)
                time = "12:0" + lastSyncTime.getMinute() + " AM";
            else
                time = "12:" + lastSyncTime.getMinute() + " AM";
        } else if (lastSyncTime.getMinute() == 0)
            time = String.valueOf(lastSyncTime.getHour()) + ":00 AM";
        else if (String.valueOf(lastSyncTime.getMinute()).length() == 1)
            time = String.valueOf(lastSyncTime.getHour()) + ":0" + lastSyncTime.getMinute() + " AM";
        else
            time = String.valueOf(lastSyncTime.getHour()) + ":" + lastSyncTime.getMinute() + " AM";

        // time = String.valueOf(lastSyncTime.getHour()) + ":" + lastSyncTime.getMinute() + " AM";

        if (isWeb)
            if (time.split(":")[0].startsWith("0"))
                time = time.substring(1, time.length());

        return time;
    }

    private String getSubmittedValue() {
        Integer sys = Integer.parseInt(this.sys.strip());
        Integer dys = Integer.parseInt(this.dia.strip());

        if (sys >= dys)
            return sys + "/" + dys;
        else
            return dys + "/" + sys;
    }

    @And("^verify entered (Blood pressure|Weight|Temporal temperature|SpO2|Blood glucose) value on details page$")
    public void verifyInDashboard(String arg0) throws Exception {
        MyPatientAllPatientPage myPatientAllPatientPage = new MyPatientAllPatientPage(web);

        String actual = myPatientAllPatientPage.getLastSyncValue(arg0);
        String expected;
        if (arg0.equalsIgnoreCase("Blood glucose"))
            expected = this.expected.split("\\.")[0];
        else
            expected = this.expected;
        Assert.assertEquals(actual, expected,
                "Last sync " + arg0 + " values not matched. Expected [" + expected + "] - Actual [" + actual + "]");

        actual = myPatientAllPatientPage.getLastSyncTime(arg0);
        expected = "Last synced: Today at " + this.verifyLastSyncTime(this.lastSyncDateTime, true);
        Assert.assertEquals(actual, expected,
                "Last sync " + arg0 + " time not matched. Expected [" + expected + "] - Actual [" + actual + "]");
    }

    @Then("verify entered BP value on dashboard")
    public void verifyEnteredBPValueOnDashboard() {
        MyPatientAllPatientPage myPatientAllPatientPage = new MyPatientAllPatientPage(web);
        String expected = this.expected;

        List<String> vitalsValues = myPatientAllPatientPage.getValues();

        Assert.assertTrue(vitalsValues.contains(expected),
                "Last updated BP value should be [" + expected + "].");
    }

    @Then("click on Enter vitals button and verify close icon")
    public void clickOnEnterVitalsButtonAndVerifyCloseIcon() throws Exception {

        log.info("Click on Enter vitals button.");
        myHealthPage.clickOnEnterVitalsBtn();

        Assert.assertTrue(myHealthPage.isSelectVitalsPopUpDisplayed(localisation.getString("select.vital")),
                "Select vitals pop up be displayed.");

        Assert.assertTrue(myHealthPage.isClosePopUpIconDisplayed(),
                "Close pop icon not displayed.");

        log.info("Click on close pop icon.");
        myHealthPage.clickOnClosePopUp();

        Assert.assertFalse(myHealthPage.isSelectVitalsPopUpDisplayed(localisation.getString("select.vital")),
                "After clicking on close icon Select vitals pop up should be closed.");
    }

    @And("verify entering invalid Weight and cancel option")
    public void verifyEnteringInvalidWeightAndCancelOption() throws Exception {
        myHealthPage = new MyHealthPage(patient);

        log.info("Navigate to my health screen by clicking on My health tab.");
        myHealthPage.navigateToMyHealthTab();

        log.info("Verify is Enter vitals button should be displayed.");
        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp(localisation.getString("enter.vitals")),
                "Enter vitals button not displayed. [" + localisation.getString("enter.vitals") + "].");

        lastSyncTime = myHealthPage.getLastSyncVitalTime(localisation.getString("text.weight"));
        lastSyncValue = myHealthPage.getLastSyncVitalValue(localisation.getString("text.weight"));

        log.info("Click on Enter vitals button.");
        myHealthPage.clickOnEnterVitalsBtn();

        log.info("Click on " + localisation.getString("text.weight"));
        myHealthPage.clickOnVital(localisation.getString("text.weight"));

        log.info("Verify is Close pop up icon displayed.");
        Assert.assertTrue(myHealthPage.isClosePopUpIconDisplayed(),
                "Vitals pop and it's close icon not displayed.");

        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp("Check weight"),
                "Check weight title not displayed.");
        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp("Please input your weight in the box below"),
                "Please input your weight in the box below header not displayed.");

        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp("lb"),
                "lb as a unit not displayed.");
        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp("Cancel"),
                "Cancel button not displayed.");
        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp("Submit"),
                "Submit button not displayed.");

        log.info("Click on submit button.");
        myHealthPage.clickOnSubmit();

        Assert.assertEquals(myHealthPage.verifyToastMessage().strip(), "Input value cannot be empty",
                "[Input value cannot be empty] toast message not displayed.");

        log.info("Click on cancel button.");
        myHealthPage.clickOnCancel();

        Assert.assertFalse(myHealthPage.isOptionDisplayedOnPopUp("Check weight"),
                "Check weight pop up should be closed after clicking on cancel button.");

        log.info("Click on Enter vitals button.");
        myHealthPage.clickOnEnterVitalsBtn();

        log.info("Click on " + localisation.getString("text.weight"));
        myHealthPage.clickOnVital(localisation.getString("text.weight"));

        log.info("Send 0 as a weight value.");
        myHealthPage.enterVital("0");

        log.info("Click on submit button.");
        myHealthPage.clickOnSubmit();

        Assert.assertEquals(myHealthPage.verifyToastMessage().strip(), "value should be in the range of 66 to 882",
                "[value should be in the range of 66 to 882] toast message not displayed.");

        log.info("Send 55 as a weight value.");
        myHealthPage.enterVital("55");

        log.info("Click on submit button.");
        myHealthPage.clickOnSubmit();

        Assert.assertEquals(myHealthPage.verifyToastMessage().strip(), "value should be in the range of 66 to 882",
                "[value should be in the range of 66 to 882] toast message not displayed.");

        log.info("Click on cancel button.");
        myHealthPage.clickOnCancel();

        Assert.assertFalse(myHealthPage.isOptionDisplayedOnPopUp("Check weight"),
                "Check weight pop up should be closed after clicking on cancel button.");

        String temp = myHealthPage.getLastSyncVitalTime(localisation.getString("text.weight"));

        Assert.assertEquals(lastSyncTime, temp,
                "Last sync time should not be updated after click on Cancel after filling invalid value.");

        temp = myHealthPage.getLastSyncVitalValue(localisation.getString("text.weight"));

        Assert.assertEquals(lastSyncValue, temp,
                "Last sync value should not be updated after click on Cancel after filling invalid value.");

        log.info("Click on Enter vitals button.");
        myHealthPage.clickOnEnterVitalsBtn();

        log.info("Click on " + localisation.getString("text.weight"));
        myHealthPage.clickOnVital(localisation.getString("text.weight"));

        log.info("Send 883 as a weight value.");
        myHealthPage.enterVital("883");

        log.info("Click on submit button.");
        myHealthPage.clickOnSubmit();

        Assert.assertEquals(myHealthPage.verifyToastMessage().strip(), "value should be in the range of 66 to 882",
                "[value should be in the range of 66 to 882] toast message not displayed.");

        log.info("Click on close pop icon.");
        myHealthPage.clickOnClosePopUp();

        Assert.assertFalse(myHealthPage.isOptionDisplayedOnPopUp("Check weight"),
                "Check weight pop up should be closed after clicking on cross (X) icon.");

        temp = myHealthPage.getLastSyncVitalTime(localisation.getString("text.weight"));

        Assert.assertEquals(lastSyncTime, temp,
                "Last sync time should not be updated after click on Cancel after filling invalid value.");

        temp = myHealthPage.getLastSyncVitalValue(localisation.getString("text.weight"));

        Assert.assertEquals(lastSyncValue, temp,
                "Last sync value should not be updated after click on Cancel after filling invalid value.");

    }

    String weight;

    @When("verify entering valid Weight")
    public void verifyEnteringValidWeight() throws Exception {
        weight = String.valueOf(faker.number().numberBetween(66, 882));

        log.info("Click on Enter vitals button.");
        myHealthPage.clickOnEnterVitalsBtn();

        log.info("Click on " + localisation.getString("text.weight"));
        myHealthPage.clickOnVital(localisation.getString("text.weight"));

        log.info("Send " + weight + " as a weight value.");
        myHealthPage.enterVital(weight);

        log.info("Click on submit button.");
        myHealthPage.clickOnSubmit();

        lastSyncDateTime = LocalDateTime.now();

        Assert.assertEquals(myHealthPage.verifyToastMessage().strip(), "Vitals data submitted successfully",
                "[Vitals data submitted successfully] toast message not displayed.");

        String actual = myHealthPage.getLastSyncVitalValue("Weight");
        this.expected = this.weight + ".0";
        Assert.assertEquals(actual, expected,
                "Last sync weight value not matched. Expected [" + expected + "] - Actual [" + actual + "]");

        String lastSyncTime = myHealthPage.getLastSyncVitalTime(localisation.getString("text.weight"));

        Assert.assertEquals(lastSyncTime, "Today, Now",
                "Last sync Weight values not matched. Expected [Today, Now] - Actual [" + lastSyncTime + "]");

        myHealthPage._sleep(60);

        lastSyncTime = myHealthPage.getLastSyncVitalTime(localisation.getString("text.weight"));

        String time = verifyLastSyncTime(lastSyncDateTime, false);

        Assert.assertEquals(lastSyncTime, "Today, " + time,
                "Last sync Weight time not matched. Expected [Today, " + time + "] - Actual [" + lastSyncTime + "]");

    }

    @And("verify entering invalid Temporal temperature and cancel option")
    public void verifyEnteringInvalidTemporalTemperatureAndCancelOption() throws Exception {
        myHealthPage = new MyHealthPage(patient);

        log.info("Navigate to my health screen by clicking on My health tab.");
        myHealthPage.navigateToMyHealthTab();

        log.info("Verify is Enter vitals button should be displayed.");
        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp(localisation.getString("enter.vitals")),
                "Enter vitals button not displayed. [" + localisation.getString("enter.vitals") + "].");

        lastSyncTime = myHealthPage.getLastSyncVitalTime(localisation.getString("text.temporal.temperature"));
        lastSyncValue = myHealthPage.getLastSyncVitalValue(localisation.getString("text.temporal.temperature"));

        log.info("Click on Enter vitals button.");
        myHealthPage.clickOnEnterVitalsBtn();

        log.info("Click on " + localisation.getString("text.temporal.temperature"));
        myHealthPage.clickOnVital(localisation.getString("text.temporal.temperature"));

        log.info("Verify is Close pop up icon displayed.");
        Assert.assertTrue(myHealthPage.isClosePopUpIconDisplayed(),
                "Vitals pop and it's close icon not displayed.");

        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp("Check temperature"),
                "Check temperature title not displayed.");
        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp("Please input your temperature in the box below"),
                "Please input your temperature in the box below header not displayed.");

        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp("°F"),
                "°F as a unit not displayed.");
        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp("Cancel"),
                "Cancel button not displayed.");
        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp("Submit"),
                "Submit button not displayed.");

        log.info("Click on submit button.");
        myHealthPage.clickOnSubmit();

        Assert.assertEquals(myHealthPage.verifyToastMessage().strip(), "Input value cannot be empty",
                "[Input value cannot be empty] toast message not displayed.");

        log.info("Click on cancel button.");
        myHealthPage.clickOnCancel();

        Assert.assertFalse(myHealthPage.isOptionDisplayedOnPopUp("Check temperature"),
                "Check temperature pop up should be closed after clicking on cancel button.");

        log.info("Click on Enter vitals button.");
        myHealthPage.clickOnEnterVitalsBtn();

        log.info("Click on " + localisation.getString("text.temporal.temperature"));
        myHealthPage.clickOnVital(localisation.getString("text.temporal.temperature"));

        log.info("Send 0 as a temperature value.");
        myHealthPage.enterVital("0");

        log.info("Click on submit button.");
        myHealthPage.clickOnSubmit();

        Assert.assertEquals(myHealthPage.verifyToastMessage().strip(), "value should be in the range of 68 to 113",
                "[value should be in the range of 68 to 113] toast message not displayed.");

        log.info("Send 58 as a temperature value.");
        myHealthPage.enterVital("58.6");

        log.info("Click on submit button.");
        myHealthPage.clickOnSubmit();

        Assert.assertEquals(myHealthPage.verifyToastMessage().strip(), "value should be in the range of 68 to 113",
                "[value should be in the range of 68 to 113] toast message not displayed.");

        log.info("Click on cancel button.");
        myHealthPage.clickOnCancel();

        Assert.assertFalse(myHealthPage.isOptionDisplayedOnPopUp("Check temperature"),
                "Check temperature pop up should be closed after clicking on cancel button.");

        String temp = myHealthPage.getLastSyncVitalTime(localisation.getString("text.temporal.temperature"));

        Assert.assertEquals(lastSyncTime, temp,
                "Last sync time should not be updated after click on Cancel after filling invalid value.");

        temp = myHealthPage.getLastSyncVitalValue(localisation.getString("text.temporal.temperature"));

        Assert.assertEquals(lastSyncValue, temp,
                "Last sync value should not be updated after click on Cancel after filling invalid value.");

        log.info("Click on Enter vitals button.");
        myHealthPage.clickOnEnterVitalsBtn();

        log.info("Click on " + localisation.getString("text.temporal.temperature"));
        myHealthPage.clickOnVital(localisation.getString("text.temporal.temperature"));

        log.info("Send 114 as a temperature value.");
        myHealthPage.enterVital("114");

        log.info("Click on submit button.");
        myHealthPage.clickOnSubmit();

        Assert.assertEquals(myHealthPage.verifyToastMessage().strip(), "value should be in the range of 68 to 113",
                "[value should be in the range of 68 to 113] toast message not displayed.");

        log.info("Click on close pop icon.");
        myHealthPage.clickOnClosePopUp();

        Assert.assertFalse(myHealthPage.isOptionDisplayedOnPopUp("Check temperature"),
                "Check temperature pop up should be closed after clicking on cross (X) icon.");

        temp = myHealthPage.getLastSyncVitalTime(localisation.getString("text.temporal.temperature"));

        Assert.assertEquals(lastSyncTime, temp,
                "Last sync time should not be updated after click on Cancel after filling invalid value.");

        temp = myHealthPage.getLastSyncVitalValue(localisation.getString("text.temporal.temperature"));

        Assert.assertEquals(lastSyncValue, temp,
                "Last sync value should not be updated after click on Cancel after filling invalid value.");

    }

    String temperature;

    @When("verify entering valid Temporal temperature")
    public void verifyEnteringValidTemporalTemperature() throws Exception {
        this.temperature = String.valueOf(faker.number().numberBetween(68, 113));

        log.info("Click on Enter vitals button.");
        myHealthPage.clickOnEnterVitalsBtn();

        log.info("Click on " + localisation.getString("text.temporal.temperature"));
        myHealthPage.clickOnVital(localisation.getString("text.temporal.temperature"));

        log.info("Send " + this.temperature + " as a temperature value.");
        myHealthPage.enterVital(this.temperature);

        log.info("Click on submit button.");
        myHealthPage.clickOnSubmit();

        lastSyncDateTime = LocalDateTime.now();

        Assert.assertEquals(myHealthPage.verifyToastMessage().strip(), "Vitals data submitted successfully",
                "[Vitals data submitted successfully] toast message not displayed.");

        String actual = myHealthPage.getLastSyncVitalValue("Temporal temperature");
        this.expected = this.temperature + ".0";
        Assert.assertEquals(actual, expected,
                "Last sync Temporal temperature value not matched. Expected [" + expected + "] - Actual [" + actual + "]");

        String lastSyncTime = myHealthPage.getLastSyncVitalTime(localisation.getString("text.temporal.temperature"));

        Assert.assertEquals(lastSyncTime, "Today, Now",
                "Last sync temperature values not matched. Expected [Today, Now] - Actual [" + lastSyncTime + "]");

        myHealthPage._sleep(60);

        lastSyncTime = myHealthPage.getLastSyncVitalTime(localisation.getString("text.temporal.temperature"));

        String time = verifyLastSyncTime(lastSyncDateTime, false);

        Assert.assertEquals(lastSyncTime, "Today, " + time,
                "Last sync temperature time not matched. Expected [Today, " + time + "] - Actual [" + lastSyncTime + "]");
    }

    @And("verify entering invalid SpO2 and cancel option")
    public void verifyEnteringInvalidSpOAndCancelOption() throws Exception {
        myHealthPage = new MyHealthPage(patient);

        log.info("Navigate to my health screen by clicking on My health tab.");
        myHealthPage.navigateToMyHealthTab();

        log.info("Verify is Enter vitals button should be displayed.");
        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp(localisation.getString("enter.vitals")),
                "Enter vitals button not displayed. [" + localisation.getString("enter.vitals") + "].");

        lastSyncTime = myHealthPage.getLastSyncVitalTime(localisation.getString("text.spo2"));
        lastSyncValue = myHealthPage.getLastSyncVitalValue(localisation.getString("text.spo2"));

        log.info("Click on Enter vitals button.");
        myHealthPage.clickOnEnterVitalsBtn();

        log.info("Click on " + localisation.getString("text.spo2"));
        myHealthPage.clickOnVital(localisation.getString("text.spo2"));

        log.info("Verify is Close pop up icon displayed.");
        Assert.assertTrue(myHealthPage.isClosePopUpIconDisplayed(),
                "Vitals pop and it's close icon not displayed.");

        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp("Check oxygen level"),
                "Check oxygen level title not displayed.");
        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp("Please input your oxygen level in the box below"),
                "Please input your oxygen level in the box below header not displayed.");

        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp("%"),
                "% as a unit not displayed.");
        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp("Cancel"),
                "Cancel button not displayed.");
        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp("Submit"),
                "Submit button not displayed.");

        log.info("Click on submit button.");
        myHealthPage.clickOnSubmit();

        Assert.assertEquals(myHealthPage.verifyToastMessage().strip(), "Input value cannot be empty",
                "[Input value cannot be empty] toast message not displayed.");

        log.info("Click on cancel button.");
        myHealthPage.clickOnCancel();

        Assert.assertFalse(myHealthPage.isOptionDisplayedOnPopUp("Check oxygen level"),
                "Check oxygen level pop up should be closed after clicking on cancel button.");

        log.info("Click on Enter vitals button.");
        myHealthPage.clickOnEnterVitalsBtn();

        log.info("Click on " + localisation.getString("text.spo2"));
        myHealthPage.clickOnVital(localisation.getString("text.spo2"));

        log.info("Send 0 as oxygen level value.");
        myHealthPage.enterVital("0");

        log.info("Click on submit button.");
        myHealthPage.clickOnSubmit();

        Assert.assertEquals(myHealthPage.verifyToastMessage().strip(), "value should be in the range of 86 to 100",
                "[value should be in the range of 86 to 100] toast message not displayed.");

        log.info("Send 58 as oxygen level value.");
        myHealthPage.enterVital("58");

        log.info("Click on submit button.");
        myHealthPage.clickOnSubmit();

        Assert.assertEquals(myHealthPage.verifyToastMessage().strip(), "value should be in the range of 86 to 100",
                "[value should be in the range of 86 to 100] toast message not displayed.");

        log.info("Click on cancel button.");
        myHealthPage.clickOnCancel();

        Assert.assertFalse(myHealthPage.isOptionDisplayedOnPopUp("Check oxygen level"),
                "Check oxygen level pop up should be closed after clicking on cancel button.");

        String temp = myHealthPage.getLastSyncVitalTime(localisation.getString("text.spo2"));

        Assert.assertEquals(lastSyncTime, temp,
                "Last sync time should not be updated after click on Cancel after filling invalid value.");

        temp = myHealthPage.getLastSyncVitalValue(localisation.getString("text.spo2"));

        Assert.assertEquals(lastSyncValue, temp,
                "Last sync value should not be updated after click on Cancel after filling invalid value.");

        log.info("Click on Enter vitals button.");
        myHealthPage.clickOnEnterVitalsBtn();

        log.info("Click on " + localisation.getString("text.spo2"));
        myHealthPage.clickOnVital(localisation.getString("text.spo2"));

        log.info("Send 80 as oxygen level value.");
        myHealthPage.enterVital("80");

        log.info("Click on submit button.");
        myHealthPage.clickOnSubmit();

        Assert.assertEquals(myHealthPage.verifyToastMessage().strip(), "value should be in the range of 86 to 100",
                "[value should be in the range of 86 to 100] toast message not displayed.");

        log.info("Send 110 as oxygen level value.");
        myHealthPage.enterVital("110");

        log.info("Click on submit button.");
        myHealthPage.clickOnSubmit();

        Assert.assertEquals(myHealthPage.verifyToastMessage().strip(), "value should be in the range of 86 to 100",
                "[value should be in the range of 86 to 100] toast message not displayed.");

        log.info("Click on close pop icon.");
        myHealthPage.clickOnClosePopUp();

        Assert.assertFalse(myHealthPage.isOptionDisplayedOnPopUp("Check oxygen level"),
                "Check oxygen level pop up should be closed after clicking on cross (X) icon.");

        temp = myHealthPage.getLastSyncVitalTime(localisation.getString("text.spo2"));

        Assert.assertEquals(lastSyncTime, temp,
                "Last sync time should not be updated after click on Cancel after filling invalid value.");

        temp = myHealthPage.getLastSyncVitalValue(localisation.getString("text.spo2"));

        Assert.assertEquals(lastSyncValue, temp,
                "Last sync value should not be updated after click on Cancel after filling invalid value.");

    }

    String spo2;

    @When("verify entering valid SpO2")
    public void verifyEnteringValidSpO() throws Exception {
        this.spo2 = String.valueOf(faker.number().numberBetween(88, 100));

        log.info("Click on Enter vitals button.");
        myHealthPage.clickOnEnterVitalsBtn();

        log.info("Click on " + localisation.getString("text.spo2"));
        myHealthPage.clickOnVital(localisation.getString("text.spo2"));

        log.info("Send " + this.spo2 + " as a oxygen level value.");
        myHealthPage.enterVital(this.spo2);

        log.info("Click on submit button.");
        myHealthPage.clickOnSubmit();

        lastSyncDateTime = LocalDateTime.now();

        Assert.assertEquals(myHealthPage.verifyToastMessage().strip(), "Vitals data submitted successfully",
                "[Vitals data submitted successfully] toast message not displayed.");

        String actual = myHealthPage.getLastSyncVitalValue("SpO2");
        this.expected = this.spo2;
        Assert.assertEquals(actual, expected,
                "Last sync SpO2 value not matched. Expected [" + expected + "] - Actual [" + actual + "]");

        String lastSyncTime = myHealthPage.getLastSyncVitalTime(localisation.getString("text.spo2"));

        Assert.assertEquals(lastSyncTime, "Today, Now",
                "Last sync spo2 values not matched. Expected [Today, Now] - Actual [" + lastSyncTime + "]");

        myHealthPage._sleep(60);

        lastSyncTime = myHealthPage.getLastSyncVitalTime(localisation.getString("text.spo2"));

        String time = verifyLastSyncTime(lastSyncDateTime, false);

        Assert.assertEquals(lastSyncTime, "Today, " + time,
                "Last sync spo2 time not matched. Expected [Today, " + time + "] - Actual [" + lastSyncTime + "]");

    }

    @And("verify entering invalid Blood Glucose and cancel option")
    public void verifyEnteringInvalidBloodGlucoseAndCancelOption() throws Exception {
        myHealthPage = new MyHealthPage(patient);

        log.info("Navigate to my health screen by clicking on My health tab.");
        myHealthPage.navigateToMyHealthTab();

        log.info("Verify is Enter vitals button should be displayed.");
        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp(localisation.getString("enter.vitals")),
                "Enter vitals button not displayed. [" + localisation.getString("enter.vitals") + "].");

        myHealthPage._swipeUp(1);

        lastSyncTime = myHealthPage.getLastSyncVitalTime(localisation.getString("text.blood.glucose"));
        lastSyncValue = myHealthPage.getLastSyncVitalValue(localisation.getString("text.blood.glucose"));

        log.info("Click on Enter vitals button.");
        myHealthPage.clickOnEnterVitalsBtn();

        log.info("Click on " + localisation.getString("text.blood.glucose"));
        myHealthPage.clickOnVital(localisation.getString("text.blood.glucose"));

        log.info("Verify is Close pop up icon displayed.");
        Assert.assertTrue(myHealthPage.isClosePopUpIconDisplayed(),
                "Vitals pop and it's close icon not displayed.");

        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp("Check blood glucose"),
                "Check blood glucose title not displayed.");
        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp("Please input your blood glucose in the box below"),
                "Please input your blood glucose in the box below header not displayed.");

        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp("mg/dL"),
                "mg/dL as a unit not displayed.");
        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp("Cancel"),
                "Cancel button not displayed.");
        Assert.assertTrue(myHealthPage.isOptionDisplayedOnPopUp("Submit"),
                "Submit button not displayed.");

        log.info("Click on submit button.");
        myHealthPage.clickOnSubmit();

        Assert.assertEquals(myHealthPage.verifyToastMessage().strip(), "Input value cannot be empty",
                "[Input value cannot be empty] toast message not displayed.");

        log.info("Click on cancel button.");
        myHealthPage.clickOnCancel();

        Assert.assertFalse(myHealthPage.isOptionDisplayedOnPopUp("Check blood glucose"),
                "Check blood glucose pop up should be closed after clicking on cancel button.");

        log.info("Click on Enter vitals button.");
        myHealthPage.clickOnEnterVitalsBtn();

        log.info("Click on " + localisation.getString("text.blood.glucose"));
        myHealthPage.clickOnVital(localisation.getString("text.blood.glucose"));

        log.info("Send 0 as blood glucose value.");
        myHealthPage.enterVital("0");

        log.info("Click on submit button.");
        myHealthPage.clickOnSubmit();

        Assert.assertEquals(myHealthPage.verifyToastMessage().strip(), "value should be in the range of 30 to 250",
                "[value should be in the range of 30 to 250] toast message not displayed.");

        log.info("Send 29 as blood glucose value.");
        myHealthPage.enterVital("29");

        log.info("Click on submit button.");
        myHealthPage.clickOnSubmit();

        Assert.assertEquals(myHealthPage.verifyToastMessage().strip(), "value should be in the range of 30 to 250",
                "[value should be in the range of 30 to 250] toast message not displayed.");

        log.info("Click on cancel button.");
        myHealthPage.clickOnCancel();

        Assert.assertFalse(myHealthPage.isOptionDisplayedOnPopUp("Check blood glucose"),
                "Check blood glucose pop up should be closed after clicking on cancel button.");

        String temp = myHealthPage.getLastSyncVitalTime(localisation.getString("text.blood.glucose"));

        Assert.assertEquals(lastSyncTime, temp,
                "Last sync time should not be updated after click on Cancel after filling invalid value.");

        temp = myHealthPage.getLastSyncVitalValue(localisation.getString("text.blood.glucose"));

        Assert.assertEquals(lastSyncValue, temp,
                "Last sync value should not be updated after click on Cancel after filling invalid value.");

        log.info("Click on Enter vitals button.");
        myHealthPage.clickOnEnterVitalsBtn();

        log.info("Click on " + localisation.getString("text.blood.glucose"));
        myHealthPage.clickOnVital(localisation.getString("text.blood.glucose"));

        log.info("Send 28.1 as blood glucose value.");
        myHealthPage.enterVital("28.1");

        log.info("Click on submit button.");
        myHealthPage.clickOnSubmit();

        Assert.assertEquals(myHealthPage.verifyToastMessage().strip(), "value should be in the range of 30 to 250",
                "[value should be in the range of 30 to 250] toast message not displayed.");

        log.info("Send 252 as blood glucose value.");
        myHealthPage.enterVital("252");

        log.info("Click on submit button.");
        myHealthPage.clickOnSubmit();

        Assert.assertEquals(myHealthPage.verifyToastMessage().strip(), "value should be in the range of 30 to 250",
                "[value should be in the range of 30 to 250] toast message not displayed.");

        log.info("Click on close pop icon.");
        myHealthPage.clickOnClosePopUp();

        Assert.assertFalse(myHealthPage.isOptionDisplayedOnPopUp("Check blood glucose"),
                "Check blood glucose pop up should be closed after clicking on cross (X) icon.");

        temp = myHealthPage.getLastSyncVitalTime(localisation.getString("text.blood.glucose"));

        Assert.assertEquals(lastSyncTime, temp,
                "Last sync time should not be updated after click on Cancel after filling invalid value.");

        temp = myHealthPage.getLastSyncVitalValue(localisation.getString("text.blood.glucose"));

        Assert.assertEquals(lastSyncValue, temp,
                "Last sync value should not be updated after click on Cancel after filling invalid value.");

    }

    String glucose;

    @When("verify entering valid Blood Glucose")
    public void verifyEnteringValidBloodGlucose() throws Exception {
        this.glucose = String.valueOf(faker.number().numberBetween(30, 250));

        log.info("Click on Enter vitals button.");
        myHealthPage.clickOnEnterVitalsBtn();

        log.info("Click on " + localisation.getString("text.blood.glucose"));
        myHealthPage.clickOnVital(localisation.getString("text.blood.glucose"));

        log.info("Send " + this.glucose + " as a blood glucose value.");
        myHealthPage.enterVital(this.glucose);

        log.info("Click on submit button.");
        myHealthPage.clickOnSubmit();

        lastSyncDateTime = LocalDateTime.now();

        Assert.assertEquals(myHealthPage.verifyToastMessage().strip(), "Vitals data submitted successfully",
                "[Vitals data submitted successfully] toast message not displayed.");

        String actual = myHealthPage.getLastSyncVitalValue("Blood glucose");
        this.expected = this.glucose + ".0";
        Assert.assertEquals(actual, expected,
                "Last sync SpO2 value not matched. Expected [" + expected + "] - Actual [" + actual + "]");

        String lastSyncTime = myHealthPage.getLastSyncVitalTime(localisation.getString("text.blood.glucose"));

        Assert.assertEquals(lastSyncTime, "Today, Now",
                "Last sync blood glucose values not matched. Expected [Today, Now] - Actual [" + lastSyncTime + "]");

        myHealthPage._sleep(60);

        lastSyncTime = myHealthPage.getLastSyncVitalTime(localisation.getString("text.blood.glucose"));

        String time = verifyLastSyncTime(lastSyncDateTime, false);

        Assert.assertEquals(lastSyncTime, "Today, " + time,
                "Last sync blood glucose time not matched. Expected [Today, " + time + "] - Actual [" + lastSyncTime + "]");

    }
}
