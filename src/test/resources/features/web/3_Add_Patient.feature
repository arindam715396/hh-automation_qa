Feature: HCP web [Add Patient] flow verification

  Background:
    Given Launch the browser
    And Login into dashboard

  Scenario: To verify Add Patient button and wizard
    Then Verify Add Patient button and wizard

  Scenario: To verify Cancel option without filling any field
    Then Verify flow should terminate with cancel info pop-up

  Scenario: To verify Add Patient with decline Cancel option and save patient
    And Open Add Patient wizard
    Then Fill patient details of section Personal information
    And Click on Cancel and decline the info pop-up
    Then Fill patient details of section Medical information
    And Click on Cancel and decline the info pop-up
    Then Fill patient details of section Care team
    And Click on Cancel and decline the info pop-up
    Then Fill patient details of section Equipment information
    And Click on Cancel and decline the info pop-up
    Then Fill patient details of section Review
    And Click on Cancel and decline the info pop-up
    Then Verify all field details on review page
    And Save patient
    Then Verify patient created successfully

  Scenario: To verify Add Patient with accept Cancel option on review page
    And Open Add Patient wizard
    Then Fill patient details of section Personal information
    And Click on Cancel and decline the info pop-up
    Then Fill patient details of section Medical information
    And Click on Cancel and decline the info pop-up
    Then Fill patient details of section Care team
    And Click on Cancel and decline the info pop-up
    Then Fill patient details of section Equipment information
    And Click on Cancel and decline the info pop-up
    Then Fill patient details of section Review
    And Click on Cancel and accept the info pop-up
    And Verify patient should not be created with given MNR Number

  Scenario: To verify Add Patient with Mandatory fields only
    And Add patient mandatory details
    Then Verify mandatory field details on review page
    And Save patient
    Then Verify patient created successfully