Feature: HCP web patient [Hand Off] functionality verification

  Background:
    Given Launch the browser
    And Login into dashboard

  Scenario: Prerequisite for automation
    And Add a new patient

  @TestCaseKey=HOHP-T676 @TestCaseKey=HOHP-T677 @TestCaseKey=HOHP-T678 @TestCasekey=HOHP-T679 @TestCasekey=HOHP-T680 @TestCaseKey=HOHP-T684 @TestCaseKey=HOHP-T685 @TestCasekey=HOHP-T686
  Scenario: Check all section is mandatory for handoff submission
    Then Search patient under my patients
    And Verify handoff container details and mandatory sections
    Then Verify adding HandOff with "Stable" Illness severity
    And Navigate to patient under my patients
    Then Verify HandOff details after successfully HandOff patient
    And Logout user
    Then Login nurse into dashboard
    And Navigate to patient under my patients
    Then Verify HandOff details after successfully HandOff patient
    And Search patient under my patients
    Then Remove this patient from my patients
    And Navigate to removed patient under all patients and Verify HandOff details should be read only mode
    And Logout user
    Then Login into dashboard
    And Navigate to patient under my patients
    Then Update HandOff details with "Unstable" Illness severity
    And Verify decline reset No change priory visit
    Then Verify HandOff details after successfully HandOff patient
    Then Update HandOff details with "Watcher" Illness severity
    And Verify accept reset No change priory visit
    Then Verify Nothing should change after decline reset

  @TestCasekey=HOHP-T681 @TestCaseKey=HOHP-T682 @TestCaseKey=HOHP-T683
  Scenario: To verify multiple and customize of awareness
    And Navigate to patient under my patients
    And Verify all Illness severity are selectable and highlighted after selected
    Then Verify multi select awareness options
    And Verify customize option of awareness

