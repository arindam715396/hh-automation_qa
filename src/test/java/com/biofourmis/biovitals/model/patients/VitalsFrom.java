package com.biofourmis.biovitals.model.patients;

import lombok.Builder;

@Builder
public class VitalsFrom {
    String hr;
    String rr;
    String bpsys;
    String bpdia;
    String weight;
    String bpw;
    String bi;
    String core_temp;
    String o_temp;
    String b_temp;
    String spo2;
    String hr_bp;
    String si;
    String steps;
}
