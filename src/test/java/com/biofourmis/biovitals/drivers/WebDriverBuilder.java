package com.biofourmis.biovitals.drivers;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;
import org.springframework.stereotype.Component;

/**
 * This class is read only do not modify anything here
 */
@Component
public class WebDriverBuilder {

    public WebDriver setupDriver(String platformName) {
        WebDriver driver;
        switch (platformName.toLowerCase()) {
            case "firefox": {
                FirefoxOptions firefoxOptions = new FirefoxOptions();
                firefoxOptions.setPageLoadStrategy(PageLoadStrategy.NORMAL);
                firefoxOptions.addArguments("--disable-notifications");
                firefoxOptions.addArguments("--disable-notifications");
                firefoxOptions.addArguments("use-fake-device-for-media-stream");
                firefoxOptions.addArguments("use-fake-ui-for-media-stream");
                WebDriverManager.firefoxdriver().setup();
                driver = new FirefoxDriver(firefoxOptions);
                break;
            }
            case "safari": {
                SafariOptions safariOptions = new SafariOptions();
//                safariOptions.setPageLoadStrategy(PageLoadStrategy.NORMAL.toString());
//                safariOptions.addArguments("--disable-notifications");
//                safariOptions.addArguments("--disable-notifications");
//                safariOptions.addArguments("use-fake-device-for-media-stream");
//                safariOptions.addArguments("use-fake-ui-for-media-stream");
                driver = new SafariDriver();
                break;
            }
            case "edge":
            {
                EdgeOptions edgeOptions = new EdgeOptions();
                edgeOptions.setPageLoadStrategy(PageLoadStrategy.NORMAL.toString());
//                edgeOptions.addArguments("--disable-notifications");
//                edgeOptions.addArguments("--disable-notifications");
//                edgeOptions.addArguments("use-fake-device-for-media-stream");
//                edgeOptions.addArguments("use-fake-ui-for-media-stream");
                driver = new EdgeDriver();
                break;
            }
            case "chrome":
            default: {
                WebDriverManager.chromedriver().setup();
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.setPageLoadStrategy(PageLoadStrategy.NORMAL);
                chromeOptions.addArguments("--disable-notifications");
                chromeOptions.addArguments("use-fake-device-for-media-stream");
                chromeOptions.addArguments("use-fake-ui-for-media-stream");
                //chromeOptions.setHeadless(true);
               driver = new ChromeDriver(chromeOptions);
            }
        }
        driver.manage().window().maximize();
        return driver;
    }

}
