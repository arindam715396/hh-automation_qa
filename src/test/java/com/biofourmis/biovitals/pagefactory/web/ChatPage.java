package com.biofourmis.biovitals.pagefactory.web;

import com.biofourmis.biovitals.utilities.CommonUtils;
import com.github.javafaker.Faker;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.*;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.awt.*;
import java.awt.event.InputEvent;
import java.util.ArrayList;
import java.util.List;

@Log4j
public class ChatPage extends CommonUtils {

    public ChatPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        faker = new Faker();
        PageFactory.initElements(driver, this);
    }

    private WebDriver driver;
    private Faker faker;

    @FindBy(xpath = "//*[@type=\"message\"]")
    private WebElement messageIcon;
    @FindBy(xpath = "//*[@type=\"ic-video-call\"]")
    private WebElement videoCallIcon;
    @FindBy(xpath = "//*[@type=\"ic-voice-call\"]")
    private WebElement voiceCallIcon;
    @FindBy(xpath = "//*[contains(@class,'call-end-icon')]")
    private WebElement callEnd;
    @FindBy(xpath = "//*[contains(@class,'chat-container')]//*[contains(text(),'Chat messages')]")
    private WebElement chatContainer;
    @FindBy(xpath = "//*[@placeholder=\"Write a message\"]")
    private WebElement chatInput;
    @FindBy(xpath = "//*[@type=\"send\"]")
    private WebElement sendButton;
    @FindBy(xpath = "//*[contains(@class,'chat-container')]//*[@type='ic-close']")
    private WebElement closeChat;
    @FindBy(xpath = "//*[contains(@class,'initialize-call')]")
    private WebElement callContainer;
    @FindBy(xpath = "//*[@class=\"end-call\"]")
    private WebElement endCall;
    @FindBy(xpath = "//*[contains(@class,'incoming-call-dialog')]")
    private WebElement recvCallDialog;
    @FindBy(xpath = "//*[contains(@class,'ic-reject-call')]")
    private WebElement rejectCall;
    @FindBy(xpath = "//*[contains(@class,'ic-accept-call')]")
    private WebElement acceptCall;
    @FindBy(xpath = "//*[@type=\"message\"]/..//*[contains(@class,'notification-badge')]")
    private WebElement chatBadgeWithPatientName;

    public void clickOnChatIconOnMyPatients() throws Exception {
        _click(messageIcon);
        _waitForPageLoad();
    }

    public boolean verifyChatWindowDisplayed() {
        return _isElementVisible(chatContainer);
    }

    public boolean verifyChatScreenComponents(String element) {
        return _isTextContainsPresent(element);
    }

    public boolean isCloseChatButtonVisible() {
        return _isElementVisible(closeChat);
    }

    public boolean isInputChatBoxVisible() {
        return _isElementPresent(chatInput);
    }

    public boolean isSendChatButtonVisible() {
        return _isElementVisible(sendButton);
    }

    public boolean closeChatContainer() throws Exception {
        _waitForLoading();
        _waitForPageLoad();
        _waitForElementVisible(closeChat);
        _click(closeChat);
        return _isElementVisible(chatContainer);
    }

    public void sendTextMessage(String first_message) throws Exception {
        _waitForElementVisible(chatInput);
        _sendKeys(chatInput, first_message);
        _click(sendButton);
    }

    public boolean verifySentTextMessage(String first_message) {
        //Message format should be Random text to Today's Date With time stamp
        return _isElementVisible(By.xpath("//*[@class='msg-body']//*[text()='" + first_message + "']"));
    }

    public ArrayList<String> verifySentTextMessage(ArrayList<String> messages) {
        _waitForLoading();
        _waitForElementInvisible(By.xpath("//*[contains(text(),'Loading')]"));
        _sleep(15);
        List<WebElement> chatMessages = driver.findElements(By.xpath("//*[contains(@class,'message')]"));
        ArrayList<String> actualMessages = new ArrayList<>();
        while (chatMessages.size() != messages.size()) {
            chatMessages.remove(0);
        }
        for (WebElement element : chatMessages)
            actualMessages.add(element.getText());
        return actualMessages;
    }

    public void clickOnVideoCallIconOnMyPatients() throws Exception {
        _waitForPageLoad();
        _waitForElementVisible(videoCallIcon);
        _smartClick(videoCallIcon);
    }

    public void clickOnVoiceCallIconOnMyPatients() throws Exception {
        _waitForPageLoad();
        _waitForElementVisible(voiceCallIcon);
        _click(voiceCallIcon);
    }

    public boolean isCallInitiated() {
        _waitForElementVisible(callContainer);
        Assert.assertTrue(_isElementVisible(callContainer),
                "Call container not displayed.");
        Assert.assertTrue(_isTextContainsPresent("End call"),
                "[End call] text not displayed.");
        Assert.assertTrue(_isTextContainsPresent("Calling..."),
                "[Calling...] text not displayed.");
        return true;
    }

    public boolean declineCall() throws Exception {
        _click(endCall);
        _waitForElementInvisible(callContainer);
        return _isElementVisible(callContainer);
    }

    public boolean verifyReceiveCall(String arg0) {
        _waitForElementVisible(recvCallDialog, 20);
        return _isElementVisible(recvCallDialog);
    }

    public boolean verifyReceiveDialogElements(String arg0) {
        return _isTextContainsPresent(arg0);
    }

    public boolean verifyDeclineCall() throws Exception {
        _clickOnTextContains("Reject");
        _waitForElementInvisible(rejectCall);
        return _isElementVisible(recvCallDialog);
    }

    public boolean verifyAcceptCall() throws Exception {
        _clickOnTextContains("Accept");
        return _isElementVisible(recvCallDialog);
    }

    public boolean endCall() throws Exception {
        _waitForElementVisible(callEnd);
        _click(callEnd);
        _waitForElementInvisible(callEnd);
        return _isElementVisible(callEnd);
    }

    public boolean verifyCallRejected() {
        _waitForElementInvisible(callContainer);
        return _isElementVisible(callContainer);
    }

    public boolean verifyCallAccepted() {
        _waitForElementInvisible(callEnd);
        return _isElementVisible(callEnd);
    }

    public boolean verifyBadgeCount(int count) throws Exception {
        _waitForElementVisible(chatBadgeWithPatientName);
        return _getText(chatBadgeWithPatientName).strip().equals(String.valueOf(count));
    }

    public void clickOnBadgeAndNavigateToChat() throws Exception {
        _click(chatBadgeWithPatientName);
    }

    @FindBy(xpath = "//*[@type=\"minimize\"]")
    private WebElement minBtn;
    @FindBy(xpath = "//*[@type=\"maximize\"]")
    private WebElement maxBtn;
    private By gridContent = By.xpath("//*[@class=\"grid-content\"]");

    private Dimension defaultSizeOfContainer;
    private Point defaultLocationOfContainer;

    public boolean verifyMinimizeContainer() throws Exception {
        Point maxLoc = driver.findElement(gridContent).getLocation();
        Dimension maxSize = driver.findElement(gridContent).getSize();
        defaultLocationOfContainer = maxLoc;
        defaultSizeOfContainer = maxSize;
        _click(minBtn);
        _sleep(2);
        Point minLoc = driver.findElement(gridContent).getLocation();
        Dimension minSize = driver.findElement(gridContent).getSize();

        if (maxSize.height < minSize.height && maxSize.width < minSize.width)
            return false;

        if (minLoc.x + minSize.width + 100 < driver.manage().window().getSize().width)
            return false;

        if (minLoc.y + minSize.height + 150 < driver.manage().window().getSize().height)
            return false;

        return true;
    }

    public boolean verifyMaximizeContainer() throws Exception {
        _click(maxBtn);
        _sleep(2);
        Point minLoc = driver.findElement(gridContent).getLocation();
        Dimension minSize = driver.findElement(gridContent).getSize();

        if (minLoc.equals(defaultLocationOfContainer) && minSize.equals(defaultSizeOfContainer))
            return true;
        else
            return false;
    }

    @FindBy(xpath = "//*[@type=\"ic-hamburger\"]")
    private WebElement hamburger;
    @FindBy(xpath = "//*[@type=\"ic-close\"]")
    private WebElement close;

    private By search = By.id("filterPatient");

    public boolean clickOnHamburger() throws Exception {
        try {
            _click(hamburger);
            return _isElementVisible(close);
        } catch (ElementClickInterceptedException e) {
            return true;
        }
    }

    public boolean dragCallContainerToAnotherLoc() {
        Point preLoc = driver.findElement(gridContent).getLocation();
        JavascriptExecutor js = (JavascriptExecutor) driver;
        WebElement element = driver.findElement(By.xpath("//*[contains(@class,'grid-container video-minimize')]"));
        Point p = driver.findElement(search).getLocation();
        js.executeScript("arguments[0].setAttribute('style', 'transform: translate3d(-" + p.x + "px, -" + p.y + "px, 0px);')", element);
        _sleep(2);
        Point postLoc = driver.findElement(gridContent).getLocation();

        return !preLoc.equals(postLoc);
    }

    public boolean verifyVideoIconIsDisable() throws Exception {
        try {
            _click(videoCallIcon);
            return false;
        } catch (ElementClickInterceptedException e) {
            return true;
        }
    }

    public boolean verifyVoiceIconIsDisable() throws Exception {
        try {
            _click(voiceCallIcon);
            return false;
        } catch (ElementClickInterceptedException e) {
            return true;
        }
    }

    public void clickOnMessageicon() throws Exception {
        _click(messageIcon);
        _waitForPageLoad();
    }

    public boolean verifyVideoCallOptionInChatDisable() throws Exception {
        try {
            _clickOnTextContains("Video call");
            return false;
        } catch (ElementClickInterceptedException e) {
            return true;
        }
    }

    public boolean verifyVoiceCallOptionInChatDisable() throws Exception {
        try {
            _clickOnTextContains("Voice call");
            return false;
        } catch (ElementClickInterceptedException e) {
            return true;
        }
    }

    public boolean verifyVideoIconIsInvisible() {
        return !_isElementVisible(videoCallIcon);
    }

    public boolean verifyVoiceIconIsInvisible() {
        return !_isElementVisible(videoCallIcon);
    }

    public boolean verifyChatIconIsDisable() throws Exception {
        try {
            _click(messageIcon);
            return false;
        } catch (ElementClickInterceptedException e) {
            return true;
        }
    }

    public boolean verifyChatIconIsInvisible() {
        return !_isElementVisible(messageIcon);
    }

    public void waitForMinutes(int arg0) {
        _sleep(arg0*60+5);
    }

    public boolean verifyOnGoingCall() {
        return _isElementVisible(callEnd);
    }
}

