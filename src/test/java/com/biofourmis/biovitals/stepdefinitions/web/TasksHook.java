package com.biofourmis.biovitals.stepdefinitions.web;

import com.biofourmis.biovitals.constants.configuration.TestConfig;
import com.biofourmis.biovitals.pagefactory.hcp.TasksPageApp;
import com.biofourmis.biovitals.pagefactory.web.TasksPage;
import com.biofourmis.biovitals.utilities.CustomUtils;
import com.github.javafaker.Faker;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

@Log4j
public class TasksHook {
    @Autowired(required = false)
    @Qualifier(value = "web")
    private WebDriver driver;

    @Autowired(required = false)
    @Qualifier(value = "HCP")
    private WebDriver clinician;

    @Autowired
    private Faker faker;

    @Autowired
    private TestConfig testConfig;

    private TasksPage tasks;
    private TasksPageApp taskPageApp;
    private String openTaskValue;
    private String patientMrn;
    private Map<String, String> details = new LinkedHashMap<>();
    private Map<String, String> updatedDetails = new LinkedHashMap<>();
    private CustomUtils customUtils = new CustomUtils(null);

    @Given("^Verify create new task with (Required|All) fields$")
    public void verify_create_new_task_with_fields(String string) throws Exception {
        tasks = new TasksPage(driver);
        log.info("Click on Task icon.");
        tasks.clickOnTaskIcon();
        boolean isAll = string.equals("All");
        log.info("Click on New Task.");
        tasks.clickOnNewTask();

        Map<String, String> taskValue = getTaskValue(isAll);
        log.info("Input task description.");
        tasks.inputTaskDescription(taskValue.get("Description"));

        if (isAll) {
            log.info("Input optional fields. [" + taskValue + "]");
            details = tasks.inputOptionalFields(taskValue);
        }
        log.info("Click on Create task.");
        tasks.createTask();

        log.info("Search and click on created task. [" + details.get("Description") + "]");
        tasks.viewTask(details.get("Description"));

        log.info("Verify task created successfully.");
        tasks.verifyCreatedTaskSuccessfully(taskValue.get("Description"));
        if (isAll) {
            log.info("Verify optional fields.");
            tasks.verifyOptionalFieldsValue(taskValue);
        }
    }

    private void verifyCreatedTask(boolean isAll) throws Exception {
        tasks = new TasksPage(driver);
        log.info("Search and click on created task. [" + details.get("Description") + "]");
        tasks.viewTask(details.get("Description"));

        log.info("Verify task created successfully.");
        tasks.verifyCreatedTaskSuccessfully(details.get("Description"));
        if (isAll) {
            log.info("Verify optional fields.");
            tasks.verifyOptionalFieldsValue(details);
        }
    }

    private Map<String, String> getTaskValue(boolean isAll) {
        if (!details.isEmpty())
            return details;

        details.put("Description", faker.aviation().aircraft() + LocalDateTime.now());
        details.put("AssignedBy", testConfig.getClinicianName());
        details.put("AssignedOn", "");
        details.put("AssignedTo", "--");
        details.put("DueDate", "--");
        if (isAll)
            details.put("type", "all");
        else
            details.put("type", "required");
        return details;
    }

    private Map<String, String> updateTaskValue() {
        if (details.isEmpty())
            throw new RuntimeException("No Task Value found.");

        updatedDetails.put("Description", faker.aviation().aircraft() + LocalDateTime.now());
        updatedDetails.put("AssignedBy", details.get("AssignedBy"));
        updatedDetails.put("AssignedOn", details.get("AssignedOn"));

        updatedDetails.put("AssignedTo", "--");
        updatedDetails.put("DueDate", "--");

        updatedDetails.put("LastUpdatedBy", "");
        updatedDetails.put("LastUpdatedOn", "");

        return updatedDetails;
    }

    @Then("Verify comment on created task")
    public void verifyCommentOnCreatedTask() throws Exception {
        String desc = details.get("Description");
        log.info("View task [" + desc + "].");
        tasks.viewTask(desc);
        String comment = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();

        log.info("Add comment [" + comment + "].");
        tasks.addComment(comment);

        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Comment not matched. Expected comment [" + comment + "].");

        Assert.assertTrue(tasks.verifyCloseIcon(),
                "Task not closed after clicking on close icon.");

        log.info("Click on task icon.");
        tasks.clickOnTaskIcon();
        log.info("View task [" + desc + "].");
        tasks.viewTask(desc);
        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Comment not matched. Expected comment [" + comment + "].");
    }

    @And("^Verify edit a task with (Required|All) fields$")
    public void verifyEditATaskWithFields(String arg0) throws Exception {
        boolean isAll = arg0.equals("All");
        log.info("Click on edit task.");
        tasks.clickOnEditTask();

        details.put("Description", faker.gameOfThrones().dragon() + LocalDateTime.now().toString());
        String desc = details.get("Description");
        log.info("Update task description as [" + desc + "].");
        tasks.updateDescription(desc);

        if (isAll) {
            log.info("Update all input fields as [" + details + "].");
            details = tasks.inputOptionalFields(details);
        }

        log.info("Click on update.");
        tasks.clickOnUpdate();

        Assert.assertTrue(tasks.verifyUpdatedTaskDescription(details.get("Description")),
                "Task with updates description npt found. Description [" + desc + "].");

        log.info("Click on Back option.");
        tasks.VerifyClickOnBack();

        log.info("View task [" + desc + "].");
        tasks.viewTask(details.get("Description"));

        if (isAll)
            Assert.assertTrue(tasks.verifyOptionalFieldsValue(details),
                    "Task optional fields not matched after updating. Expected [" + details + "].");
    }

    @Then("Verify complete a task")
    public void verifyCompleteATask() throws Exception {
        tasks = new TasksPage(driver);
        log.info("Complete a task.");
        tasks.completeTask();

        log.info("Add Completed details.");
        details.put("CompletedOn", getAssignedOn());
        details.put("CompletedBy", testConfig.getClinicianName());

        log.info("Open Completed task option.");
        tasks.clickOnCompletedTasks();

        log.info("verify task should be present in completed section.");
        tasks.viewTask(details.get("Description"));
    }

    @And("Verify comment on completed task")
    public void verifyCommentOnCompletedTask() throws Exception {
        String comment = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();

        log.info("Add comment in task.");
        tasks.addComment(comment);
        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Verify comment added successfully. Expected [" + comment + "].");

        log.info("Click on close icon.");
        tasks.verifyCloseIcon();
        log.info("Click on task icon.");
        tasks.clickOnTaskIcon();
        log.info("Open Completed task option.");
        tasks.clickOnCompletedTasks();
        log.info("Search task. Description [" + details.get("Description") + "].");
        tasks.viewTask(details.get("Description"));
        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Comment not matched. Expected comment on task is [" + comment + "].");
    }

    @Then("Verify re-open completed task")
    public void verifyReOpenCompletedTask() throws Exception {
        tasks = new TasksPage(driver);

        String desc = details.get("Description");
        log.info("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);

        log.info("Click on reopen task.");
        tasks.clickOnReOpen();

        Assert.assertTrue(tasks.verifyTaskReopened(),
                "Not able to reopen completed task.");

        log.info("Click on close icon.");
        tasks.verifyCloseIcon();

        log.info("Click on task icon.");
        tasks.clickOnTaskIcon();

        log.info("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);
    }

    @And("Verify comment on re-opened task")
    public void verifyCommentOnReOpenedTask() throws Exception {
        String comment = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();

        String desc = details.get("Description");
        log.info("Add comment in task. [" + comment + "].");
        tasks.addComment(comment);

        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Expected comment is [" + comment + "].");

        log.info("Click on close icon.");
        tasks.verifyCloseIcon();

        log.info("Click on task icon.");
        tasks.clickOnTaskIcon();

        log.info("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);

        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Expected comment is [" + comment + "].");
    }

    @Then("^Verify edit re-open task with (Required|All) fields$")
    public void verifyEditReOpenTaskWithFields(String arg0) throws Exception {
        boolean isAll = arg0.equals("All");
        log.info("Click on reopen task.");
        tasks.clickOnEditTask();
        details.put("Description", faker.gameOfThrones().dragon() + LocalDateTime.now().toString());
        String desc = details.get("Description");
        log.info("Update task description as [" + desc + "].");
        tasks.updateDescription(desc);

        if (isAll) {
            log.info("Update optional fields.");
            details = tasks.inputOptionalFields(details);
        }

        log.info("Click on info.");
        tasks.clickOnUpdate();
        Assert.assertTrue(tasks.verifyUpdatedTaskDescription(details.get("Description")),
                "Task with description [" + desc + "] not found.");
        log.info("Click on back.");
        tasks.VerifyClickOnBack();
        log.info("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);
        if (isAll)
            Assert.assertTrue(tasks.verifyOptionalFieldsValue(details),
                    "Option fields updated value not matched. [" + details + "].");
    }

    @Then("Verify complete a re-open task")
    public void verifyCompleteAReOpenTask() throws Exception {
        String desc = details.get("Description");
        log.info("Click on complete task.");
        tasks.completeTask();

        log.info("Open complete task(s) option.");
        tasks.clickOnCompletedTasks();
        log.info("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);
    }

    @And("Verify closing task container")
    public void verifyClosingTaskContainer() throws Exception {
        tasks = new TasksPage(driver);

        log.info("Click on Task icon.");
        tasks.clickOnTaskIcon();
        Assert.assertTrue(tasks.verifyCloseIcon(),
                "Not able to close Task container after click on close icon..");

        log.info("Click on Task icon.");
        tasks.clickOnTaskIcon();
        String description = faker.aquaTeenHungerForce() + LocalDateTime.now().toString();

        log.info("Click on new Task");
        tasks.clickOnNewTask();

        log.info("Enter task description.");
        tasks.inputTaskDescription(description);

        Assert.assertTrue(tasks.verifyCloseIcon(description),
                "Task should not be created without after click ob cancel icon.");
    }

    @Then("Verify Cancel button functionality")
    public void verifyCancelButtonFunctionality() throws Exception {
        log.info("Click on Task icon.");
        tasks.clickOnTaskIcon();
        String description = faker.aquaTeenHungerForce() + LocalDateTime.now().toString();
        log.info("Click on New Task option.");
        tasks.clickOnNewTask();
        log.info("Enter task description. [" + description + "]");
        tasks.inputTaskDescription(description);
        log.info("Verify after canceling the task. Task should not be created.");
        Assert.assertFalse(tasks.verifyCancelButton(description),
                "Task created with description [" + description + "] by just input description and click on cancel.");
    }

    @And("Verify edit a task and remove optional fields")
    public void verifyEditATaskAndRemoveOptionalFields() throws Exception {
        log.info("Click on Edit.");
        tasks.clickOnEditTask();
        log.info("Remove optional fields values.");
        tasks.removeOptionalFields();
        log.info("Click on update.");
        tasks.clickOnUpdate();
        //TODO Verify removed tasks
    }

    @And("^Verify create new task with (Required|All) fields on tasks tab$")
    public void verifyCreateNewTaskWithRequiredFieldsOnTaskTab(String arg0) throws Exception {
        tasks = new TasksPage(driver);
        boolean isAll = arg0.equals("All");

        log.info("Scroll to patient.");
        tasks.searchForPatientInTasks(customUtils.getDefaultPatientDetails().get("MRN"));

        log.info("Click on new task.");
        tasks.clickOnNewTaskOnTasks(customUtils.getDefaultPatientDetails().get("MRN"));

        Map<String, String> taskValue = getTaskValue(isAll);
        String desc = taskValue.get("Description");
        log.info("Enter task description.");
        tasks.inputTaskDescription(desc);

        if (isAll) {
            log.info("Input optional fields. Values [" + taskValue + "]");
            details = tasks.inputOptionalFields(taskValue);
        }

        log.info("Click on Create.");
        tasks.createTask();
        desc = details.get("Description");
        log.info("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);

        Assert.assertTrue(tasks.verifyCreatedTaskSuccessfully(desc),
                "Task not created with details [" + details + "].");
        if (isAll)
            Assert.assertTrue(tasks.verifyOptionalFieldsValue(taskValue),
                    "Task details not matched. Values [" + details + "]");
    }

    @Then("Verify comment on created task on tasks tab")
    public void verifyCommentOnCreatedTasksTab() throws Exception {
        String comment = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();
        log.info("Add comment [" + comment + "].");
        tasks.addComment(comment);
        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Comment not added as [" + comment + "]");

        log.info("Refresh page.");
        tasks._refreshPage();

        String desc = details.get("Description");
        log.info("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);

        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Comment not added as [" + comment + "]");
    }

    @And("Verify comment on completed task on tasks tab")
    public void verifyCommentOnCompletedTasksTab() throws Exception {
        String comment = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();
        log.info("Add comment [" + comment + "].");
        tasks.addComment(comment);
        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Comment not added as [" + comment + "]");
        log.info("Refresh page.");
        tasks._refreshPage();
        log.info("Click on complete task.");
        tasks.clickOnCompletedTasksTab(customUtils.getDefaultPatientDetails().get("MRN"));

        String desc = details.get("Description");
        log.info("Search task. Description [" + desc + "].");
        tasks.searchForPatientInTasks(customUtils.getDefaultPatientDetails().get("MRN"));
        tasks.viewTask(desc);

        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Comment not added as [" + comment + "]");
    }

    @Then("Verify re-open completed task on tasks tab")
    public void verifyReOpenCompletedTasksTab() throws Exception {
        String desc = details.get("Description");

        log.info("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);

        log.info("Click on reopen.");
        tasks.clickOnReOpen();

        Assert.assertTrue(tasks.verifyTaskReopened(),
                "Task with description [" + desc + "] not re-open.");

        log.info("Refresh page.");
        tasks._refreshPage();

        log.info("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);
    }

    @And("Verify comment on re-opened task on tasks tab")
    public void verifyCommentOnReOpenedTasksTab() throws Exception {
        String comment = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();
        log.info("Add comment [" + comment + "].");
        tasks.addComment(comment);
        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Comment not added as [" + comment + "]");

        log.info("Refresh page.");
        tasks._refreshPage();

        String desc = details.get("Description");
        log.info("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);
        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Comment not added as [" + comment + "]");
    }

    @And("^Verify edit a task with (Required|All) fields on tasks tab$")
    public void verifyEditATaskWithRequiredFieldsOnTasksTab(String arg0) throws Exception {
        boolean isAll = arg0.equals("All");
        log.info("Click on edit.");
        tasks.clickOnEditTask();

        details.put("Description", faker.gameOfThrones().dragon() + LocalDateTime.now().toString());
        String desc = details.get("Description");
        log.info("Update task description as [" + desc + "].");
        tasks.updateDescription(desc);

        if (isAll) {
            log.info("Update optional fields..");
            details = tasks.inputOptionalFields(details);
            log.info("Optional fields entered as [" + details + "].");
        }

        log.info("Click on update.");
        tasks.clickOnUpdate();

        Assert.assertTrue(tasks.verifyUpdatedTaskDescription(details.get("Description")),
                "Updated task description not found as [" + desc + "].");

        log.info("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);
        if (isAll)
            Assert.assertTrue(tasks.verifyOptionalFieldsValue(details),
                    "Optional fields value not matched. Values [" + details + "].");
    }

    @Then("^Verify edit re-open task with (Required|All) fields on tasks tab$")
    public void verifyEditReOpenTaskWithFieldsTab(String arg0) throws Exception {
        boolean isAll = arg0.equals("All");
        log.info("Click on edit.");
        tasks.clickOnEditTask();
        details.put("Description", faker.gameOfThrones().dragon() + LocalDateTime.now().toString());

        String desc = details.get("Description");
        log.info("Update task description as [" + desc + "].");
        tasks.updateDescription(desc);

        if (isAll) {
            log.info("Update optional fields..");
            details = tasks.inputOptionalFields(details);
            log.info("Optional fields entered as [" + details + "].");
        }

        log.info("Click on update.");
        tasks.clickOnUpdate();

        Assert.assertTrue(tasks.verifyUpdatedTaskDescription(details.get("Description")),
                "Updated task description not found as [" + desc + "].");

        log.info("Click on close icon.");
        tasks.verifyCloseIcon();

        log.info("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);

        if (isAll)
            Assert.assertTrue(tasks.verifyOptionalFieldsValue(details),
                    "Optional fields value not matched. Values [" + details + "].");
    }

    @Then("Verify complete a task on task tab")
    public void verifyCompleteATaskOnTaskTab() throws Exception {
        log.info("Complete a task.");
        tasks.completeTask();

        log.info("Open Completed task option.");
        tasks.clickOnCompletedTasksTab(customUtils.getDefaultPatientDetails().get("MRN"));

        log.info("verify task should be present in completed section.");
        tasks.viewTask(details.get("Description"));
    }

    @Then("Verify complete a re-open task on task tab")
    public void verifyCompleteAReOpenTaskOnTaskTab() throws Exception {
        String desc = details.get("Description");
        log.info("Click on complete task.");
        tasks.completeTask();

        log.info("Open complete task(s) option.");
        tasks.clickOnCompletedTasksTab(customUtils.getDefaultPatientDetails().get("MRN"));

        log.info("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);
    }

    @When("^Add new task with (required|all) fields$")
    public void addNewTaskWithRequiredFields(String arg0) throws Exception {
        taskPageApp = new TasksPageApp(clinician);

        Map<String, String> taskDetails = getTaskValue(false);

        String name = testConfig.getPrimaryPatientName();
        String[] sNameArr = name.split(" ");
        String sName = "";
        for (String val : sNameArr)
            sName += val.charAt(0);
        sName = sName.toUpperCase(Locale.ROOT);

        log.info("Click on add new task.");
        taskPageApp.clickOnAddNewTaskIcon(sName);

        log.info("Enter Task description.");
        taskPageApp.addTaskDescription(taskDetails.get("Description"));


        if (arg0.equalsIgnoreCase("all")) {
            log.info("Select Due date and time");

            log.info("Select assignee as " + testConfig.getClinicianName());
        }

        taskPageApp.clickOnCreate();
    }

    @Then("verify created task in HCP web")
    public void verifyCreatedTaskInHCPWeb() throws Exception {
        verifyCreatedTask(false);
    }

    //-------------------------------Auto---------------------------\\
    @Then("^verify (Active|Archived) task tab$")
    public void verifyActiveTaskTab(String arg0) throws Exception {
        TasksPage webPage = new TasksPage(driver);
        if (arg0.equalsIgnoreCase("Active"))
            verifyActiveTab(webPage);
        else
            verifyArchivedTab(webPage);
    }

    private void verifyArchivedTab(TasksPage tasksPage) throws Exception {
        log.info("Verify Archived tab displayed.");
        Assert.assertTrue(tasksPage.isTabDisplayed("Archived"),
                "Archived tab not displayed.");

        log.info("Click on Archived tab.");
        tasksPage.clickOnTab("Archived");

        log.info("Verify Archived tab displayed.");
        Assert.assertTrue(tasksPage.isTabDisplayed("Archived"),
                "Archived tab not displayed.");

        log.info("Verify Archived task is selected by default.");
        Assert.assertTrue(tasksPage.isTabSelected("Archived"),
                "Archived tab not selected under Tasks.");

        log.info("Verify Active task not selected after clicked on Archived.");
        Assert.assertFalse(tasksPage.isTabSelected("Active"),
                "Active tab should not be selected under Tasks when Archived is selected.");
    }

    private void verifyActiveTab(TasksPage tasksPage) {
        log.info("Verify Active tab displayed.");
        Assert.assertTrue(tasksPage.isTabDisplayed("Active"),
                "Active tab not displayed.");

        log.info("Verify Active task is selected by default.");
        Assert.assertTrue(tasksPage.isTabSelected("Active"),
                "Active tab not selected under Tasks.");

        log.info("Verify Archived task not selected by default.");
        Assert.assertFalse(tasksPage.isTabSelected("Archived"),
                "Archived tab should not be selected under Tasks when Active is selected.");
    }

    @Then("verify Task page navigated successfully")
    public void verifyTaskPageNavigatedSuccessfully() {
        TasksPage webPage = new TasksPage(driver);
        log.info("Verify Task page displayed successfully.");
        Assert.assertTrue(webPage.isTaskPageDisplayed(), "Task page not displayed.");
    }

    @Then("^verify create new task under (Tasks|Patient) Page with (Required|All) fields in HCP web$")
    public void verifyCreateNewTaskUnderTasksPageWithRequiredFieldsInHCPWeb(String arg0, String arg1) throws Exception {
        TasksPage webPage = new TasksPage(driver);
        if (arg0.equalsIgnoreCase("Patient")) {
            log.info("Click on Task icon.");
            webPage.clickOnTaskIcon();

            log.info("Click on New Task.");
            webPage.clickOnNewTask();
        } else {
            log.info("Search for patient mrn.");
            webPage.searchForPatientInTasks(testConfig.getPrimaryPatientMRN());

            log.info("Click on New Task.");
            webPage.clickOnNewTaskOnTasks(testConfig.getPrimaryPatientMRN());
        }

        if (arg1.equalsIgnoreCase("Required"))
            createTaskWithRequired(webPage);
        else
            createTaskWithAll(webPage);
    }

    private void createTaskWithAll(TasksPage tasksPage) throws Exception {
        log.info("Create task details.");
        getTaskValue(true);

        log.info("Create task with all fields");
        log.info("Input task description.");
        tasksPage.inputTaskDescription(details.get("Description"));

        log.info("Select due date time.");
        String due = tasksPage.setDueDate();

        log.info("Select Assigned To . [" + testConfig.getNurseName() + "]");
        tasksPage.selectAssigneeTo(testConfig.getNurseName());

        log.info("Click on Create task.");
        tasksPage.createTask();

        details.put("AssignedOn", getAssignedOn());
        details.put("DueDate", due);
        details.put("AssignedTo", testConfig.getNurseName());

        log.info("Search and click on created task. [" + details.get("Description") + "]");
        tasksPage.viewTask(details.get("Description"));

        log.info("Verify task created successfully.");
        tasksPage.verifyCreatedTaskSuccessfully(details.get("Description"));

        log.info("Verify task Assigned By [" + details.get("AssignedBy") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedBy(details.get("Description"), details.get("AssignedBy")),
                "Assigned By value not matched. Expected [" + details.get("AssignedBy") + "]");

        log.info("Verify task Assigned On [" + details.get("AssignedOn") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedOn(details.get("Description"), details.get("AssignedOn")),
                "Assigned On value not matched. Expected [" + details.get("AssignedOn") + "]");

        log.info("Verify task Assigned To [" + details.get("AssignedTo") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedTo(details.get("Description"), details.get("AssignedTo")),
                "Assigned To value not matched. Expected [" + details.get("AssignedTo") + "]");

        log.info("Verify task Due Date [" + details.get("DueDate") + "].");
        Assert.assertTrue(tasksPage.verifyDueDate(details.get("Description"), details.get("DueDate")),
                "Due Date value not matched. Expected [" + details.get("DueDate") + "]");
    }

    private String getAssignedOn() {
        String pattern = "hh:mm a";
        DateFormat dateFormat = new SimpleDateFormat(pattern);

        LocalTime now = LocalTime.now();
        String time = now.format(DateTimeFormatter.ofPattern(pattern)).strip();

        if (time.startsWith("0"))
            return time.substring(1, time.length());
        else
            return time;
    }

    private void createTaskWithRequired(TasksPage tasksPage) throws Exception {
        log.info("Create task details.");
        getTaskValue(false);

        log.info("Create task with required fields");

        log.info("Input task description.");
        tasksPage.inputTaskDescription(details.get("Description"));

        log.info("Click on Create task.");
        tasksPage.createTask();

        details.put("AssignedOn", getAssignedOn());

        log.info("Search and click on created task. [" + details.get("Description") + "]");
        tasksPage.viewTask(details.get("Description"));

        log.info("Verify task created successfully.");
        tasksPage.verifyCreatedTaskSuccessfully(details.get("Description"));

        log.info("Verify task Assigned By [" + details.get("AssignedBy") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedBy(details.get("Description"), details.get("AssignedBy")),
                "Assigned By value not matched. Expected [" + details.get("AssignedBy") + "]");

        log.info("Verify task Assigned On [" + details.get("AssignedOn") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedOn(details.get("Description"), details.get("AssignedOn")),
                "Assigned On value not matched. Expected [" + details.get("AssignedOn") + "]");

        log.info("Verify task Assigned To [" + details.get("AssignedTo") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedTo(details.get("Description"), details.get("AssignedTo")),
                "Assigned To value not matched. Expected [" + details.get("AssignedTo") + "]");

        log.info("Verify task Due Date [" + details.get("DueDate") + "].");
        Assert.assertTrue(tasksPage.verifyDueDate(details.get("Description"), details.get("DueDate")),
                "Due Date value not matched. Expected [" + details.get("DueDate") + "]");
    }

    @And("^verify task comment on (Patient|Task) page$")
    public void verifyTaskCommentOnPatientPage(String arg0) throws Exception {
        TasksPage webPage = new TasksPage(driver);
        if (arg0.equalsIgnoreCase("Task")) {
            log.info("Verify comment on Tasks page.");
            verifyCommentOnTasksSec(webPage);
        } else {
            log.info("Verify comment on Patient details page.");
            verifyCommentOnPatientSec(webPage);
        }
    }

    private void verifyCommentOnPatientSec(TasksPage taskPage) throws Exception {
        String desc = details.get("Description");
        log.info("View task [" + desc + "].");
        taskPage.viewTask(desc);
        String comment = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();

        log.info("Verify comment box label.");
        Assert.assertTrue(taskPage.verifyCommentLabel(), "Comment place holder not displayed. [Leave a comment]");

        log.info("Add comment [" + comment + "].");
        taskPage.addComment(comment);

        String commentData = testConfig.getClinicianName() + ", Today, " + getAssignedOn();
        Assert.assertTrue(taskPage.verifyAddedComment(comment),
                "Comment not matched. Expected comment [" + comment + "].");

        Assert.assertTrue(taskPage.verifyCloseIcon(),
                "Task not closed after clicking on close icon.");

        log.info("Click on task icon.");
        taskPage.clickOnTaskIcon();
        log.info("View task [" + desc + "].");
        taskPage.viewTask(desc);
        Assert.assertTrue(taskPage.verifyAddedComment(comment),
                "Comment not matched. Expected comment [" + comment + "].");

        log.info("Verify Commented By and comment posted date time.");
        Assert.assertTrue(taskPage.verifyCommentDetails(comment, commentData),
                "Comment details not matched. Expected details [" + commentData + "]");
    }

    private void verifyCommentOnTasksSec(TasksPage taskPage) throws Exception {
        String desc = details.get("Description");
        log.info("View task [" + desc + "].");
        taskPage.viewTask(desc);
        String comment = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();

        log.info("Verify comment box label.");
        Assert.assertTrue(taskPage.verifyCommentLabel(), "Comment place holder not displayed. [Leave a comment]");

        log.info("Add comment [" + comment + "].");
        taskPage.addComment(comment);

        String commentData = testConfig.getClinicianName() + ", Today, " + getAssignedOn();
        Assert.assertTrue(taskPage.verifyAddedComment(comment),
                "Comment not matched. Expected comment [" + comment + "].");

        Assert.assertTrue(taskPage.verifyAddedComment(comment),
                "Comment not matched. Expected comment [" + comment + "].");

        Assert.assertTrue(taskPage.verifyCloseIcon(),
                "Task not closed after clicking on close icon.");

        log.info("View task [" + desc + "].");
        taskPage.viewTask(desc);
        Assert.assertTrue(taskPage.verifyAddedComment(comment),
                "Comment not matched. Expected comment [" + comment + "].");

        log.info("Verify Commented By and comment posted date time.");
        Assert.assertTrue(taskPage.verifyCommentDetails(comment, commentData),
                "Comment details not matched. Expected details [" + commentData + "]");
    }

    @Then("^verify edit task with (Required|All) fields$")
    public void verifyEditTaskWithRequiredFields(String arg0) throws Exception {

        TasksPage webPage = new TasksPage(driver);

        log.info("Verify Edit button displayed.");
        Assert.assertTrue(webPage.isEditButtonDisplayed(), "Edit button not displayed om task tab.");

        log.info("Verify Edit icon displayed.");
        Assert.assertTrue(webPage.isEditIconDisplayed(), "Edit icon not displayed om task tab.");

        log.info("Click on Edit task button.");
        webPage.clickOnEditTask();

        log.info("Update task details.");
        updateTaskValue();

        if (arg0.equalsIgnoreCase("All"))
            editTaskAllFields(webPage);
        else
            editRequiredFields(webPage, true);
    }

    private void editTaskAllFields(TasksPage tasksPage) throws Exception {
        log.info("Update task description.");
        tasksPage.updateDescription(updatedDetails.get("Description"));

        log.info("Update task due date time.");
        tasksPage.updateDueDate();

        log.info("Update Assign To.");
        tasksPage.selectAssigneeTo(testConfig.getClinicianName());

        log.info("Click on Cancel.");
        tasksPage.clickOnCancel();

        /**
         * VERIFY NO CHANGES UPDATE
         */
        log.info("Search and click on created task. [" + details.get("Description") + "]");
        tasksPage.viewTask(details.get("Description"));

        log.info("Verify task created successfully.");
        tasksPage.verifyCreatedTaskSuccessfully(details.get("Description"));

        log.info("Verify task Assigned By [" + details.get("AssignedBy") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedBy(details.get("Description"), details.get("AssignedBy")),
                "Assigned By value not matched. Expected [" + details.get("AssignedBy") + "]");

        log.info("Verify task Assigned On [" + details.get("AssignedOn") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedOn(details.get("Description"), details.get("AssignedOn")),
                "Assigned On value not matched. Expected [" + details.get("AssignedOn") + "]");

        log.info("Verify task Assigned To [" + details.get("AssignedTo") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedTo(details.get("Description"), details.get("AssignedTo")),
                "Assigned To value not matched. Expected [" + details.get("AssignedTo") + "]");

        log.info("Verify task Due Date [" + details.get("DueDate") + "].");
        Assert.assertTrue(tasksPage.verifyDueDate(details.get("Description"), details.get("DueDate")),
                "Due Date value not matched. Expected [" + details.get("DueDate") + "]");

        /**
         * Enter details again
         */

        log.info("Click on edit button.");
        tasksPage.clickOnEditTask();

        log.info("Update task description.");
        tasksPage.updateDescription(updatedDetails.get("Description"));

        log.info("Update task due date time.");
        String dueDate = tasksPage.updateDueDate();

        log.info("Update Assign To.");
        tasksPage.selectAssigneeTo(testConfig.getClinicianName());

        log.info("Click on Update.");
        tasksPage.clickOnUpdate();

        updatedDetails.put("DueDate", dueDate);
        updatedDetails.put("AssignedTo", testConfig.getClinicianName());
        updatedDetails.put("UpdatedBy", testConfig.getClinicianName());
        updatedDetails.put("UpdatedOn", getAssignedOn());

        log.info("Update Previous task details into dic.");
        details.putAll(updatedDetails);

        /**
         * VERIFY NO CHANGES UPDATE
         */
        log.info("Search and click on created task. [" + updatedDetails.get("Description") + "]");
        tasksPage.viewTask(updatedDetails.get("Description"));

        log.info("Verify task created successfully.");
        tasksPage.verifyCreatedTaskSuccessfully(details.get("Description"));

        log.info("Verify task Assigned By [" + details.get("AssignedBy") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedBy(details.get("Description"), details.get("AssignedBy")),
                "Assigned By value not matched. Expected [" + details.get("AssignedBy") + "]");

        log.info("Verify task Assigned On [" + details.get("AssignedOn") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedOn(details.get("Description"), details.get("AssignedOn")),
                "Assigned On value not matched. Expected [" + details.get("AssignedOn") + "]");

        log.info("Verify task Assigned To [" + details.get("AssignedTo") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedTo(details.get("Description"), details.get("AssignedTo")),
                "Assigned To value not matched. Expected [" + details.get("AssignedTo") + "]");

        log.info("Verify task Due Date [" + details.get("DueDate") + "].");
        Assert.assertTrue(tasksPage.verifyDueDate(details.get("Description"), details.get("DueDate")),
                "Due Date value not matched. Expected [" + details.get("DueDate") + "]");

        log.info("Verify task Updated By [" + details.get("UpdatedBy") + "].");
        Assert.assertTrue(tasksPage.verifyUpdatedBy(details.get("Description"), details.get("UpdatedBy")),
                "Updated By value not matched. Expected [" + details.get("UpdatedBy") + "]");

        log.info("Verify task Updated on [" + details.get("UpdatedOn") + "].");
        Assert.assertTrue(tasksPage.verifyUpdatedOn(details.get("Description"), details.get("UpdatedOn")),
                "Updated On  value not matched. Expected [" + details.get("UpdatedOn") + "]");

        /**
         * TILL HERE
         */

    }

    private void editRequiredFields(TasksPage tasksPage, boolean removeOptional) throws Exception {

        log.info("Update task description.");
        tasksPage.updateDescription(updatedDetails.get("Description"));

        log.info("Remove task due date time.");
        tasksPage.removeDueDate();

        log.info("Remove task Assign To.");
        tasksPage.removeAssignedTo();

        log.info("Click on Cancel.");
        tasksPage.clickOnCancel();

        log.info("Verify task details should not changed.");

        log.info("Click on Update.");

        /**
         * VERIFY NO CHANGES UPDATE
         */
        log.info("Search and click on created task. [" + details.get("Description") + "]");
        tasksPage.viewTask(details.get("Description"));

        log.info("Verify task created successfully.");
        tasksPage.verifyCreatedTaskSuccessfully(details.get("Description"));

        log.info("Verify task Assigned By [" + details.get("AssignedBy") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedBy(details.get("Description"), details.get("AssignedBy")),
                "Assigned By value not matched. Expected [" + details.get("AssignedBy") + "]");

        log.info("Verify task Assigned On [" + details.get("AssignedOn") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedOn(details.get("Description"), details.get("AssignedOn")),
                "Assigned On value not matched. Expected [" + details.get("AssignedOn") + "]");

        log.info("Verify task Assigned To [" + details.get("AssignedTo") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedTo(details.get("Description"), details.get("AssignedTo")),
                "Assigned To value not matched. Expected [" + details.get("AssignedTo") + "]");

        log.info("Verify task Due Date [" + details.get("DueDate") + "].");
        Assert.assertTrue(tasksPage.verifyDueDate(details.get("Description"), details.get("DueDate")),
                "Due Date value not matched. Expected [" + details.get("DueDate") + "]");

        /**
         * Enter details again
         */

        log.info("Click on edit button.");
        tasksPage.clickOnEditTask();

        log.info("Update task description.");
        tasksPage.updateDescription(updatedDetails.get("Description"));

        log.info("Remove task due date time.");
        tasksPage.removeDueDate();

        log.info("Remove task Assign To.");
        tasksPage.removeAssignedTo();

        log.info("Click on Update.");
        tasksPage.clickOnUpdate();

        updatedDetails.put("DueDate", "--");
        updatedDetails.put("AssignedTo", "--");
        updatedDetails.put("UpdatedBy", testConfig.getClinicianName());
        updatedDetails.put("UpdatedOn", getAssignedOn());

        log.info("Update Previous task details into dic.");
        details.putAll(updatedDetails);

        /**
         * VERIFY NO CHANGES UPDATE
         */
        log.info("Search and click on created task. [" + updatedDetails.get("Description") + "]");
        tasksPage.viewTask(updatedDetails.get("Description"));

        log.info("Verify task created successfully.");
        tasksPage.verifyCreatedTaskSuccessfully(details.get("Description"));

        log.info("Verify task Assigned By [" + details.get("AssignedBy") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedBy(details.get("Description"), details.get("AssignedBy")),
                "Assigned By value not matched. Expected [" + details.get("AssignedBy") + "]");

        log.info("Verify task Assigned On [" + details.get("AssignedOn") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedOn(details.get("Description"), details.get("AssignedOn")),
                "Assigned On value not matched. Expected [" + details.get("AssignedOn") + "]");

        log.info("Verify task Assigned To [" + details.get("AssignedTo") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedTo(details.get("Description"), details.get("AssignedTo")),
                "Assigned To value not matched. Expected [" + details.get("AssignedTo") + "]");

        log.info("Verify task Due Date [" + details.get("DueDate") + "].");
        Assert.assertTrue(tasksPage.verifyDueDate(details.get("Description"), details.get("DueDate")),
                "Due Date value not matched. Expected [" + details.get("DueDate") + "]");

        log.info("Verify task Updated By [" + details.get("UpdatedBy") + "].");
        Assert.assertTrue(tasksPage.verifyUpdatedBy(details.get("Description"), details.get("UpdatedBy")),
                "Updated By value not matched. Expected [" + details.get("UpdatedBy") + "]");

        log.info("Verify task Updated on [" + details.get("UpdatedOn") + "].");
        Assert.assertTrue(tasksPage.verifyUpdatedOn(details.get("Description"), details.get("UpdatedOn")),
                "Updated On  value not matched. Expected [" + details.get("UpdatedOn") + "]");

        /**
         * TILL HERE
         */

    }

    @Then("verify completed task section displayed correctly")
    public void verifyCompletedTaskSectionDisplayedCorrectly() throws Exception {
        TasksPage tasksPage = new TasksPage(driver);
        log.info("Verify completed task page.");

        log.info("Verify task Assigned By [" + details.get("AssignedBy") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedBy(details.get("Description"), details.get("AssignedBy")),
                "Assigned By value not matched. Expected [" + details.get("AssignedBy") + "]");

        log.info("Verify task Assigned On [" + details.get("AssignedOn") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedOn(details.get("Description"), details.get("AssignedOn")),
                "Assigned On value not matched. Expected [" + details.get("AssignedOn") + "]");

        log.info("Verify task Assigned To [" + details.get("AssignedTo") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedTo(details.get("Description"), details.get("AssignedTo")),
                "Assigned To value not matched. Expected [" + details.get("AssignedTo") + "]");

        log.info("Verify task Due Date [" + details.get("DueDate") + "].");
        Assert.assertTrue(tasksPage.verifyDueDate(details.get("Description"), details.get("DueDate")),
                "Due Date value not matched. Expected [" + details.get("DueDate") + "]");

        log.info("Verify task Completed by [" + details.get("CompletedBy") + "].");
        Assert.assertTrue(tasksPage.verifyCompletedBy(details.get("Description"), details.get("CompletedBy")),
                "Completed by value not matched. Expected [" + details.get("CompletedBy") + "]");

        log.info("Verify task Completed on [" + details.get("CompletedOn") + "].");
        Assert.assertTrue(tasksPage.verifyCompletedOn(details.get("Description"), details.get("CompletedOn")),
                "Completed on value not matched. Expected [" + details.get("CompletedOn") + "]");

        //Verify active comment section.
        String comment = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();

        log.info("Verify comment box label.");
        Assert.assertTrue(tasksPage.verifyCommentLabel(), "Comment place holder not displayed. [Leave a comment]");

        log.info("Add comment [" + comment + "].");
        tasksPage.addComment(comment);

        String commentData = testConfig.getClinicianName() + ", Today, " + getAssignedOn();
        Assert.assertTrue(tasksPage.verifyAddedComment(comment),
                "Comment not matched. Expected comment [" + comment + "].");

        Assert.assertTrue(tasksPage.verifyAddedComment(comment),
                "Comment not matched. Expected comment [" + comment + "].");

        log.info("Verify Commented By and comment posted date time.");
        Assert.assertTrue(tasksPage.verifyCommentDetails(comment, commentData),
                "Comment details not matched. Expected details [" + commentData + "]");

        //Reopen Button
        log.info("Verify Reopen button displayed.");
        Assert.assertTrue(tasksPage.isReopenButtonDisplayed(),
                "Reopen button not displayed on completed task.");

        //Close Icon
        log.info("Verify Close icon displayed.");
        Assert.assertTrue(tasksPage.isCloseIconDisplayed(),
                "Close icon not displayed.");
    }

    @Then("^verify cancel button functionality on (Patient|Tasks) page while creating new task$")
    public void verifyCancelButtonFunctionalityWhileCreatingNewTask(String arg0) throws Exception {
        TasksPage webPage = new TasksPage(driver);
        if (arg0.equalsIgnoreCase("Patient")) {
            log.info("Click on Task icon.");
            webPage.clickOnTaskIcon();

            log.info("Click on New Task.");
            webPage.clickOnNewTask();
        } else {
            log.info("Search for patient mrn.");
            webPage.searchForPatientInTasks(testConfig.getPrimaryPatientMRN());

            log.info("Click on New Task.");
            webPage.clickOnNewTaskOnTasks(testConfig.getPrimaryPatientMRN());
        }

        log.info("Create task details.");
        getTaskValue(true);

        log.info("Create task with all fields");
        log.info("Input task description.");
        webPage.inputTaskDescription(details.get("Description"));

        log.info("Select due date time.");
        String due = webPage.setDueDate();

        log.info("Select Assigned To . [" + testConfig.getNurseName() + "]");
        webPage.selectAssigneeTo(testConfig.getNurseName());

        log.info("Click on cancel button.");
        webPage.clickOnCancel();

        log.info("Search and click on created task. [" + details.get("Description") + "]");
        Assert.assertFalse(webPage.isTaskPresent(details.get("Description")),
                "Task should not be created with description [" + details.get("Description") + "] after clicking on cancel button.");
    }

    //**************************************************************************************\\

     @And("Search patient name under tasks")
    public void searchPatientNameUnderTasks() throws Exception {
        taskPageApp = new TasksPageApp(clinician);
        if (CustomUtils.mnr != null)
            patientMrn = CustomUtils.mnr;
        else
            patientMrn = testConfig.getPrimaryPatientMRN();

        log.info("CLick on Tasks tab.");
        //Instead of this navigate to task by search patient and click on View task.
         taskPageApp.clickOnTasks();
        log.info("Scroll till patient given MRN.");
         taskPageApp.scrollTillPatient(patientMrn);

        log.info("Read open task value.");
        openTaskValue = taskPageApp.getOpenTasksValue(patientMrn);
        log.info("Click on Add new task icon.");
         taskPageApp.clickOnAddNewTaskIcon(patientMrn);
    }

    @And("^Verify create new task in HCP app with (Required|All) fields$")
    public void verifyCreateNewTaskInHCPAppWithRequiredFields(String arg0) throws Exception {
        boolean flag = arg0.equals("All");
        log.info("Verify create task button should be disable when all fields are empty.");
        Assert.assertFalse(taskPageApp.verifyCreateButtonEnable(),
                "Create button should be disable before entering required field details.");

        log.info("Get values to create a task.");
        details = getTaskValue(true);

        log.info("Add task details.");
        taskPageApp.addNewTaskDetails(details, flag);

        log.info("Add task description.");
        taskPageApp.updateTaskDescription(details.get("Description"));

        if (flag) {
            log.info("Add due date.");
            details = taskPageApp.addDueDateTime(details);
            log.info("Add assignee.");
            taskPageApp.addAssignee(details.get("Assignee"));
        }

        log.info("Verify Created button should be enable after entering required fields.");
        Assert.assertTrue(taskPageApp.verifyCreateButtonEnable(),
                "Create button should be enable after entering required field details.");

        log.info("Click on create button.");
        taskPageApp.clickOnCreate();

        log.info("Get updated open task value.");
        String newOpenTaskValue = taskPageApp.getOpenTasksValue(patientMrn);

        log.info("Verifying old Open task value with latest Open task value.");
        Assert.assertFalse(openTaskValue.equalsIgnoreCase(newOpenTaskValue),
                "Open task count not updated after adding new task. Value before adding [" + openTaskValue + "] after adding [" + newOpenTaskValue + "].");

        log.info("Verify created task displayed under open tasks.");
        Assert.assertTrue(taskPageApp.verifyTaskAddedSuccessFully(patientMrn, details),
                "Added task with description [" + details.get("Description") + " not displayed.");

        if (flag) {
            log.info("Navigate to created task.");
            taskPageApp.viewTask(details.get("Description"));
            String expected = details.get("Assignee").strip(), actual = taskPageApp.verifyUpdatedAssignee().strip();
            Assert.assertTrue(expected.equalsIgnoreCase(actual),
                    "Expected assignee is [" + expected + "] and actual assignee is [" + actual + "].");
            expected = details.get("Due");
            actual = taskPageApp.verifyUpdatedDueDate();

            Assert.assertTrue(expected.equalsIgnoreCase(actual),
                    "Expected Due date is [" + expected + "] and actual Due date is [" + actual + "].");
        }
    }

    @Then("Verify comment on created task in HCP app")
    public void verifyCommentOnCreatedTaskInHCPApp() throws Exception {
        log.info("Navigate to created task.");
        taskPageApp.viewTask(details.get("Description"));

        String comment = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();
        log.info("Add comment in task. [" + comment + "].");
        taskPageApp.addComment(comment);

        log.info("Scroll till comment");
        taskPageApp.scrollComments();
        log.info("Verify commented value should match.");
        Assert.assertTrue(taskPageApp.verifyCommentAddSuccessfully(comment),
                "Comment [" + comment + "] not displayed.");
    }

    @And("^Verify edit a task in HCP app with (All|Required) fields$")
    public void verifyEditATaskInHCPAppWithRequiredFields(String arg0) throws Exception {
        log.info("Click on Edit task.");
        taskPageApp.clickOnEditTask();
        boolean flag = arg0.equals("All");
        String desc = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();
        details.replace("Description", desc);
        taskPageApp.updateTaskDescription(desc);
        if (flag) {
            log.info("Update due date.");
            details = taskPageApp.addDueDateTime(details);
            log.info("Update assignee name.");
            taskPageApp.addAssignee(details.get("Assignee"));
        }
        log.info("Click on update task.");
        taskPageApp.updateTask();

        desc = details.get("Description");
        Assert.assertTrue(taskPageApp.verifyUpdatedTaskDescription(desc),
                "Expected updated task description is [" + desc + "].");
        if (flag) {
            String expected = details.get("Assignee").strip(),
                    actual = taskPageApp.verifyUpdatedAssignee().strip();
            Assert.assertTrue(expected.equalsIgnoreCase(actual),
                    "Expected assignee is [" + expected + "] and actual assignee is [" + actual + "].");
            expected = details.get("Due");
            actual = taskPageApp.verifyUpdatedDueDate();

            Assert.assertTrue(expected.equalsIgnoreCase(actual),
                    "Expected Due date is [" + expected + "] and actual Due date is [" + actual + "].");
        }
    }

    @Then("Verify complete a task in HCP app")
    public void verifyCompleteATaskInHCPApp() throws Exception {
        log.info("Click on Complete task.");
        taskPageApp.completeATask();
        log.info("Click on Completed task section.");
        taskPageApp.clickOnCompletedTask(patientMrn);

        log.info("Verify task displayed under complete task section.");
        String desc = details.get("Description");
        Assert.assertTrue(taskPageApp.verifyTaskDisplayedUnderCompleted(desc),
                "Task with desc [" + desc + "] should be displayed.");
    }

    @And("Verify comment on completed task in HCP app")
    public void verifyCommentOnCompletedTaskInHCPApp() throws Exception {
        verifyCommentOnCreatedTaskInHCPApp();
    }

    @Then("Verify re-open completed task in HCP app")
    public void verifyReOpenCompletedTaskInHCPApp() throws Exception {
        log.info("Click on complete a task.");
        taskPageApp.completeATask();
        log.info("Click on Open task section.");
        taskPageApp.clickOnOpen(patientMrn);
        log.info("Verify re-open task should be displayed under Open task section.");
        String desc = details.get("Description");
        Assert.assertTrue(taskPageApp.verifyReOpenTask(desc),
                "Re-opened task should be displayed under Open task section with description [" + desc + "].");
    }

    @And("Verify comment on re-opened task in HCP app")
    public void verifyCommentOnReOpenedTaskInHCPApp() throws Exception {
        log.info("Navigate to task.");
        taskPageApp.viewTask(details.get("Description"));
        verifyCommentOnCreatedTaskInHCPApp();
    }

    @Then("^Verify edit re-open task in HCP app with (All|Required) fields$")
    public void verifyEditReOpenTaskInHCPAppWithAllFields(String arg0) throws Exception {
        log.info("Click on Edit task.");
        taskPageApp.clickOnEditTask();
        boolean flag = arg0.equals("All");
        String desc = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();
        details.replace("Description", desc);

        log.info("Update task description.");
        taskPageApp.updateTaskDescription(desc);
        if (flag) {
            log.info("Update task due date.");
            details = taskPageApp.addDueDateTime(details);
            log.info("Edit assignee.");
            taskPageApp.addAssignee(details.get("Assignee"));
        }
        log.info("Click on update task.");
        taskPageApp.updateTask();

        log.info("Verify task description.");
        Assert.assertTrue(taskPageApp.verifyUpdatedTaskDescription(desc),
                "Task description [" + desc + "] should be displayed after updating.");
        if (flag) {
            String expected = details.get("Assignee").strip(),
                    actual = taskPageApp.verifyUpdatedAssignee().strip();
            Assert.assertTrue(expected.equalsIgnoreCase(actual),
                    "Expected assignee is [" + expected + "] and actual assignee is [" + actual + "].");
            expected = details.get("Due");
            actual = taskPageApp.verifyUpdatedDueDate();

            Assert.assertTrue(expected.equalsIgnoreCase(actual),
                    "Expected Due date is [" + expected + "] and actual Due date is [" + actual + "].");
        }

    }

    @Then("Verify complete a re-open task in HCP app")
    public void verifyCompleteAReOpenTaskInHCPApp() throws Exception {
        verifyCompleteATaskInHCPApp();
    }

    @Then("Verify Tasks screen content")
    public void verifyTasksScreenContent() {
        taskPageApp = new TasksPageApp(clinician);
        log.info("Verify Task title should be displayed.");
        Assert.assertTrue(taskPageApp.isTitleDisplayed(), "Task tab title not displayed.");

        log.info("Verify Active tasks option should be displayed.");
        Assert.assertTrue(taskPageApp.isTabOptionDisplayed("Active"), "Active tab not displayed.");

        log.info("Verify Archived tasks option should be displayed.");
        Assert.assertTrue(taskPageApp.isTabOptionDisplayed("Archived"), "Archived tab not displayed.");
    }

    @And("Go back to home screen and navigate to Tasks section")
    public void goBackToHomeScreenAndNavigateToTasksSection() throws Exception {
        log.info("Click on Patients tab.");
        taskPageApp.clickOnPatients();

        log.info("Click on Tasks tab.");
        taskPageApp.clickOnTasks();

        log.info("Verify Task screen options.");
        verifyTasksScreenContent();
    }

    //XCUIElementTypeOther[@name="TR"]//following-sibling:: XCUIElementTypeCell[1]//*[@label='ic arrow down detail']
    //Open(3)
    //Completed(0)
    //XCUIElementTypeOther[@name='AS']//*[@name='ic add']

    //XCUIElementTypeNavigationBar[@name="New Task"]

    //XCUIElementTypeStaticText[@name="Task description*"]

    //XCUIElementTypeStaticText[@name="Due date & time"]
    //XCUIElementTypeStaticText[@name="Assign to"]
    //XCUIElementTypeStaticText[@name="Create"]

    //XCUIElementTypeImage[@name="ic-calendar"]
    //XCUIElementTypeButton[@name="ic edit task clear"]
    //On Jan 19,2022 at 8:27 PM

    //XCUIElementTypeImage[@name="ic-arrow-next-detail"]
    //XCUIElementTypeStaticText[@name="Select care team member"]


    //XCUIElementTypeButton[@name="Tasks"]

    //XCUIElementTypeNavigationBar[@name="Select care team member"]
    //XCUIElementTypeButton[@name="New Task"]
    //XCUIElementTypeStaticText[@name="Select care team member"]
    ////XCUIElementTypeStaticText[@name="Assign"]
    ////XCUIElementTypeStaticText[@name="Physicians"]

}
