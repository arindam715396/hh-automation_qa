Feature: HCP web [Tasks] functionality verification

  Background:
    Given Launch the browser
    And Login into dashboard

  @TestCaseKey=HOHP-T687 @TestCaseKey=HOHP-T688 @TestCaseKey=HOHP-T689
  Scenario: Verify Active Task Tab
    When Navigate to Tasks tab on HCP web dashboard
    Then verify Task page navigated successfully
    And verify Active task tab
    And verify Archived task tab

  @TestCaseKey=HOHP-T698 @TestCaseKey=HOHP-T701 @TestCaseKey=HOHP-T710
  Scenario: Verify User is able to add/edit new task for patients on individual patient page
    When Navigate to patient under my patients
    Then verify create new task under Patient Page with Required fields in HCP web
    And verify task comment on Patient page
    Then verify edit task with All fields

  @TestCaseKey=HOHP-T699 @TestCaseKey=HOHP-T700 @TestCaseKey=HOHP-T709
  Scenario: Verify User is able to add/edit new task for patients under Tasks Page
    When Navigate to Tasks tab on HCP web dashboard
    Then verify create new task under Tasks Page with All fields in HCP web
    And verify task comment on Task page
    Then verify edit task with Required fields

  @TestCaseKey=HOHP-T703 @TestCaseKey=HOHP-T705 @TestCaseKey=HOHP-T693
  Scenario: Verify User is able to complete and reopen the task from Individual patient page
    When Navigate to patient under my patients
    Then verify create new task under Patient Page with All fields in HCP web
    When Verify complete a task
    Then verify completed task section displayed correctly
    And Verify re-open completed task

  @TestCaseKey=HOHP-T702 @TestCaseKey=HOHP-T704 @TestCaseKey=HOHP-T691
  Scenario: Verify User is able to complete and reopen the task from Tasks Page
    When Navigate to Tasks tab on HCP web dashboard
    Then verify create new task under Tasks Page with Required fields in HCP web
    And Verify complete a task
    Then verify completed task section displayed correctly
    And Verify re-open completed task on tasks tab

  @TestCaseKey=HOHP-T696
  Scenario: Verify Cancel button works as expected when adding new task on Task Page
    When Navigate to Tasks tab on HCP web dashboard
    Then verify cancel button functionality on Tasks page while creating new task

  @TestCaseKey=HOHP-T697
  Scenario: Verify Cancel button works as expected when adding new task on Individual patient page
    When Navigate to patient under my patients
    Then verify cancel button functionality on Patient page while creating new task

  Scenario: To verify task container and cancel options for creation of task
    Then Navigate to patient "MRN1000" under my patients
    And Verify closing task container
    Then Verify Cancel button functionality

  Scenario: To verify Add, Edit, Comment and Complete a task with required fields on patient details page
    Then Navigate to patient "MRN1000" under my patients
    And Verify create new task with Required fields
    Then Verify comment on created task
    And Verify edit a task with Required fields
    Then Verify complete a task
    And Verify comment on completed task
    Then Verify re-open completed task
    And Verify comment on re-opened task
    Then Verify edit re-open task with All fields
    Then Verify complete a re-open task

  Scenario: To verify Add, Edit, Comment and Complete a task with all fields on patient details page
    Then Navigate to patient "MRN1000" under my patients
    And Verify create new task with All fields
    Then Verify comment on created task
    And Verify edit a task and remove optional fields
    Then Verify complete a task
    And Verify comment on completed task
    Then Verify re-open completed task
    And Verify comment on re-opened task
    Then Verify edit re-open task with All fields
    Then Verify complete a re-open task

  Scenario: To verify Add, Edit, Comment and Complete a task with required/all fields from tasks LHN menu
    Then Navigate to Tasks tab on HCP web dashboard
    And Verify create new task with Required fields on tasks tab
    Then Verify comment on created task on tasks tab
    And Verify edit a task with Required fields on tasks tab
    Then Verify complete a task
    And Verify comment on completed task on tasks tab
    Then Verify re-open completed task on tasks tab
    And Verify comment on re-opened task on tasks tab
    Then Verify edit re-open task with All fields on tasks tab
    Then Verify complete a re-open task