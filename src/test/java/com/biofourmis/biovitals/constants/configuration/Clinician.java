package com.biofourmis.biovitals.constants.configuration;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

@Data
public class Clinician {

    @JsonAlias({"user", "User Name"})
    private String user;
    @JsonAlias({"mail", "Mail"})
    private String mail;
    @JsonAlias({"password", "Password"})
    private String password;
    @JsonAlias({"role", "Role"})
    private String role;
}
