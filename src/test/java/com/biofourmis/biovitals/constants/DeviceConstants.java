package com.biofourmis.biovitals.constants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This class is read only do not modify anything here
 */
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class DeviceConstants {

    private String udid;
    private String devicePlatform;
    private String buildVersion;
    private String modelNumber;
    private String productVersion;
    private String serialNumber;
    private String integratedCircuitCardIdentity;
    private boolean isActive;

}
