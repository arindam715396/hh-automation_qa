Feature: HCP web [LHN Menus] verification flow

  Background:
    Given Launch the browser
    And Login into dashboard

  @HOHP-T258
  Scenario: To verify navigation on LHN menus and respected pages
    Then Verify LHN menu options displayed
      | My patients  |
      | All patients |
      | Discharged   |
      | Tasks        |
      | Inventory    |
    And Verify each menu is clickable and should navigate to respected page

