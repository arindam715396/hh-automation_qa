Feature: HCP web [Activity and Care plan] flow verification

  Background:
    Given Launch the browser
    Then Login into dashboard

  Scenario: Prerequisite
    When Add a new patient
    Then Launch the "HH" App with reset
    And Scan QR and login into app

  @TestCaseKey=HOHP-T294
  Scenario: To verify Care Plan container under Activities and Care plan tab
    And Navigate to patient under my patients
    Then Verify Care plan page

  Scenario: To verify Add and delete a Care Plan from HCP web
    And Navigate to patient under my patients
    Then Verify Add a Care plan
    And Verify Delete a Care plan

  @TestCaseKey=HOHP-T295 @TestCaseKey=HOHP-T296
  Scenario: To verify Add and Delete Care Plan on Patient app
    And Launch the "HH" App with noReset
    Then Navigate to patient under my patients
    Then Verify Add a Care plan
    Then Verify Added Care plan displayed in Patient App
    And Verify Delete a Care plan
    Then Verify Deleted Care plan displayed in Patient App

  @TestCaseKey=HOHP-T298
  Scenario: Try to add care plan for other patients
    Then Navigate to patient under all patients
    And Adding a care plan should not be applicable for other patients

  @TestCaseKey=HOHP-T279
  Scenario: To verify Activity Page details under my patients
    And Navigate to patient under my patients
    Then Verify Activity Page details of clinic

  @TestCaseKey=HOHP-T279
  Scenario: To verify Activity Timeline and filter options under my patients
    Then Navigate to patient under my patients
    Then Verify Activity Page details of clinic

  @TestCaseKey=HOHP-T281 @TestCaseKey=HOHP-T287 @TestCaseKey=HOHP-T288
  Scenario: To verify Add, Update and Delete an Activity
    And Navigate to patient under my patients
    Then Verify Add an Activity "Drink water"
      | No of Glass |
      | 2           |
    And Verify added activity "Drink water" in patient app
    Then Verify Update an Activity "Drink water"
    And Verify updated activity "Drink water" in patient app
    Then Verify Delete an Activity "Drink water"
    And Verify deleted activity "Drink water" in patient app
