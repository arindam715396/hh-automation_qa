package com.biofourmis.biovitals.model.patients;

import lombok.Builder;

@Builder
public class VitalsAt {
    Integer hr;
    Integer rr;
    Integer bpw;
    Integer bpsys;
    Integer bpdia;
    Integer weight;
    Integer bi;
    Integer core_temp;
    Integer spo2;
    Integer o_temp;
    Integer b_temp;
    Integer steps;
    Integer hr_bp;
    Long si;
}
