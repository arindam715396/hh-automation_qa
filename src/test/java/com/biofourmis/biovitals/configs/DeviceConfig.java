package com.biofourmis.biovitals.configs;

import com.biofourmis.biovitals.DeviceInfo;
import com.biofourmis.biovitals.DeviceInfoImpl;
import com.biofourmis.biovitals.constants.DeviceConstants;

import com.biofourmis.biovitals.device.DeviceType;
import com.biofourmis.biovitals.exception.DeviceNotFoundException;
import com.biofourmis.biovitals.model.Android;
import com.biofourmis.biovitals.model.Device;
import com.biofourmis.biovitals.model.Ios;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;

/**
 * This class is read only do not modify anything here
 */
@Configuration
public class DeviceConfig {

    private volatile Set<DeviceConstants> deviceConstants = new HashSet<>();
    private static final Object lock = new Object();

    @Autowired
    AppConfig appConfig;

    public DeviceConstants getFreeDevice(String appName) throws DeviceNotFoundException, IOException {
        if (appConfig.getAppPlatform(appName) == null) {
            throw new RuntimeException("You are trying to invoke a mobile driver without specifying the platform in the TestNG XML");
        }

        synchronized (lock) {
            if (this.deviceConstants.isEmpty())
                getConnectedDevices(appName);

            String platFormName = appConfig.getAppPlatform(appName).toLowerCase(Locale.ROOT);

            if (this.deviceConstants.stream()
                    .noneMatch(device -> device.getDevicePlatform().toLowerCase(Locale.ROOT).contains(platFormName)))
                throw new RuntimeException("No " + platFormName.toUpperCase(Locale.ROOT) + " device connected.");

            if (this.deviceConstants.stream()
                    .noneMatch(device -> !device.isActive() &&
                            device.getDevicePlatform().toLowerCase(Locale.ROOT).contains(platFormName)))
                throw new RuntimeException("All " + platFormName.toUpperCase(Locale.ROOT) + " device(s) are performing automation. No free device available.");

            DeviceConstants freeDevice = this.deviceConstants.stream()
                    .filter(deviceConstants ->
                            !deviceConstants.isActive() &&
                                    deviceConstants.getDevicePlatform().toLowerCase(Locale.ROOT).contains(platFormName))
                    .findFirst()
                    .get();

            toggleDeviceStatus(freeDevice);
            return freeDevice;
        }
    }

    public void toggleDeviceStatus(DeviceConstants deviceConstants) {
        this.deviceConstants.remove(deviceConstants);
        deviceConstants.setActive(!deviceConstants.isActive());
        this.deviceConstants.add(deviceConstants);
    }

    private void getConnectedDevices(String appName) throws DeviceNotFoundException, IOException {
        if (appConfig.getCurrentOS().toLowerCase(Locale.ROOT).contains("window")) {
            if (getDeviceInfo(DeviceType.ANDROID))
                addDevices(new DeviceInfoImpl(DeviceType.ANDROID));
        } else {
            String platform = appConfig.getAppPlatform(appName).trim();
            if (platform.equalsIgnoreCase("ios") || platform.equalsIgnoreCase("ipad")) {
                if (getDeviceInfo(DeviceType.IOS))
                    addDevices(new DeviceInfoImpl(DeviceType.IOS));
                if (getDeviceInfo(DeviceType.IOSSIMULATOR))
                    addDevices(new DeviceInfoImpl(DeviceType.IOSSIMULATOR));
            } else if (platform.equalsIgnoreCase("android") || platform.equalsIgnoreCase("tab")) {
                if (getDeviceInfo(DeviceType.ANDROID))
                    addDevices(new DeviceInfoImpl(DeviceType.ANDROID));
            } else
                throw new RuntimeException("This platform is not supported yet. Platform name :" + platform);
        }

        if (this.deviceConstants.isEmpty())
            System.exit(0);
    }

    private void addDevices(DeviceInfo deviceInfo) throws DeviceNotFoundException, IOException {
        for (Device device : deviceInfo.getDevices()) {
            this.deviceConstants.add(new DeviceConstants().builder()
                    .devicePlatform(getPlatform(device))
                    .udid(device.getUniqueDeviceID())
                    .productVersion(device.getProductVersion())
                    .buildVersion(device.getBuildVersion())
                    .modelNumber(device.getModelNumber())
                    .serialNumber(device.getSerialNumber())
                    .integratedCircuitCardIdentity(device.getIntegratedCircuitCardIdentity())
                    .isActive(false)
                    .build());
        }
    }

    private String getPlatform(Device device) {
        try {
            Field privateStringField;
            if (device.getDeviceProductName().equalsIgnoreCase("Android")) {
                privateStringField = Android.class.
                        getDeclaredField("robuildcharacteristics");
            } else {
                privateStringField = Ios.class.
                        getDeclaredField("deviceClass");
                return "ios";
            }
            privateStringField.setAccessible(true);

            String type = privateStringField.get(device).toString();

            //TODO Add condition for iOS Once get iOS Device
            if (device.getDeviceProductName().toLowerCase(Locale.ROOT).contains("ios"))
                return type;
            else {
                if (type.equalsIgnoreCase("tablet"))
                    return "tab";
                if (type.equalsIgnoreCase("phone"))
                    return "android";
                if (type.equalsIgnoreCase("default"))
                    return "android";
            }
            return type;
        } catch (Exception ec) {
            throw new RuntimeException(ec);
        }
    }

    private boolean getDeviceInfo(DeviceType type) {
        try {
            return new DeviceInfoImpl(type).anyDeviceConnected();
        } catch (IOException | DeviceNotFoundException | NullPointerException ioException) {
            return false;
        }
    }

}

