package com.biofourmis.biovitals.model.patients;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CAD {
    Boolean answer;
    Boolean CABG;
    Boolean CABGYear;
    Boolean stents;
    Integer numStents;
    String[] stentsYears;
}
