package com.biofourmis.biovitals.pagefactory.patient;

import com.biofourmis.biovitals.utilities.CommonUtils;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;


@Log4j
public class ContactPage extends CommonUtils {

    private WebDriver driver;

    public ContactPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        log.info("Provide audio and camera permissions.");
        try {
            acceptAudioVideoPermissions();
        } catch (Exception e) {
            throw new RuntimeException("Failed while provide audio and camera permissions.");
        }
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/textview_contacts_list_header")
    private WebElement contactHeader;
    @AndroidFindBy(xpath = "//*[@content-desc='Diary']")
    private WebElement diary;
    @AndroidFindBy(xpath = "//*[@content-desc=\"Today's plan\"]")
    private WebElement todayPlan;
    @AndroidFindBy(xpath = "//*[@content-desc=\"My health\"]")
    private WebElement myHealth;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/image_close_popup")
    private WebElement cancelPop;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/et_chat")
    private WebElement messageInput;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/send_chat")
    private WebElement sendBtn;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/rejectCall")
    private WebElement rejectCall;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/acceptCall")
    private WebElement acceptCall;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/disconnectActionIB")
    private WebElement endCall;
    @AndroidFindBy(id = "com.android.permissioncontroller:id/permission_allow_foreground_only_button")
    private WebElement alwaysAllow;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/textview_message_loading")
    private WebElement loading;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/tvMessage")
    private WebElement unreadMessagePopUpText;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/textview_chat_count")
    private WebElement chatBadge;
    private By chatMessages = By.xpath("//*[@resource-id='com.biofourmis.careathomerpm:id/message_body']");
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/textview_header_chat")
    private WebElement chatHeader;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/et_chat")
    private WebElement inputMessage;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/send_chat")
    private WebElement sendChat;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/image_close_chat")
    private WebElement closeChatPop;


    public void clickOnTodaysPlan() throws Exception {
        _click(todayPlan);
    }

    public void clickOnMyHealth() throws Exception {
        _click(myHealth);
    }

    public void clickOnDiary() throws Exception {
        _click(diary);
    }

    public boolean isContactHeaderVisible() {
        return _isElementVisible(contactHeader);
    }

    public void clickOnClinicianName(String arg0) throws Exception {
        if (!_isTextContainsPresent(arg0))
            Assert.fail("Clinician [" + arg0 + "] not displayed.");
        _clickOnTextContains(arg0);
    }

    public boolean verifyContactPopUp(String element) {
        return _isTextContainsPresent(element);
    }

    public void cancelPopUp() throws Exception {
        _click(cancelPop);
    }

    public void sendText(String free_text) throws Exception {
        _clickOnTextContains("Text message");
        _click(messageInput);
        _sendKeys(messageInput, free_text);
        _click(sendBtn);
    }

    public void startCall(String arg0) throws Exception {
        _clickOnTextContains(arg0 + " call");
    }

    public boolean verifyCallRejected() {
        return _isElementVisible(endCall);
    }

    public boolean verifyCallAccepted() {
        return _isElementVisible(endCall);
    }

    public boolean verifyCallEnded() {
        return _isElementVisible(endCall);
    }

    public boolean isCallRinging() throws Exception {
        if (!_isTextContainsPresent("Tap to accept")) {
            if (_isElementVisible(alwaysAllow))
                _click(alwaysAllow);
            if (_isElementVisible(alwaysAllow))
                _click(alwaysAllow);
        }
        return _isTextContainsPresent("Tap to accept");
    }

    public boolean verifyIncomingCallText(String incoming_video_call) {
        return _isTextContainsPresent(incoming_video_call);
    }

    public boolean rejectCall() throws Exception {
        _click(rejectCall);
        return _isElementVisible(rejectCall);
    }

    public boolean acceptCall() throws Exception {
        _click(acceptCall);
        return _isElementVisible(acceptCall);
    }

    public void endCall() throws Exception {
        _click(endCall);
    }

    public void acceptAudioVideoPermissions() throws Exception {
        AndroidDriver android = (AndroidDriver) driver;
        String udid = android.getCapabilities().getCapability("udid").toString();
        Runtime.getRuntime().exec("adb -s " + udid + " shell pm grant " + android.getCurrentPackage() + " android.permission.CAMERA");
        Runtime.getRuntime().exec("adb -s " + udid + " shell pm grant " + android.getCurrentPackage() + " android.permission.RECORD_AUDIO");
    }

    public boolean verifyUnreadMessagePopUp() {
        _sleep(5);
        return _waitForElementVisible(unreadMessagePopUpText, 80);
    }

    public boolean clickOnPopUp(String arg0) throws Exception {
        _sleep(8);
        _clickOnTextContains(arg0);
        return _waitForElementInvisible(unreadMessagePopUpText);
    }

    public boolean verifyBadgeCount(int messageCount) throws Exception {
        _waitForElementVisible(chatBadge);
        return _getText(chatBadge).strip().equals(String.valueOf(messageCount));
    }

    public void clickOn(String text_message) throws Exception {
        _clickOnTextContains(text_message);
    }

    public boolean verifyChatHeader(String chat) throws Exception {
        return _getText(chatHeader).equals(chat);
    }

    public boolean verifyAlertMessage(String s) {
        return _isTextContainsPresent(s);
    }

    public boolean verifyInputMessage() {
        return _isElementVisible(inputMessage);
    }

    public boolean verifyChatSendBtn() {
        return _isElementVisible(sendChat);
    }

    public boolean verifyClosePopUp() {
        return _isElementVisible(closeChatPop);
    }

    public ArrayList<String> verifyChatMessage(ArrayList<String> messages) throws Exception {
        _waitForLoading();
        List<WebElement> chatMessages = driver.findElements(this.chatMessages);
        ArrayList<String> actualMessages = new ArrayList<>();
        while (chatMessages.size() != messages.size()) {
            chatMessages.remove(0);
        }
        for (WebElement element : chatMessages)
            actualMessages.add(element.getText());
        return actualMessages;
    }

    public boolean closeChatPopUp() throws Exception {
        _click(closeChatPop);
        return _waitForElementInvisible(closeChatPop);
    }

    public void sendTextMessage(String textMessage) throws Exception {
        _waitForLoading();
        _sleep(10);
        _waitForElementVisible(inputMessage);
        _sendKeys(inputMessage, textMessage);
        _click(sendBtn);
    }

    public boolean verifySentTextMessage(String textMessage) {
        _waitForLoading();
        _sleep(10);
        List<WebElement> chatMessages = driver.findElements(this.chatMessages);
        int messages = chatMessages.size();
        return textMessage.equals(chatMessages.get(messages - 1).getText());
    }

    public boolean verifyUnreadMessagePopUpText(int count) {
        return _isTextContainsPresent("You have " + count + " unread message from care team. Please check your message from the care team contact.");
    }
}
