package com.biofourmis.biovitals.pagefactory.hcp;

import com.biofourmis.biovitals.utilities.CommonUtils;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class ClinicalNotesPageApp extends CommonUtils {

    private WebDriver driver;

    public ClinicalNotesPageApp(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @iOSXCUITFindBy(id = "ic expand")
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/expand_collapse")
    private WebElement expandCollapse;

    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/searchIcon")
    private WebElement search;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"Patients\"]")
    private WebElement backIcon;

    public boolean verifyNoteTitleDisplayed(String title) throws Exception {
        _waitForProgressBarDisappearHCP();
        return _isTextContainsPresent(title);
    }

    public boolean verifyNoteDescriptionDisplayed(String description) throws Exception {
        _waitForProgressBarDisappearHCP();
        if (_isTextContainsPresent(description))
            return true;
        else
            _click(expandCollapse);
        return _isTextContainsPresent(description);
    }

    public void navigateBackToPatient() throws Exception {
        try {
            if (driver instanceof AndroidDriver)
                while (!_isElementVisible(search)) {
                    _waitForProgressBarDisappearHCP();
                    _pressBack();
                }
            return;
        } catch (Exception e) {
            e.printStackTrace();
        }
        _click(backIcon);
    }

    public boolean verifyNoteDeleted(String title) {
        _waitForProgressBarDisappearHCP();
        return !_isTextContainsPresent(title);
    }

    public boolean verifyClinicalPagNavBar(String primaryPatientName) {
        _waitForProgressBarDisappearHCP();
        if (driver instanceof IOSDriver)
            return _isElementVisible(By.xpath("//XCUIElementTypeNavigationBar[@name='" + primaryPatientName + "']"));
        else
            throw new RuntimeException("Fix for android");
    }

    public boolean verifyNoClinicalNotesYetMessage(String no_clinical_notes_yet) {
        if (driver instanceof IOSDriver)
            return _isElementPresent(By.xpath("//XCUIElementTypeStaticText[@name='" + no_clinical_notes_yet + "']"));
        else
            throw new RuntimeException("Fix for android");
    }

    public boolean isNoteDetailsDisplayed(String title) {
        _waitForProgressBarDisappearHCP();
        return _isTextContainsPresent(title);
    }

    public void waitForPageAutoRefresh() {
        _sleep(20);
    }
}
